/*
 * Copyright (c) 1997, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package java.util;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import sun.misc.SharedSecrets;

/**
 * 此类是基于Hash Table的,并且实现了Map接口：它实现了所有Map接口中的操作，并且允许null值和null键。
 * 与Hash Table不同的是①他并不是同步的，并且允许空键和空值。②这个类不保证节点的存放是有序进行的。
 * <p>
 * 这里所有的实现方法为基础操作get和put提供了O(1)时间复杂度的操作性能。假设hash函数将节点适当的
 * 分散在桶中，那么在集合视图上迭代耗费的时间和HashMap实例的容量(也就是实例中桶的数量)加上它的预设大小
 * 成正比。
 * 因此，为了保持性能，请不把初始容量设置过高或者将负载因子设置得太低。
 * <p>
 * 影响HashMap实例的性能的指标有两个:
 * 1.hash表中桶的数量 2.实例创建时的设置的初始容量
 * 负载因子（load Factor）是hash表自动扩容时实际使用量的一个比例值。
 * 当桶的数量超过负载因子阈值和当前hash表的容量时，就会发生rehashed（意思是，数据结构会重建，hash表中的桶数量大致会翻倍）
 * <p>
 * 一个默认的规则，负载因子的值设置为0.75 也就是四分之三时，在空间和时间的性能花费上是一个比较好的
 * 折中数值。 高于这个值会减少空间开支但是也会增加get和put的时间花费。
 * 在设置Map的初始容量时，应该考虑Map的初始容量数值和它的负载因子，以此来减少rehashed动作的数量。如果Map的初始容量 > 预期容量 ÷ 负载因子，那么将不会触发rehashed。
 * （节省一次重新哈希并且扩容的时间。）
 * <p>
 * 如果有许多可以预期的数据要被放到HashMap实例的桶中去，那么在初始化的时候给一个足够大的容量将能够让这些映射更有效率地存储。而不是根据Hash表的容量大小不断地
 * 扩容然后重新Hash，直到一个最合适的容量为止。
 * 有一点需要注意，使用大量hashCode相同的对象作为key肯定会降低hash表的性能。（原因是，每次发现hashCode相同都要再通过equals进行二次比较）。
 * 所以为了这些影响，当键是 Comparable 时，这个类可以使键之间的比较顺序来帮助打破约束。
 * <p>
 * 注意，这里的实现是不同步的。如果有多个线程并发地去操作同一个HashMap，并且至少有一个线程对HashMap进行了结构性的修改，那么这个HashMap一定要是同步的。
 * （结构性的修改是指任何添加或者删除一个或多个节点的操作，仅仅改变一个Hash桶中已经存在的key所对应的值不能叫做结构性的修改）。
 * 这个同步通常是通过封装在HashMap中的一些对象上的同步来完成的。
 * <p>
 * 如果不存在这样的方法，那么Map应该在使用之前进行包装一下：
 * Map m = Collections.synchronizedMap(new HashMap(...));
 * 以防意外的非同步方法的访问。
 * <p>
 * 一个类所有的集合视图方法返回的迭代器都遵循fast-fail规则：如果迭代器在遍历的任何时候结构发生了修改，除了通过迭代器自己的remove方法，其他的任何方式
 * 修改迭代器都会抛出 ConcurrentModificationException 异常。所以，在迭代器或者容器面对并发修改结构时，迭代器会立即触发fast-fail机制，立即地抛出异常。
 * 为了避免集合在任意不确定的时间冒任何不确定的风险。
 * <p>
 * 注意： 迭代器的 fail-fast 行为无法被保证，因为一般来说，在存在非同的并发修改的情况下，不可能做出任何硬性保证。fail-fast机制尽最大的努力抛出
 * ConcurrentModificationException 来确保集合结构被并发修改时能发出通知。因此，在编写程序的时候依靠此异常来保证集合结构被修改的正确性的行为是错误的。
 * 我们必须知道，迭代器的 fail-fast 行为应该只用于检测BUG。
 * <p>
 * <p>
 * 这个类是JAVA框架说明网址的成员之一，具体地址如下：
 * https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html
 *
 * @param <K> 此映射维护的键类型
 * @param <V> 映射值的类型
 * @author Doug Lea 也是 道格 利写的，后续由其他人员维护
 * @author Josh Bloch
 * @author Arthur van Hoff
 * @author Neal Gafter
 * @see Object#hashCode()
 * @see Collection
 * @see Map
 * @see TreeMap
 * @see Hashtable
 * @since 1.2
 */
public class HashMap<K, V> extends AbstractMap<K, V>
        implements Map<K, V>, Cloneable, Serializable {

    private static final long serialVersionUID = 362498820763181265L;

    /*
     * 实现此类时的注意事项：
     * 这个 Map类通常被当做散列表来使用。当其中的桶数量太多时，他们就会转换为TreeNode节点，转换后的每个
     * 桶的节点和 java.util.TreeMap中的桶结构类似。
     *
     * 此Map中的结构大多数只是普通的桶结构。但是在适当的时机桶结构会转化为TreeNode结构。
     * TreeNodes的桶可以像其他桶一样遍历和使用，但是在填充节点过多的情况下能支持更快地查找。
     * 然而，由于正常使用的多大多数桶都没有被过度填充，因此在使用table方法的过程中可能会延迟检查是否存在TreeNodes节点。
     *
     * Tree形式的Hash桶（即：其节点都是TreeNode的桶结构），主要通过hashCode进行排序，但是假如两个节点是同一个类
     * 并且都实现了 Comparable<C> 接口，那么他们将通过 重写的compareTo方法进行排序
     * （我们保守地通过反射检查泛型类型来验证这一点——请参阅comparableclass方法）。
     *
     * 在效率最坏的情况下的 O(log n)操作时，当键具有不同的Hash值或者可排序时，树容器的额外复杂度是值得的，因此，在hashCode()
     * 方法中返回分布不均匀的值以及许多键共享一个hashCode的值将会导致性能慢慢地下降。（如果这两种情况都不适用，我们可能会浪费大约
     * 2倍的时间和空间，而不采取任何预防措施。但目前唯一已知的情况是来自糟糕的用户编程实践，这些时间已经非常缓慢，几乎没有什么区别。）
     *
     * 因为TreeNodes的大小大约是常规节点的两倍，所以一般只有在bin包含了足够多的节点时，才会使用它（参考 TREEIFY_THRESHOLD 树形阈值）。
     * 当树节点变得过小时（由于移除或者调整大小时），他们会被重新转换成普通的桶节点。
     * 在使用分布良好的用户hashCode时，很少使用到树容器。理想情况下，在随机哈希码下，箱中节点的频率
     * 服从泊松分布(http://en.wikipedia.org/wiki/Poisson)参数平均约为0.5，默认的调整阈值为0.75，
     * 但由于调整粒度的原因差异较大。
     *
     * 忽略方差，
     * 列表大小k的预期出现次数为（exp（-0.5）*pow（0.5，k）/阶乘（k））。第一个值是：
     * 0: 0.60653066
     * 1: 0.30326533
     * 2: 0.07581633
     * 3: 0.01263606
     * 4: 0.00157952
     * 5: 0.00015795
     * 6: 0.00001316
     * 7: 0.00000094
     * 8: 0.00000006
     * 更多：不到千万分之一
     *
     *
     * 树结构的根节点通常是它的第一个节点。然而，有时候（当前仅在 Iterator.remove方法），根节点有可能在其他地方，
     * 但可以通过附链接的方法 TreeNode.root()恢复。
     *
     * 所有适用的内部方法都接受哈希代码作为参数（通常由公共方法提供），允许它们在不重新计算用户哈希代码的情况下互相调用。
     * 大多数内部方法也接受“tab”参数，通常是当前表，但在调整大小或转换时可能是新表或旧表。
     *
     * 当桶列表树形化时，切分或者反树形化，我们保证他们在相同的相对访问/遍历顺序（即Node.next）更好地保留局部性，并且稍微
     * 简化处理调用迭代器的remove方法。当插入节点使用Comparator时，为了重新平衡过程中保持总的顺序（或者说尽可能接近这里所要求的顺序）
     * 我们将类和标识hashcode作为连接断路器进行比较。
     *
     *
     * 子类LinkedHashMap的存在，使得普通模式和树模式之间的使用和转换变得复杂。请参阅下面的一些钩子方法，
     * 定义这些方法是为了在插入、移除和访问节点时进行监听式调用。依赖这些机制，从而允许LinkedHashMap在内部保持独立。
     * （这些还需要将Map实例传递给一些可能创建节点的使用工具方法。）
     *
     * 类似于并发编程的基于 SSA的编码风格有助于避免所有扭曲指针操作中的混叠错误。
     */

    /**
     * 默认的初始容量 16 ，必须是2的次幂
     * 为什么？
     * ¡ 因为n永远是2的n次幂，所以n-1 永远保证尾端是连续的1的形式。然后使用 (n-1)&hash则可以快速取到hash的后x位的1.
     * ¡¡ 能保证索引值肯定 < capacity，不会超出HashMap的数组长度。
     * ¡¡¡ (n-1) & hash ，当n为2的次幂时，满足一个公式 (n-1) & hash = hash % n
     * 在jdk1.7中 初始容量
     */
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16

    /**
     * 最大容量默认值，如果有参数的构造函数隐式制定了更高的值，则使用1<<30来替代它。
     * 大约是10个亿， 1073741824
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * 构造函数中未指定时使用的负载因子默认值
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * Hash桶的存储的结构形式树形化的阈值。当数量超过这个值，则转红黑树。
     */
    static final int TREEIFY_THRESHOLD = 8;

    /**
     * 在调整大小的操作期间判断 Hash桶的数量小于这个值时，树形收缩为链表。
     */
    static final int UNTREEIFY_THRESHOLD = 6;

    /**
     * 如果允许HashMap树形化，那么它的最小的容量至少要大于这个值（64）。（否则bin中的节点太多，则会调整表的大小）
     * 所以，为了避免扩容和树形化发生冲突，Hash表的大小应该至少是 4倍的 树形化阈值，即 4*8=32。
     */
    static final int MIN_TREEIFY_CAPACITY = 64;

    /**
     * 基本哈希桶节点类，用于存储大多数条目。
     * （请参见下面的TreeNode子类，并在 LinkedHashMap 中查看其Entry子类。）
     */
    static class Node<K, V> implements Map.Entry<K, V> {
        final int hash;//hash地址
        final K key; //键
        V value;//值
        Node<K, V> next;//下一个节点

        //构造
        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public final K getKey() {
            return key;
        }

        @Override
        public final V getValue() {
            return value;
        }

        @Override
        public final String toString() {
            return key + "=" + value;
        }

        //重写equals后最好重写 hashCode方法，如果这个对象一定不会在散列表中使用也可以不重写。
        @Override
        public final int hashCode() {
            //将键的hashCode与 值的hashCode 异或混合。
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        @Override
        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }

        //重写equals 方法
        @Override
        public final boolean equals(Object o) {
            //引用相同则必相同
            if (o == this) return true;
            if (o instanceof Map.Entry) {
                //如果是 Map.Entry类型，则分别比较键和值是否都相等则返回true 否则返回false
                Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
                return Objects.equals(key, e.getKey()) && Objects.equals(value, e.getValue());

            }
            return false;
        }
    }

    /* ---------------- 静态程序集 -------------- */

    /**
     * 著名的【扰动函数】
     * 计算存储在Map中key的hash值.
     * 计算方式: 将传入key的hashCode的散列值的 高16位 与 自身进行异或，来加大key的散列程度。
     * <p>
     * 由于该表使用的两个掩码的幂，因此仅在当前掩码的低位上产生hash将会经常发生hash碰撞（也就是常说的 哈希冲突），
     * 在一直的例子中有一组数据，它们以一串连续整数的浮点数作为键。
     * <p>
     * 因此我们应用了一种策略：通过将原来的键的hashCode右移16位，并与自己异或，来让高16位也参与到hash的过程中。
     * 这样做是在速度、实用性和位扩展质量之间取的一种折中方案。因为许多常见的散列集已经被合理地分布了
     * （因此不能从扩展中受益），而且因为我们使用树型结构来处理容器中的大量冲突，所以我们只是以最小开销的方式
     * 采用移位运算和疑惑运算，以减少系统性能的损失，以及合并最高位的影响。否则由于表边界的限制，这些key所对应的
     * hashCode的高位将永远不会用于索引计算。
     */
    static final int hash(Object key) {
        int h;
        //扰动函数：  如果key为空，返回0  否则返回 ((key。hashCode) ^ (key.hashCode >>> 16))
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    /**
     * 当x的类型为X，且X直接实现了Comparable接口（比较类型必须为X类本身）时，返回x的运行时类型；
     * 否则返回null。
     */
    static Class<?> comparableClassFor(Object x) {
        //是否实现了Comparable接口
        if (x instanceof Comparable) {
            Class<?> c;
            Type[] ts, as;
            Type t;
            ParameterizedType p;
            //string类型 直接返回String.class
            if ((c = x.getClass()) == String.class) // bypass checks
                return c;
            //是否有直接实现的接口
            if ((ts = c.getGenericInterfaces()) != null) {
                //遍历直接实现的接口
                for (int i = 0; i < ts.length; ++i) {
                    if (((t = ts[i]) instanceof ParameterizedType) && //该接口实现了泛型
                            ((p = (ParameterizedType) t).getRawType() == Comparable.class) && // 获取接口不带参数部分的类型对象，且该类型对象是Comparable
                            (as = p.getActualTypeArguments()) != null && //获取泛型参数数组
                            as.length == 1 && as[0] == c) // type arg is c  只有一个泛型参数，且该实现类型是该类型本身
                        return c; //返回该类型
                }
            }
        }
        return null;
    }

    /**
     * 如果X与kc（k的可比类）类型相匹配，则返回 k.compareTo(x),否则返回0 即false。
     * <p>
     * <p>
     * compareTo方法：
     * 如果指定的数与参数相等返回0。
     * 如果指定的数小于参数返回 -1。
     * 如果指定的数大于参数返回 1。
     */
    @SuppressWarnings({"rawtypes", "unchecked"}) // for cast to Comparable
    static int compareComparables(Class<?> kc, Object k, Object x) {
        return (x == null || x.getClass() != kc ? 0 : ((Comparable) k).compareTo(x));
    }

    /**
     * 返回一个最接近设置的初始容量的 2的次幂值作为初始容量。
     */
    static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;//等价于 n = n >>> 1 | n
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    /* ---------------- 对象 -------------- */

    /**
     * Hash表的对象，在第一次使用的时候进行初始化，并根据需要扩容。（容量始终是2的次幂）
     * （在某些操作中，能够容忍长度为0，以允许在创建时不需要初始化的策略）
     */
    transient Node<K, V>[] table;

    /**
     * 保留缓存的 entrySet()。请注意,AbstractMap字段用于keySet()和values()。
     */
    transient Set<Map.Entry<K, V>> entrySet;

    /**
     * Map中包含的 key-value映射总量。
     */
    transient int size;

    /**
     * 此HashMap的结构被修改时，modCount+1.
     * (结构被修改的场景指的是：指的是更改HashMap中的映射数量或者以其他方式修改其内部结构。例如:rehash)
     * 此字段主要用于 HashMap在迭代器中操作时触发 fast-fail 机制。
     * The number of times this HashMap has been structurally modified
     * Structural modifications are those that change the number of mappings in
     * the HashMap or otherwise modify its internal structure (e.g.,
     * rehash).  This field is used to make iterators on Collection-views of
     * the HashMap fail-fast.  (See ConcurrentModificationException).
     */
    transient int modCount;

    /**
     * 决定 HashMap 是否扩容的 阈值大小。这个值 = capacity * loadfactor
     *
     * @serial
     */
    // 序列化后javadoc描述为true。
    // 此外，如果尚未分配表数组，
    // 则此字段将保留初始数组容量，或零
    // 表示默认的初始容量。）
    int threshold;

    /**
     * hash表的负载因子，浮点数
     *
     * @serial
     */
    final float loadFactor;

    /* ---------------- 公共业务 -------------- */

    /**
     * 一个空的HashMap的构造方法，允许使用指定的 初始容量 和 负载因子。
     *
     * @param initialCapacity 初始容量
     * @param loadFactor      负载因子
     * @throws IllegalArgumentException 如果初始容量为负或负载系数为非正，将会抛出异常
     */
    public HashMap(int initialCapacity, float loadFactor) {
        //初始容量 < 0 则抛异常
        if (initialCapacity < 0) throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);

        //超过最大值 则取最大值
        if (initialCapacity > MAXIMUM_CAPACITY) initialCapacity = MAXIMUM_CAPACITY;

        //负载因子 <=0 或者 无限大 则抛异常
        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);

        //赋值 负载因子、 初始容量
        this.loadFactor = loadFactor;
        //初始容量并不一定等于初始设置的值，而是取最接近 给定值的 2的次幂值。
        // **** 值得注意的是，这里接收表的初始容量的对象是 threshold 所以，在扩容时，如果没有没初始化但是存在旧的阈值时，会直接将旧的阈值作为扩容后的容量 ****
        this.threshold = tableSizeFor(initialCapacity);
    }

    /**
     * 一个空的HashMap的构造方法，允许使用指定的 初始容量 和 默认的负载因子 0.75。
     *
     * @param initialCapacity 初始容量
     * @throws IllegalArgumentException 如果初始容量为负，则抛异常.
     */
    public HashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * 一个空的HashMap的构造方法，初始容量和负载因子都是默认值。
     */
    public HashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR; // all other fields defaulted
    }

    /**
     * 由Map构造HashMap。负载因子为默认值。
     *
     * @param m 将要填充到HashMap中的Map映射表
     * @throws NullPointerException map 为 null则抛异常
     */
    public HashMap(Map<? extends K, ? extends V> m) {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        putMapEntries(m, false);
    }

    /**
     * putMapEntries函数会被
     * HashMap的拷贝构造函数public HashMap(Map<? extends K, ? extends V> m)
     * 或者
     * Map接口的putAll函数（被HashMap给实现了）调用到。
     * 该函数由于是默认的包访问权限，所以一般情况下用户无法调用。
     *
     * @param m     需要填充的Map映射集
     * @param evict false when initially constructing this map, else
     *              true (relayed to method afterNodeInsertion).
     */
    final void putMapEntries(Map<? extends K, ? extends V> m, boolean evict) {
        //接收 Map大小
        int s = m.size();
        if (s > 0) {
            //valid 1 至少有一个节点才允许拷贝
            if (table == null) { // pre-size
                //valid 2 如果table == null 说明是拷贝构造函数来调用，因为之前表里面没有放任何节点
                //括号中会计算一个刚好不大于阈值的容量 由于可能出现浮点数，所以这里需要 + 1.0F 向上取整。（预期容量/负载因子 +1F 是减少扩容频率的最佳尝试）
                float ft = ((float) s / loadFactor) + 1.0F;
                //valid 3 如果预期最佳容量 小于最大值则取之，否则取最大值。
                int t = ((ft < (float) MAXIMUM_CAPACITY) ? (int) ft : MAXIMUM_CAPACITY);
                //valid 4 如果预期最佳容量大于扩容阈值，则传入预期容量重新计算最佳容量（取大于且最靠近的2的次幂的值作为容量）
                if (t > threshold) threshold = tableSizeFor(t);
            } else if (s > threshold) {
                //table 不为null，说明之前表中已有节点存在。 然后这里传入map的容量又超过了 阈值。那么预先扩容再放置。
                resize();
            }

            //遍历需要put到HashMap ，根据key-value 对号入坐。
            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
                K key = e.getKey();
                V value = e.getValue();
                //put键值对，如果原来HashMap中存在这个key则直接覆盖。
                putVal(hash(key), key, value, false, evict);
            }
        }
    }

    /**
     * 返回Map中键值对的数量
     *
     * @return the number of key-value mappings in this map
     */
    public int size() {
        return size;
    }

    /**
     * 返回Map中是不是一个键值对都没有。
     *
     * @return <tt>true</tt> if this map contains no key-value mappings
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * 返回一个Key对应的值。
     * ¡ 如果没有匹配的值，将返回null。（更确切地说，如果这个Map包含Key到值的映射，
     * 像这样 (key == null ? k == null : key.equals(k)),那么这个方法返回值，否则返回null）
     * ¡¡ 最多有一个null值映射。
     * ¡¡¡ 返回值是nul并一定标识映射不包含该键的映射；Map也有可能显式地将键映射到 null值上了。
     * 所以如果我们要判断容器内是否有一个Key的值，一般使用containsKey()方法来区分这两种情况。
     *
     * @see #put(Object, Object)
     */
    public V get(Object key) {
        Node<K, V> e;
        return (e = getNode(hash(key), key)) == null ? null : e.value;
    }

    /**
     * 实现了Map.get方法，和相关方法
     *
     * @param hash key的Hash值
     * @param key  键对象
     * @return 返回这个Key对应的节点，如果匹配不到则返回null
     */
    final Node<K, V> getNode(int hash, Object key) {
        Node<K, V>[] tab;
        Node<K, V> first, e;
        int n;
        K k;
        if ((tab = table) != null && (n = tab.length) > 0 &&
                (first = tab[(n - 1) & hash]) != null) {
            //valid 1 (tab = table) != null && (n = tab.length) > 0  table已初始化 并且 table中至少有一对映射。
            //并且
            //valid 2 (first = tab[(n - 1) & hash]) != null （n-1）& hash 得到hash桶的索引，直接取出该桶内的第一个节点赋值给first（first即代表桶内首节点），判定不为空
            //这里始终会检查Hash桶内的首节点
            if (first.hash == hash && // always check first node
                    ((k = first.key) == key || (key != null && key.equals(k))))
                //valid 3 first.hash == hash 桶内首节点first的 hash值 等于 传入键的 hash值。
                //并且
                //valid 4 (k = first.key) == key || (key != null && key.equals(k))) 首节点的键和传入的key值有相同的引用 或者 key值不会空 并且 首节点的键等于传入的key值
                //如果首节点存在且是匹配的值，则返回之。否则下面按照链表查询方式继续查找。
                return first;
            if ((e = first.next) != null) {
                //树结构 e 存在下一个节点则继续
                if (first instanceof TreeNode)
                    //首节点节点为树形结构 则调用树形结构的方法，以log(n)的时间复杂度查找其中对应的节点并且返回。
                    return ((TreeNode<K, V>) first).getTreeNode(hash, key);
                //不是树结构，那么认为是链表结构，以链表的方式查找符合的节点。
                do {
                    //如果当前节点有next节点且不为null，则继续
                    if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k))))
                        //valid 1 e.hash == hash，下一个节点的hash地址是否等于当前的hash地址
                        //并且
                        //valid 2 (k = e.key) == key || (key != null && key.equals(k))) 下一个节点键引用和传入key相同 或者 该节点的key不为空且该节点的键等于传入的key值。
                        //满足 valid 1 和 valid 2 则说明匹配到链表中的节点。返回之，否则继续沿着链表查找 直到找到目标节点
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }

    /**
     * 调用getNode方法 能找到包含传入的Key的元素则返回true，否则返回false
     *
     * @param key 需要测试是否存在的元素 key值。
     * @return 如果能找到 key对应的元素 则返回true。
     * key.
     */
    public boolean containsKey(Object key) {
        return getNode(hash(key), key) != null;
    }

    /**
     * 将指定的值与此映射中的指定键相关联，如果Map已经包含这个键，则替换原来的值。
     *
     * @param key   键，指定的值需要与之关联
     * @param value 值，需要与键关联。
     * @return 返回key对应的旧值，如果key之前没有映射，则返回null。
     * 当然，返回null也有可能找个key之前对应的值就是null。
     */
    public V put(K key, V value) {
        //放置节点
        return putVal(hash(key), key, value, false, true);
    }

    /**
     * 实现 Map接口的put 方法并重写
     *
     * @param hash         key的hash值
     * @param key          键
     * @param value        需要put的值
     * @param onlyIfAbsent 如果为ture 则替换已经存在的值
     * @param evict        如果为false，则说明table处于创建模式
     * @return 返回已存在的key对应的元素，如果不存在则返回null
     */
    final V putVal(int hash, K key, V value, boolean onlyIfAbsent, boolean evict) {
        Node<K, V>[] tab; //tab用来接收原来的表对象
        Node<K, V> p;
        int n, i;//n为 表长度 ，i 为hash桶的下标值。

        if ((tab = table) == null || (n = tab.length) == 0)
            //valid 1 hash表尚未初始化，或者hash表长度为0
            //直接进行插入前扩容然后重新复制给tab  返回扩容后长度
            n = (tab = resize()).length;

        //这里使用 (n-1) & hash 的原因很简单。因为 hash 表的长度固定是2的次幂，所以 2^n - 1 一定是最高位为0其他未均是1，
        // 此时进行与运算 得到的值相当于是 保留 hashcode 的后x位。 这种取值方式保证了一点：可以绝对确定下标不会越界。
        if ((p = tab[i = (n - 1) & hash]) == null)
            //valid 2 取出下标对应元素发现不存在
            //新建一个节点 下标为i，设置新元素的hash、key、value  暂不设置next节点
            tab[i] = newNode(hash, key, value, null);
        else {
            //已经有一个元素了
            Node<K, V> e;
            K k;
            //先直接在数组中查看对应下标是否有元素
            //valid 3 p.hash == hash  当前元素的 hash值与将要放置元素的 hash相等。
            //valid 4 (k = p.key) == key || (key != null && key.equals(k)) 当前元素与将要放置的元素 键 值 分别相等。
            if (p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k))))
                //将 p 赋值给 e 这种情况说明 找到了Hash桶中只有一个元素，或者找到有多个元素，但是首节点是需要的元素
                e = p;
            else if (p instanceof TreeNode)
                //valid 5 如果在Hash桶中找不到对应的键，此时元素可能存在于链表上 也可能存在于 红黑树中。
                //先向红黑树中put这个元素，然后将返回值赋值给 e
                e = ((TreeNode<K, V>) p).putTreeVal(this, tab, hash, key, value);
            else {
                //valid 6如果在Hash桶中找不到，也不在红黑树中，则说明此时Hash桶内的元素是链表形式。
                for (int binCount = 0; ; ++binCount) {
                    //从链表0号元素开始 往后遍历
                    if ((e = p.next) == null) {
                        //valid 7  找到链表中最后一个元素时（再往后就是null了），如果还没有找到匹配的元素则将需要放置的节点放置在它后面
                        //? 为什么会出现 valid 7 这种情况呢？ 前面已经有一个 地址元素已存在，但是居然发现链表中本就不存在这个元素。
                        // 这种情况是正常的！ 要put的元素的key确实有相同的 下标，证明发生了hash碰撞，那么只能先取出下标对应的Hash桶中的数据结构，
                        // 再去其中深度遍历查找是否存在，如果存在则替换。不存在则追加。
                        p.next = newNode(hash, key, value, null);

                        //valid 8 判断此时所处链表的位置，如果放置之后发现链表长度达到了阈值，那么需要转换为红黑树
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            //Hash桶内链表 转树
                            treeifyBin(tab, hash);
                        //终止循环
                        break;
                    }
                    //valid 9 如果当前元素与将要放置的元素 键 值 分别相等。 则说明找到了这个已经存在的元素
                    if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    //将 e 的引用给 p，相当于把p.next指针往后移一位。
                    p = e;
                }
            }


            // 以上操作都是为了确定一件事情： 在我put之前，这个元素存在吗？由于对每种结构的查询操作如果找到结果最后都有将查找出来的值赋值给 e 的动作
            //valid 10 找到了 之前存在的元素 e
            if (e != null) { // existing mapping for key
                //取e原来的值
                V oldValue = e.value;
                // valid 11 替换旧值设置为true 或者 旧值为null 则执行
                if (!onlyIfAbsent || oldValue == null)
                    //将新值赋值给 原来的 节点 e 替换旧值。
                    e.value = value;
                //!!!节点放置成功后置钩子
                afterNodeAccess(e);
                //将旧值返回
                return oldValue;
            }
        }

        //决定 fast-fail 的数量自增1
        ++modCount;

        if (++size > threshold)
            //valid 12 由于put了一个新元素，尺寸自增1，然后判断hash表尺寸超过了阈值则继续执行
            //扩容
            resize();

        //节点插入成功后并且hash表自身调整已完成 的后置钩子
        afterNodeInsertion(evict);

        //put的是之前不存在的元素 则返回null
        return null;
    }

    /**
     * 初始化 或者 表容量翻倍。如果table容量为null，则根据阈值和初始容量进行分配。
     * <p>
     * 否则，因为我们的下标都是根据而得次幂设置的，所以扩容时，每个Hash桶中的元素要么索引保持不变，
     * 要么在新表中以二的次幂偏移量移动。
     *
     * @return 返回扩容后的Hash表
     */
    final Node<K, V>[] resize() {
        //使用 oldTab 接收扩容前的 hash表对象。
        Node<K, V>[] oldTab = table;
        //旧容量取值  如果原来hash表为null则为0  否则 取旧容量的hash表长度。
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        //旧阈值
        int oldThr = threshold;

        //声明新容量 新阈值为0
        int newCap, newThr = 0;

        if (oldCap > 0) {
            //valid 1 之前至少存在一个节点
            if (oldCap >= MAXIMUM_CAPACITY) {
                //valid 2 容量已经大于等于溢出容量  则阈值设置为Integer最大值 21,4748,3647。 所以下面这行是随便给个比最大容量还大的数字给阈值,保证永远不会扩容就完事了？！
                threshold = Integer.MAX_VALUE;
                //无法再扩容了 返回旧的Hash表
                return oldTab;
            } else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                    oldCap >= DEFAULT_INITIAL_CAPACITY)
                //valid 3 新容量翻倍后仍 < 最大容量
                //并且
                //valid 4 旧容量大于等于默认的初始值16
                //满足 valid 3 和 4 则 阈值翻倍。
                newThr = oldThr << 1;
        } else if (oldThr > 0)
            //valid 5 之前没有任何节点，但是已经初始化过了，有阈值 ？？？ 为什么要把旧的阈值给新表作为容量：  参考threshold其实就是用来接收初始化容量的变量。hashMap中没有capacity变量！！
            //初始容量设置为阈值
            newCap = oldThr;
        else {
            //valid 6 没有进行过任何初始化操作，没有任何映射 阈值为0
            //HashMap中规定: 零初始阈值表示使用默认阈值
            //新容量16 和 新阈值 12 全部给默认值。
            newCap = DEFAULT_INITIAL_CAPACITY;
            newThr = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
        }


        if (newThr == 0) {
            //新阈值为 0 则 先 新阈值为新容量 * 负载因子
            float ft = (float) newCap * loadFactor;
            //新容量小于最大容量并且阈值也小于最大容量 则取上面得到的阈值 否则 取最大整数。
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float) MAXIMUM_CAPACITY ? (int) ft : Integer.MAX_VALUE);
        }
        //将计算出来的值赋值给阈值
        threshold = newThr;


        /**从以上操作我们知道, 初始化HashMap时,
         *  如果构造函数没有指定initialCapacity, 则table大小为16
         *  如果构造函数指定了initialCapacity, 则table大小为threshold,即大于指定initialCapacity的最小的2的整数次幂
         *
         * 下面开始探究扩容的逻辑：新建一个已计算出容量的 表对象。 它将被用来接收所有旧的节点。
         */
        @SuppressWarnings({"rawtypes", "unchecked"})
        Node<K, V>[] newTab = (Node<K, V>[]) new Node[newCap];
        //将引用赋值给 当前table
        table = newTab;
        if (oldTab != null) {
            //扩容前的哈希表不为null
            //遍历长度次数，从下标0 到最后一位。
            for (int j = 0; j < oldCap; ++j) {
                //声明一个节点
                Node<K, V> e;
                if ((e = oldTab[j]) != null) {
                    //该下标存在旧值
                    //置空当前下标的值
                    oldTab[j] = null;
                    //旧值后续没有元素（Hash桶中只有一个元素，不存在链表或数组结构）
                    if (e.next == null)
                        //将旧表中元素挪到新表计算后的下标中 （hash & (newTab.length -1) 就是新表的下标）
                        newTab[e.hash & (newCap - 1)] = e;
                    else if (e instanceof TreeNode)
                        //如果发现该元素是树节点 则调用split方法对红黑树进行拆分
                        ((TreeNode<K, V>) e).split(this, newTab, j, oldCap);
                    else {
                        //既不是空，也不是树结构，那么必为链表结构
                        //对链表结构进行拆分
                        Node<K, V> loHead = null, loTail = null;//头节点1，尾节点1
                        Node<K, V> hiHead = null, hiTail = null;//头节点2，尾节点2
                        Node<K, V> next;//下一个元素
                        //do while循环： (e = next) != null 该节点仍有下一个节点。则继续循环
                        do {
                            //next 取下一个节点
                            next = e.next;
                            if ((e.hash & oldCap) == 0) {
//                               //判断 并 拆分高位区和低位区链表
                                //为什么这样设计？首先移位运算尽可能地减少性能开销，而尽量少得移动元素来完成扩容有利于提高性能 。
                                // e.hash & oldCap == 0 意思是 元素的key的hash值如果在旧容量的最高位值为1，则视为高位区链表，扩容后，位置发生移动。下标为 (oldCap << 1 -1) & hash 也就是这里的 (j + oldCap)
                                // e.hash & oldCap != 0 意思是 元素的key的hash值如果在旧容量的最高位值为0，则视为低位区链表，扩容后，位置保持不变。下标为 (oldCap - 1) & hash 也就是这里的 oldCap
                                if (loTail == null)
                                    //低位区链表尾节点是空，则说明之前没有节点放进来，直接将头节点设为e即可
                                    loHead = e;
                                else
                                    //如果有尾巴，则追加到尾巴后面  【尾插法】
                                    loTail.next = e;
                                //尾插后重新将尾节点赋值
                                loTail = e;
                            } else {
                                if (hiTail == null) {
                                    //高位区链表尾节点为空，则说明之前没有节点放进来，直接将头节点设为e即可
                                    hiHead = e;
                                } else
                                    //如果有尾巴，则追加到尾巴后面   【尾插法】
                                    hiTail.next = e;
                                //将当前元素设置为高位区链表的尾节点
                                hiTail = e;
                            }
                        } while ((e = next) != null);//一直循环到链表末尾


                        //一个Hash桶中的链表结构拆分完成
                        // 现在得到了 一个高位区链表的头节点 hiHead 和 一个低位区链表的头节点 loHead
                        if (loTail != null) {
                            //1 存在低位区链表的尾节点，则确保尾节点next为null，防止环形链表
                            loTail.next = null;
                            //将 头节点 放到数组原下标的Hash桶内即可；头节点后面跟着拆分出来的节点
                            newTab[j] = loHead;
                        }


                        if (hiTail != null) {
                            //2 存在高位区链表的尾节点，则确保尾节点next为null，放置环形链表
                            hiTail.next = null;
                            //将 头节点 放到数组原下标 偏移旧的数组长度的 下标中即可； 头节点后面跟着拆分出来的节点
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
            }
        }

        //返回扩容后的 Hash表。
        return newTab;
    }

    /**
     * 链表转换红黑树 方法
     * <p>
     * 将Hash表指定下标所在的Hash桶的节点结构 由 【链表】 转换成 【红黑树】。
     * 当然，前提是表的大小不能够小于 最小树形阈值。否则将对Hash表进行扩容。
     */
    final void treeifyBin(Node<K, V>[] tab, int hash) {
        //声明 n hash表的长度
        int n, index;
        Node<K, V> e;
        if (tab == null || (n = tab.length) < MIN_TREEIFY_CAPACITY)
            //Hash表为空，或者 Hash表长度小于树化阈值。  直接扩容
            resize();
        else if ((e = tab[index = (n - 1) & hash]) != null) {
            //根据当前Hash桶的下标取出Hash桶赋值给e  判定Hash桶不为null 则继续执行
            //声明一个树节点 hd 和 tl
            TreeNode<K, V> hd = null, tl = null;

            //do while循环： 当桶内头节点有下一个节点 则继续
            do {
                //产生一个树的头节点
                TreeNode<K, V> p = replacementTreeNode(e, null);
                //tl 为null 则说明是 此时处理的链表中的头节点
                if (tl == null)
                    //将 p 赋值给 hd，作为根节点。
                    hd = p;
                else {
                    //否则 说明已有上一个节点
                    //
                    p.prev = tl;
                    tl.next = p;
                }
                //将 p 赋值 tl 暂存起来
                tl = p;
            } while ((e = e.next) != null);

            if ((tab[index] = hd) != null)
                hd.treeify(tab);
        }
    }

    /**
     * Copies all of the mappings from the specified map to this map.
     * These mappings will replace any mappings that this map had for
     * any of the keys currently in the specified map.
     *
     * @param m mappings to be stored in this map
     * @throws NullPointerException if the specified map is null
     */
    public void putAll(Map<? extends K, ? extends V> m) {
        putMapEntries(m, true);
    }

    /**
     * Removes the mapping for the specified key from this map if present.
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>.
     * (A <tt>null</tt> return can also indicate that the map
     * previously associated <tt>null</tt> with <tt>key</tt>.)
     */
    public V remove(Object key) {
        Node<K, V> e;
        return (e = removeNode(hash(key), key, null, false, true)) == null ?
                null : e.value;
    }

    /**
     * Implements Map.remove and related methods
     *
     * @param hash       hash for key
     * @param key        the key
     * @param value      the value to match if matchValue, else ignored
     * @param matchValue if true only remove if value is equal
     * @param movable    if false do not move other nodes while removing
     * @return the node, or null if none
     */
    final Node<K, V> removeNode(int hash, Object key, Object value,
                                boolean matchValue, boolean movable) {
        Node<K, V>[] tab;
        Node<K, V> p;
        int n, index;
        if ((tab = table) != null && (n = tab.length) > 0 &&
                (p = tab[index = (n - 1) & hash]) != null) {
            Node<K, V> node = null, e;
            K k;
            V v;
            if (p.hash == hash &&
                    ((k = p.key) == key || (key != null && key.equals(k))))
                node = p;
            else if ((e = p.next) != null) {
                if (p instanceof TreeNode)
                    node = ((TreeNode<K, V>) p).getTreeNode(hash, key);
                else {
                    do {
                        if (e.hash == hash &&
                                ((k = e.key) == key ||
                                        (key != null && key.equals(k)))) {
                            node = e;
                            break;
                        }
                        p = e;
                    } while ((e = e.next) != null);
                }
            }
            if (node != null && (!matchValue || (v = node.value) == value ||
                    (value != null && value.equals(v)))) {
                if (node instanceof TreeNode)
                    ((TreeNode<K, V>) node).removeTreeNode(this, tab, movable);
                else if (node == p)
                    tab[index] = node.next;
                else
                    p.next = node.next;
                ++modCount;
                --size;
                afterNodeRemoval(node);
                return node;
            }
        }
        return null;
    }

    /**
     * Removes all of the mappings from this map.
     * The map will be empty after this call returns.
     */
    public void clear() {
        Node<K, V>[] tab;
        modCount++;
        if ((tab = table) != null && size > 0) {
            size = 0;
            for (int i = 0; i < tab.length; ++i)
                tab[i] = null;
        }
    }

    /**
     * Returns <tt>true</tt> if this map maps one or more keys to the
     * specified value.
     *
     * @param value value whose presence in this map is to be tested
     * @return <tt>true</tt> if this map maps one or more keys to the
     * specified value
     */
    public boolean containsValue(Object value) {
        Node<K, V>[] tab;
        V v;
        if ((tab = table) != null && size > 0) {
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    if ((v = e.value) == value ||
                            (value != null && value.equals(v)))
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns a {@link Set} view of the keys contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own <tt>remove</tt> operation), the results of
     * the iteration are undefined.  The set supports element removal,
     * which removes the corresponding mapping from the map, via the
     * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>,
     * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
     * operations.  It does not support the <tt>add</tt> or <tt>addAll</tt>
     * operations.
     *
     * @return a set view of the keys contained in this map
     */
    public Set<K> keySet() {
        Set<K> ks = keySet;
        if (ks == null) {
            ks = new KeySet();
            keySet = ks;
        }
        return ks;
    }

    final class KeySet extends AbstractSet<K> {
        public final int size() {
            return size;
        }

        public final void clear() {
            HashMap.this.clear();
        }

        public final Iterator<K> iterator() {
            return new KeyIterator();
        }

        public final boolean contains(Object o) {
            return containsKey(o);
        }

        public final boolean remove(Object key) {
            return removeNode(hash(key), key, null, false, true) != null;
        }

        public final Spliterator<K> spliterator() {
            return new KeySpliterator<>(HashMap.this, 0, -1, 0, 0);
        }

        public final void forEach(Consumer<? super K> action) {
            Node<K, V>[] tab;
            if (action == null)
                throw new NullPointerException();
            if (size > 0 && (tab = table) != null) {
                int mc = modCount;
                for (int i = 0; i < tab.length; ++i) {
                    for (Node<K, V> e = tab[i]; e != null; e = e.next)
                        action.accept(e.key);
                }
                if (modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * Returns a {@link Collection} view of the values contained in this map.
     * The collection is backed by the map, so changes to the map are
     * reflected in the collection, and vice-versa.  If the map is
     * modified while an iteration over the collection is in progress
     * (except through the iterator's own <tt>remove</tt> operation),
     * the results of the iteration are undefined.  The collection
     * supports element removal, which removes the corresponding
     * mapping from the map, via the <tt>Iterator.remove</tt>,
     * <tt>Collection.remove</tt>, <tt>removeAll</tt>,
     * <tt>retainAll</tt> and <tt>clear</tt> operations.  It does not
     * support the <tt>add</tt> or <tt>addAll</tt> operations.
     *
     * @return a view of the values contained in this map
     */
    public Collection<V> values() {
        Collection<V> vs = values;
        if (vs == null) {
            vs = new Values();
            values = vs;
        }
        return vs;
    }

    final class Values extends AbstractCollection<V> {
        public final int size() {
            return size;
        }

        public final void clear() {
            HashMap.this.clear();
        }

        public final Iterator<V> iterator() {
            return new ValueIterator();
        }

        public final boolean contains(Object o) {
            return containsValue(o);
        }

        public final Spliterator<V> spliterator() {
            return new ValueSpliterator<>(HashMap.this, 0, -1, 0, 0);
        }

        public final void forEach(Consumer<? super V> action) {
            Node<K, V>[] tab;
            if (action == null)
                throw new NullPointerException();
            if (size > 0 && (tab = table) != null) {
                int mc = modCount;
                for (int i = 0; i < tab.length; ++i) {
                    for (Node<K, V> e = tab[i]; e != null; e = e.next)
                        action.accept(e.value);
                }
                if (modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * Returns a {@link Set} view of the mappings contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own <tt>remove</tt> operation, or through the
     * <tt>setValue</tt> operation on a map entry returned by the
     * iterator) the results of the iteration are undefined.  The set
     * supports element removal, which removes the corresponding
     * mapping from the map, via the <tt>Iterator.remove</tt>,
     * <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt> and
     * <tt>clear</tt> operations.  It does not support the
     * <tt>add</tt> or <tt>addAll</tt> operations.
     *
     * @return a set view of the mappings contained in this map
     */
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> es;
        return (es = entrySet) == null ? (entrySet = new EntrySet()) : es;
    }

    final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
        public final int size() {
            return size;
        }

        public final void clear() {
            HashMap.this.clear();
        }

        public final Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator();
        }

        public final boolean contains(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
            Object key = e.getKey();
            Node<K, V> candidate = getNode(hash(key), key);
            return candidate != null && candidate.equals(e);
        }

        public final boolean remove(Object o) {
            if (o instanceof Map.Entry) {
                Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
                Object key = e.getKey();
                Object value = e.getValue();
                return removeNode(hash(key), key, value, true, true) != null;
            }
            return false;
        }

        public final Spliterator<Map.Entry<K, V>> spliterator() {
            return new EntrySpliterator<>(HashMap.this, 0, -1, 0, 0);
        }

        public final void forEach(Consumer<? super Map.Entry<K, V>> action) {
            Node<K, V>[] tab;
            if (action == null)
                throw new NullPointerException();
            if (size > 0 && (tab = table) != null) {
                int mc = modCount;
                for (int i = 0; i < tab.length; ++i) {
                    for (Node<K, V> e = tab[i]; e != null; e = e.next)
                        action.accept(e);
                }
                if (modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }
    }

    // Overrides of JDK8 Map extension methods

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        Node<K, V> e;
        return (e = getNode(hash(key), key)) == null ? defaultValue : e.value;
    }

    @Override
    public V putIfAbsent(K key, V value) {
        return putVal(hash(key), key, value, true, true);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return removeNode(hash(key), key, value, true, true) != null;
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        Node<K, V> e;
        V v;
        if ((e = getNode(hash(key), key)) != null &&
                ((v = e.value) == oldValue || (v != null && v.equals(oldValue)))) {
            e.value = newValue;
            afterNodeAccess(e);
            return true;
        }
        return false;
    }

    @Override
    public V replace(K key, V value) {
        Node<K, V> e;
        if ((e = getNode(hash(key), key)) != null) {
            V oldValue = e.value;
            e.value = value;
            afterNodeAccess(e);
            return oldValue;
        }
        return null;
    }

    @Override
    public V computeIfAbsent(K key,
                             Function<? super K, ? extends V> mappingFunction) {
        if (mappingFunction == null)
            throw new NullPointerException();
        int hash = hash(key);
        Node<K, V>[] tab;
        Node<K, V> first;
        int n, i;
        int binCount = 0;
        TreeNode<K, V> t = null;
        Node<K, V> old = null;
        if (size > threshold || (tab = table) == null ||
                (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((first = tab[i = (n - 1) & hash]) != null) {
            if (first instanceof TreeNode)
                old = (t = (TreeNode<K, V>) first).getTreeNode(hash, key);
            else {
                Node<K, V> e = first;
                K k;
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        old = e;
                        break;
                    }
                    ++binCount;
                } while ((e = e.next) != null);
            }
            V oldValue;
            if (old != null && (oldValue = old.value) != null) {
                afterNodeAccess(old);
                return oldValue;
            }
        }
        V v = mappingFunction.apply(key);
        if (v == null) {
            return null;
        } else if (old != null) {
            old.value = v;
            afterNodeAccess(old);
            return v;
        } else if (t != null)
            t.putTreeVal(this, tab, hash, key, v);
        else {
            tab[i] = newNode(hash, key, v, first);
            if (binCount >= TREEIFY_THRESHOLD - 1)
                treeifyBin(tab, hash);
        }
        ++modCount;
        ++size;
        afterNodeInsertion(true);
        return v;
    }

    public V computeIfPresent(K key,
                              BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        if (remappingFunction == null)
            throw new NullPointerException();
        Node<K, V> e;
        V oldValue;
        int hash = hash(key);
        if ((e = getNode(hash, key)) != null &&
                (oldValue = e.value) != null) {
            V v = remappingFunction.apply(key, oldValue);
            if (v != null) {
                e.value = v;
                afterNodeAccess(e);
                return v;
            } else
                removeNode(hash, key, null, false, true);
        }
        return null;
    }

    @Override
    public V compute(K key,
                     BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        if (remappingFunction == null)
            throw new NullPointerException();
        int hash = hash(key);
        Node<K, V>[] tab;
        Node<K, V> first;
        int n, i;
        int binCount = 0;
        TreeNode<K, V> t = null;
        Node<K, V> old = null;
        if (size > threshold || (tab = table) == null ||
                (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((first = tab[i = (n - 1) & hash]) != null) {
            if (first instanceof TreeNode)
                old = (t = (TreeNode<K, V>) first).getTreeNode(hash, key);
            else {
                Node<K, V> e = first;
                K k;
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        old = e;
                        break;
                    }
                    ++binCount;
                } while ((e = e.next) != null);
            }
        }
        V oldValue = (old == null) ? null : old.value;
        V v = remappingFunction.apply(key, oldValue);
        if (old != null) {
            if (v != null) {
                old.value = v;
                afterNodeAccess(old);
            } else
                removeNode(hash, key, null, false, true);
        } else if (v != null) {
            if (t != null)
                t.putTreeVal(this, tab, hash, key, v);
            else {
                tab[i] = newNode(hash, key, v, first);
                if (binCount >= TREEIFY_THRESHOLD - 1)
                    treeifyBin(tab, hash);
            }
            ++modCount;
            ++size;
            afterNodeInsertion(true);
        }
        return v;
    }

    @Override
    public V merge(K key, V value,
                   BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        if (value == null)
            throw new NullPointerException();
        if (remappingFunction == null)
            throw new NullPointerException();
        int hash = hash(key);
        Node<K, V>[] tab;
        Node<K, V> first;
        int n, i;
        int binCount = 0;
        TreeNode<K, V> t = null;
        Node<K, V> old = null;
        if (size > threshold || (tab = table) == null ||
                (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((first = tab[i = (n - 1) & hash]) != null) {
            if (first instanceof TreeNode)
                old = (t = (TreeNode<K, V>) first).getTreeNode(hash, key);
            else {
                Node<K, V> e = first;
                K k;
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        old = e;
                        break;
                    }
                    ++binCount;
                } while ((e = e.next) != null);
            }
        }
        if (old != null) {
            V v;
            if (old.value != null)
                v = remappingFunction.apply(old.value, value);
            else
                v = value;
            if (v != null) {
                old.value = v;
                afterNodeAccess(old);
            } else
                removeNode(hash, key, null, false, true);
            return v;
        }
        if (value != null) {
            if (t != null)
                t.putTreeVal(this, tab, hash, key, value);
            else {
                tab[i] = newNode(hash, key, value, first);
                if (binCount >= TREEIFY_THRESHOLD - 1)
                    treeifyBin(tab, hash);
            }
            ++modCount;
            ++size;
            afterNodeInsertion(true);
        }
        return value;
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> action) {
        Node<K, V>[] tab;
        if (action == null)
            throw new NullPointerException();
        if (size > 0 && (tab = table) != null) {
            int mc = modCount;
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next)
                    action.accept(e.key, e.value);
            }
            if (modCount != mc)
                throw new ConcurrentModificationException();
        }
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        Node<K, V>[] tab;
        if (function == null)
            throw new NullPointerException();
        if (size > 0 && (tab = table) != null) {
            int mc = modCount;
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    e.value = function.apply(e.key, e.value);
                }
            }
            if (modCount != mc)
                throw new ConcurrentModificationException();
        }
    }

    /* ------------------------------------------------------------ */
    // Cloning and serialization

    /**
     * Returns a shallow copy of this <tt>HashMap</tt> instance: the keys and
     * values themselves are not cloned.
     *
     * @return a shallow copy of this map
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object clone() {
        HashMap<K, V> result;
        try {
            result = (HashMap<K, V>) super.clone();
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError(e);
        }
        result.reinitialize();
        result.putMapEntries(this, false);
        return result;
    }

    // These methods are also used when serializing HashSets
    final float loadFactor() {
        return loadFactor;
    }

    final int capacity() {
        return (table != null) ? table.length :
                (threshold > 0) ? threshold :
                        DEFAULT_INITIAL_CAPACITY;
    }

    /**
     * Save the state of the <tt>HashMap</tt> instance to a stream (i.e.,
     * serialize it).
     *
     * @serialData The <i>capacity</i> of the HashMap (the length of the
     * bucket array) is emitted (int), followed by the
     * <i>size</i> (an int, the number of key-value
     * mappings), followed by the key (Object) and value (Object)
     * for each key-value mapping.  The key-value mappings are
     * emitted in no particular order.
     */
    private void writeObject(java.io.ObjectOutputStream s)
            throws IOException {
        int buckets = capacity();
        // Write out the threshold, loadfactor, and any hidden stuff
        s.defaultWriteObject();
        s.writeInt(buckets);
        s.writeInt(size);
        internalWriteEntries(s);
    }

    /**
     * Reconstitute the {@code HashMap} instance from a stream (i.e.,
     * deserialize it).
     */
    private void readObject(java.io.ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        // Read in the threshold (ignored), loadfactor, and any hidden stuff
        s.defaultReadObject();
        reinitialize();
        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new InvalidObjectException("Illegal load factor: " +
                    loadFactor);
        s.readInt();                // Read and ignore number of buckets
        int mappings = s.readInt(); // Read number of mappings (size)
        if (mappings < 0)
            throw new InvalidObjectException("Illegal mappings count: " +
                    mappings);
        else if (mappings > 0) { // (if zero, use defaults)
            // Size the table using given load factor only if within
            // range of 0.25...4.0
            float lf = Math.min(Math.max(0.25f, loadFactor), 4.0f);
            float fc = (float) mappings / lf + 1.0f;
            int cap = ((fc < DEFAULT_INITIAL_CAPACITY) ?
                    DEFAULT_INITIAL_CAPACITY :
                    (fc >= MAXIMUM_CAPACITY) ?
                            MAXIMUM_CAPACITY :
                            tableSizeFor((int) fc));
            float ft = (float) cap * lf;
            threshold = ((cap < MAXIMUM_CAPACITY && ft < MAXIMUM_CAPACITY) ?
                    (int) ft : Integer.MAX_VALUE);

            // Check Map.Entry[].class since it's the nearest public type to
            // what we're actually creating.
            SharedSecrets.getJavaOISAccess().checkArray(s, Map.Entry[].class, cap);
            @SuppressWarnings({"rawtypes", "unchecked"})
            Node<K, V>[] tab = (Node<K, V>[]) new Node[cap];
            table = tab;

            // Read the keys and values, and put the mappings in the HashMap
            for (int i = 0; i < mappings; i++) {
                @SuppressWarnings("unchecked")
                K key = (K) s.readObject();
                @SuppressWarnings("unchecked")
                V value = (V) s.readObject();
                putVal(hash(key), key, value, false, false);
            }
        }
    }

    /* ------------------------------------------------------------ */
    // iterators

    abstract class HashIterator {
        Node<K, V> next;        // next entry to return
        Node<K, V> current;     // current entry
        int expectedModCount;  // for fast-fail
        int index;             // current slot

        HashIterator() {
            expectedModCount = modCount;
            Node<K, V>[] t = table;
            current = next = null;
            index = 0;
            if (t != null && size > 0) { // advance to first entry
                do {
                } while (index < t.length && (next = t[index++]) == null);
            }
        }

        public final boolean hasNext() {
            return next != null;
        }

        final Node<K, V> nextNode() {
            Node<K, V>[] t;
            Node<K, V> e = next;
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
            if (e == null)
                throw new NoSuchElementException();
            if ((next = (current = e).next) == null && (t = table) != null) {
                do {
                } while (index < t.length && (next = t[index++]) == null);
            }
            return e;
        }

        public final void remove() {
            Node<K, V> p = current;
            if (p == null)
                throw new IllegalStateException();
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
            current = null;
            K key = p.key;
            removeNode(hash(key), key, null, false, false);
            expectedModCount = modCount;
        }
    }

    final class KeyIterator extends HashIterator
            implements Iterator<K> {
        public final K next() {
            return nextNode().key;
        }
    }

    final class ValueIterator extends HashIterator
            implements Iterator<V> {
        public final V next() {
            return nextNode().value;
        }
    }

    final class EntryIterator extends HashIterator
            implements Iterator<Map.Entry<K, V>> {
        public final Map.Entry<K, V> next() {
            return nextNode();
        }
    }

    /* ------------------------------------------------------------ */
    // spliterators

    static class HashMapSpliterator<K, V> {
        final HashMap<K, V> map;
        Node<K, V> current;          // current node
        int index;                  // current index, modified on advance/split
        int fence;                  // one past last index
        int est;                    // size estimate
        int expectedModCount;       // for comodification checks

        HashMapSpliterator(HashMap<K, V> m, int origin,
                           int fence, int est,
                           int expectedModCount) {
            this.map = m;
            this.index = origin;
            this.fence = fence;
            this.est = est;
            this.expectedModCount = expectedModCount;
        }

        final int getFence() { // initialize fence and size on first use
            int hi;
            if ((hi = fence) < 0) {
                HashMap<K, V> m = map;
                est = m.size;
                expectedModCount = m.modCount;
                Node<K, V>[] tab = m.table;
                hi = fence = (tab == null) ? 0 : tab.length;
            }
            return hi;
        }

        public final long estimateSize() {
            getFence(); // force init
            return (long) est;
        }
    }

    static final class KeySpliterator<K, V>
            extends HashMapSpliterator<K, V>
            implements Spliterator<K> {
        KeySpliterator(HashMap<K, V> m, int origin, int fence, int est,
                       int expectedModCount) {
            super(m, origin, fence, est, expectedModCount);
        }

        public KeySpliterator<K, V> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid || current != null) ? null :
                    new KeySpliterator<>(map, lo, index = mid, est >>>= 1,
                            expectedModCount);
        }

        public void forEachRemaining(Consumer<? super K> action) {
            int i, hi, mc;
            if (action == null)
                throw new NullPointerException();
            HashMap<K, V> m = map;
            Node<K, V>[] tab = m.table;
            if ((hi = fence) < 0) {
                mc = expectedModCount = m.modCount;
                hi = fence = (tab == null) ? 0 : tab.length;
            } else
                mc = expectedModCount;
            if (tab != null && tab.length >= hi &&
                    (i = index) >= 0 && (i < (index = hi) || current != null)) {
                Node<K, V> p = current;
                current = null;
                do {
                    if (p == null)
                        p = tab[i++];
                    else {
                        action.accept(p.key);
                        p = p.next;
                    }
                } while (p != null || i < hi);
                if (m.modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }

        public boolean tryAdvance(Consumer<? super K> action) {
            int hi;
            if (action == null)
                throw new NullPointerException();
            Node<K, V>[] tab = map.table;
            if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                while (current != null || index < hi) {
                    if (current == null)
                        current = tab[index++];
                    else {
                        K k = current.key;
                        current = current.next;
                        action.accept(k);
                        if (map.modCount != expectedModCount)
                            throw new ConcurrentModificationException();
                        return true;
                    }
                }
            }
            return false;
        }

        public int characteristics() {
            return (fence < 0 || est == map.size ? Spliterator.SIZED : 0) |
                    Spliterator.DISTINCT;
        }
    }

    static final class ValueSpliterator<K, V>
            extends HashMapSpliterator<K, V>
            implements Spliterator<V> {
        ValueSpliterator(HashMap<K, V> m, int origin, int fence, int est,
                         int expectedModCount) {
            super(m, origin, fence, est, expectedModCount);
        }

        public ValueSpliterator<K, V> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid || current != null) ? null :
                    new ValueSpliterator<>(map, lo, index = mid, est >>>= 1,
                            expectedModCount);
        }

        public void forEachRemaining(Consumer<? super V> action) {
            int i, hi, mc;
            if (action == null)
                throw new NullPointerException();
            HashMap<K, V> m = map;
            Node<K, V>[] tab = m.table;
            if ((hi = fence) < 0) {
                mc = expectedModCount = m.modCount;
                hi = fence = (tab == null) ? 0 : tab.length;
            } else
                mc = expectedModCount;
            if (tab != null && tab.length >= hi &&
                    (i = index) >= 0 && (i < (index = hi) || current != null)) {
                Node<K, V> p = current;
                current = null;
                do {
                    if (p == null)
                        p = tab[i++];
                    else {
                        action.accept(p.value);
                        p = p.next;
                    }
                } while (p != null || i < hi);
                if (m.modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }

        public boolean tryAdvance(Consumer<? super V> action) {
            int hi;
            if (action == null)
                throw new NullPointerException();
            Node<K, V>[] tab = map.table;
            if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                while (current != null || index < hi) {
                    if (current == null)
                        current = tab[index++];
                    else {
                        V v = current.value;
                        current = current.next;
                        action.accept(v);
                        if (map.modCount != expectedModCount)
                            throw new ConcurrentModificationException();
                        return true;
                    }
                }
            }
            return false;
        }

        public int characteristics() {
            return (fence < 0 || est == map.size ? Spliterator.SIZED : 0);
        }
    }

    static final class EntrySpliterator<K, V>
            extends HashMapSpliterator<K, V>
            implements Spliterator<Map.Entry<K, V>> {
        EntrySpliterator(HashMap<K, V> m, int origin, int fence, int est,
                         int expectedModCount) {
            super(m, origin, fence, est, expectedModCount);
        }

        public EntrySpliterator<K, V> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid || current != null) ? null :
                    new EntrySpliterator<>(map, lo, index = mid, est >>>= 1,
                            expectedModCount);
        }

        public void forEachRemaining(Consumer<? super Map.Entry<K, V>> action) {
            int i, hi, mc;
            if (action == null)
                throw new NullPointerException();
            HashMap<K, V> m = map;
            Node<K, V>[] tab = m.table;
            if ((hi = fence) < 0) {
                mc = expectedModCount = m.modCount;
                hi = fence = (tab == null) ? 0 : tab.length;
            } else
                mc = expectedModCount;
            if (tab != null && tab.length >= hi &&
                    (i = index) >= 0 && (i < (index = hi) || current != null)) {
                Node<K, V> p = current;
                current = null;
                do {
                    if (p == null)
                        p = tab[i++];
                    else {
                        action.accept(p);
                        p = p.next;
                    }
                } while (p != null || i < hi);
                if (m.modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }

        public boolean tryAdvance(Consumer<? super Map.Entry<K, V>> action) {
            int hi;
            if (action == null)
                throw new NullPointerException();
            Node<K, V>[] tab = map.table;
            if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                while (current != null || index < hi) {
                    if (current == null)
                        current = tab[index++];
                    else {
                        Node<K, V> e = current;
                        current = current.next;
                        action.accept(e);
                        if (map.modCount != expectedModCount)
                            throw new ConcurrentModificationException();
                        return true;
                    }
                }
            }
            return false;
        }

        public int characteristics() {
            return (fence < 0 || est == map.size ? Spliterator.SIZED : 0) |
                    Spliterator.DISTINCT;
        }
    }

    /* ------------------------------------------------------------ */
    // LinkedHashMap support


    /*
     * The following package-protected methods are designed to be
     * overridden by LinkedHashMap, but not by any other subclass.
     * Nearly all other internal methods are also package-protected
     * but are declared final, so can be used by LinkedHashMap, view
     * classes, and HashSet.
     */

    // 调用构造方法 创建一个常规的新节点。
    Node<K, V> newNode(int hash, K key, V value, Node<K, V> next) {
        return new Node<>(hash, key, value, next);
    }

    // For conversion from TreeNodes to plain nodes
    Node<K, V> replacementNode(Node<K, V> p, Node<K, V> next) {
        return new Node<>(p.hash, p.key, p.value, next);
    }

    // Create a tree bin node
    TreeNode<K, V> newTreeNode(int hash, K key, V value, Node<K, V> next) {
        return new TreeNode<>(hash, key, value, next);
    }

    //提供给 treeifyBin 调用
    TreeNode<K, V> replacementTreeNode(Node<K, V> p, Node<K, V> next) {
        //构造一个新的树节点  取p的 hash值、键、值作为自身属性。 取 next 初始化为下一个节点
        return new TreeNode<>(p.hash, p.key, p.value, next);
    }

    /**
     * Reset to initial default state.  Called by clone and readObject.
     */
    void reinitialize() {
        table = null;
        entrySet = null;
        keySet = null;
        values = null;
        modCount = 0;
        threshold = 0;
        size = 0;
    }

    // Callbacks to allow LinkedHashMap post-actions
    void afterNodeAccess(Node<K, V> p) {
    }

    void afterNodeInsertion(boolean evict) {
    }

    void afterNodeRemoval(Node<K, V> p) {
    }

    // Called only from writeObject, to ensure compatible ordering.
    void internalWriteEntries(java.io.ObjectOutputStream s) throws IOException {
        Node<K, V>[] tab;
        if (size > 0 && (tab = table) != null) {
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    s.writeObject(e.key);
                    s.writeObject(e.value);
                }
            }
        }
    }

    /* ------------------------------------------------------------ */
    //  树形桶结构

    /**
     * 继承了 LinkedHashMap.Entry类 (这个类继承了HashMap.Node节点)
     * 所以这个TreeNode 可以用作常规节点或者 链表节点的扩展。
     * <p>
     * <p>
     * <p>
     * 红黑树必须要知道的：
     * 定义： 红黑树是一颗自平衡的二叉查找树，它为每个节点增加一个颜色存储位（红或黑）。通过对任何一条从根到叶子的简单路径上各个节点的颜色进行约束，
     * 红黑树确保没有一条路径会比其他路径长出两倍，因而是近似于平衡的。
     * 【红黑树是一种自平衡二叉查找树，是计算机科学领域中的一种数据结构，典型的用途是实现关联数组，存储有序的数据】
     * 性质:
     * 1.每个节点的颜色非黑即红。
     * 2.如果一个节点是红色的，那么它两个子节点一定是黑色的。
     * 3.根节点是黑色的。
     * 4.每个叶子节点是黑色的。
     * 5.对每个节点，从该节点到其所有后代叶子节点的简单路径上，均包含相同数目的黑色节点。
     * <p>
     * 操作：
     * 1.旋转： 左旋 、右旋。
     * 左旋：父亲是红色 叔叔是黑色，当前是右子树，则进行左旋。
     * 右旋：父亲是红色，叔叔是黑色，当前是左子树，则进行右旋。
     * 2.变色： 如果当前节点父亲和叔叔都是红色，则当前节点的父亲和叔叔均变为黑色，爷爷变为红色。
     * 3.插入： 插入时需要按照指定规则通过 左旋、右旋变色三种动作来保证红黑树结构变化后仍然遵守规则。
     * 4.删除：查找到节点后删除，然后自平衡调整。
     */
    static final class TreeNode<K, V> extends LinkedHashMap.Entry<K, V> {
        TreeNode<K, V> parent; //红黑树的链接
        TreeNode<K, V> left; //左子树
        TreeNode<K, V> right; //右子树
        TreeNode<K, V> prev; // needed to unlink next upon deletion
        boolean red;

        TreeNode(int hash, K key, V val, Node<K, V> next) {
            super(hash, key, val, next);
        }

        /**
         * Returns root of tree containing this node.
         * 返回包含这个节点的树 的根节点。
         */
        final TreeNode<K, V> root() {
            for (TreeNode<K, V> r = this, p; ; ) {
                //当前树节点的 parent 赋值给 p，然后判断 p是否为null
                if ((p = r.parent) == null)
                    //为null则返回当前树节点
                    return r;
                //否则将 p 赋值给 r(等价于将 r.parent 赋值给 r，意思是一直往上找，直到父节点是null，那么代表当前的节点就是根节点）
                r = p;
            }
        }

        /**
         * 确保给定的节点是桶中的根节点。 此方法仅在 treeify() 和 putTreeVal()中调用过。具体做什么还不明确。
         */
        static <K, V> void moveRootToFront(Node<K, V>[] tab, TreeNode<K, V> root) {
            int n;
            //根节点不是空  表结构不为空 表长度不小于0
            if (root != null && tab != null && (n = tab.length) > 0) {
                //取下标
                int index = (n - 1) & root.hash;
                //取出桶内下标的第一个元素
                TreeNode<K, V> first = (TreeNode<K, V>) tab[index];
                if (root != first) {
                    // 判断传入的 root 节点不是首节点
                    //声明一个 rn 节点
                    Node<K, V> rn;
                    //将传入的root节点置为根节点
                    tab[index] = root;

                    //接收 prev 节点
                    TreeNode<K, V> rp = root.prev;

                    //当前root 的next节点如果不为空 则将next节点赋值给rn
                    if ((rn = root.next) != null)
                        //将rp赋值给rn
                        ((TreeNode<K, V>) rn).prev = rp;
                    if (rp != null)
                        //rp不为空  将 rn 赋值给rp的next节点
                        rp.next = rn;
                    if (first != null)
                        //Hash槽内第一个节点不为null  则将root节点赋值给第一个节点的 prev对象。
                        first.prev = root;
                    //将值你的first节点 赋值给当前root节点的 next 对象
                    root.next = first;
                    //将当前root的prev对象置为空
                    root.prev = null;
                }
                assert checkInvariants(root);
            }
        }

        /**
         * 使用给定的root节点的hash值 和 键对象从根节点p开始查找节点。
         * kc 参数在第一次使用比较键时缓存 comparableClassFor（键）
         */
        final TreeNode<K, V> find(int h, Object k, Class<?> kc) {
            //当前节点视为 root节点
            TreeNode<K, V> p = this;
            do {
                //声明 root 的hash值 ph 和   dir
                int ph, dir;
                //声明parent的key
                K pk;

                //声明root左子树 pl  和 右子树 pr，和 q？？
                TreeNode<K, V> pl = p.left, pr = p.right, q;
                //valid 1 传入的 hash值h 小于 根节点root，则往左侧继续查找。
                if ((ph = p.hash) > h)
                    //根节点挪到原根节点的 左子树
                    p = pl;
                    //valid 2 传入的hash值h 大于 根节点root， 则往右侧继续查找。
                else if (ph < h)
                    //根节点挪到原根节点的 右子树
                    p = pr;
                    // valid 3 如果根节点的 key和传入的 节点键k相同，或者 传入的k不为空且 k等于根节点 pk
                else if ((pk = p.key) == k || (k != null && k.equals(pk)))
                    return p;
                    //如果左子树为null
                else if (pl == null)
                    //p = 右子树
                    p = pr;
                    //valid 4如果右子树为null
                else if (pr == null)
                    //p = 左子树
                    p = pl;
                    //valid 5 如果 kc 不是null 并且 kc
                else if ((kc != null ||
                        //找到 实现了Comparable 接口的运行时类型且不为空
                        //并且 比较传入的k 和 kc 和 root节点的key，如果存在可比性则继续
                        (kc = comparableClassFor(k)) != null) &&
                        (dir = compareComparables(kc, k, pk)) != 0)
                    //比较结果小 则取左子树 大则取右子树
                    p = (dir < 0) ? pl : pr;
                    //valid 6 右子树中递归调用查找方法找到了对应节点 且不为空 则返回 该节点。
                else if ((q = pr.find(h, k, kc)) != null)
                    return q;
                else
                    //或者 根节点取 左子树
                    p = pl;
            } while (p != null);
            return null;
        }

        /**
         * 调用此方法从根节点开始查找指定节点。
         */
        final TreeNode<K, V> getTreeNode(int h, Object k) {
            //如果有父节点 则先查找到根节点，否则调用自身find方法，参数为 hash值，和键对象
            return ((parent != null) ? root() : this).find(h, k, null);
        }

        /**
         * 当哈希码相等且无法比较时，用于对插入进行排序。我们不需要总的顺序，只需要一个一致的插入规则来在重新平衡
         * 中保持等价。此方法进一步简化了测试。
         * <p>
         * 此方法只在 treeify（）和 putTreeVal（）中调用。
         */
        static int tieBreakOrder(Object a, Object b) {
            //声明两个 对象 a，b 并进行互相比较
            int d;

            if (a == null || b == null || (d = a.getClass().getName().compareTo(b.getClass().getName())) == 0)
                //valid 1 如果 有任何一个对象为空， 或者 两个对象的类名完全相同
                //比较 a、b 两个对象的 hashcode 是否有 a <= b 有则返回- 1 否则 返回 1。
                d = (System.identityHashCode(a) <= System.identityHashCode(b) ?
                        -1 : 1);
            //返回 d 的值
            return d;
        }

        /**
         * 形成从该节点开始的红黑树.
         *
         * @return root of tree
         */
        final void treeify(Node<K, V>[] tab) {
            //声明根节点
            TreeNode<K, V> root = null;

            //持续取值，从当前节点开始 不断地把next节点赋值给 x
            for (TreeNode<K, V> x = this, next; x != null; x = next) {
                //取next节点的 next节点 赋值给next。
                next = (TreeNode<K, V>) x.next;

                //清空 x的左、右子树。
                x.left = x.right = null;

                if (root == null) {
                    //valid 1 root 节点为 null  则将x父节点置为空，颜色标记为黑色。
                    x.parent = null;
                    x.red = false;
                    //将x视为根节点
                    root = x;
                } else {
                    //root不为空 说明已经设置好了根节点
                    //取 x节点key作为键 k
                    K k = x.key;
                    //取 x节点hash码作为 哈希值 h
                    int h = x.hash;
                    //声明 kc
                    Class<?> kc = null;
                    //死循环遍历节点
                    for (TreeNode<K, V> p = root; ; ) {
                        //声明 p节点，将root引用给 p
                        //声明dir 和 p的hash值 ph。
                        int dir, ph;
                        //声明 p的 键 pk
                        K pk = p.key;
                        //valid 2 当前节点x的hash值小于 父节点 的hash值，则将x节点放到左子树
                        if ((ph = p.hash) > h)
                            //比较结果设置为 小于
                            dir = -1;
                            //valid 3 当前节点x 的hash值大于 父节点的hash值，则将x节点放到右子树
                        else if (ph < h)
                            //比较结果设置为 大于
                            dir = 1;
                            //valid 4 判断连个key是否实现了 Comparable接口，如果实现，则比较一下。得到结果dir
                        else if ((kc == null &&
                                (kc = comparableClassFor(k)) == null) ||
                                (dir = compareComparables(kc, k, pk)) == 0)
                            dir = tieBreakOrder(k, pk);

                        //声明  xp节点，将父节点给xp
                        TreeNode<K, V> xp = p;

                        //根据比较的结果 -1 则 放左子树 1 则放右子树
                        if ((p = (dir <= 0) ? p.left : p.right) == null) {
                            //valid 5 如果发现比较结果放置时，对应节点并没有左子树或右子树，则根据大小关系放置该节点作为对应的左子树或右子树
                            x.parent = xp;
                            if (dir <= 0)
                                xp.left = x;
                            else
                                xp.right = x;

                            //返回调整后的根节点
                            root = balanceInsertion(root, x);
                            break;
                        }
                    }
                }
            }
            moveRootToFront(tab, root);
        }

        /**
         * Returns a list of non-TreeNodes replacing those linked from
         * this node.
         */
        final Node<K, V> untreeify(HashMap<K, V> map) {
            Node<K, V> hd = null, tl = null;
            for (Node<K, V> q = this; q != null; q = q.next) {
                Node<K, V> p = map.replacementNode(q, null);
                if (tl == null)
                    hd = p;
                else
                    tl.next = p;
                tl = p;
            }
            return hd;
        }

        /**
         * Tree version of putVal.
         */
        final TreeNode<K, V> putTreeVal(HashMap<K, V> map, Node<K, V>[] tab, int h, K k, V v) {
            Class<?> kc = null;
            boolean searched = false;
            TreeNode<K, V> root = (parent != null) ? root() : this;
            for (TreeNode<K, V> p = root; ; ) {
                int dir, ph;
                K pk;
                if ((ph = p.hash) > h)
                    dir = -1;
                else if (ph < h)
                    dir = 1;
                else if ((pk = p.key) == k || (k != null && k.equals(pk)))
                    return p;
                else if ((kc == null &&
                        (kc = comparableClassFor(k)) == null) ||
                        (dir = compareComparables(kc, k, pk)) == 0) {
                    if (!searched) {
                        TreeNode<K, V> q, ch;
                        searched = true;
                        if (((ch = p.left) != null &&
                                (q = ch.find(h, k, kc)) != null) ||
                                ((ch = p.right) != null &&
                                        (q = ch.find(h, k, kc)) != null))
                            return q;
                    }
                    dir = tieBreakOrder(k, pk);
                }

                TreeNode<K, V> xp = p;
                if ((p = (dir <= 0) ? p.left : p.right) == null) {
                    Node<K, V> xpn = xp.next;
                    TreeNode<K, V> x = map.newTreeNode(h, k, v, xpn);
                    if (dir <= 0)
                        xp.left = x;
                    else
                        xp.right = x;
                    xp.next = x;
                    x.parent = x.prev = xp;
                    if (xpn != null)
                        ((TreeNode<K, V>) xpn).prev = x;
                    moveRootToFront(tab, balanceInsertion(root, x));
                    return null;
                }
            }
        }

        /**
         * Removes the given node, that must be present before this call.
         * This is messier than typical red-black deletion code because we
         * cannot swap the contents of an interior node with a leaf
         * successor that is pinned by "next" pointers that are accessible
         * independently during traversal. So instead we swap the tree
         * linkages. If the current tree appears to have too few nodes,
         * the bin is converted back to a plain bin. (The test triggers
         * somewhere between 2 and 6 nodes, depending on tree structure).
         */
        final void removeTreeNode(HashMap<K, V> map, Node<K, V>[] tab,
                                  boolean movable) {
            int n;
            if (tab == null || (n = tab.length) == 0)
                return;
            int index = (n - 1) & hash;
            TreeNode<K, V> first = (TreeNode<K, V>) tab[index], root = first, rl;
            TreeNode<K, V> succ = (TreeNode<K, V>) next, pred = prev;
            if (pred == null)
                tab[index] = first = succ;
            else
                pred.next = succ;
            if (succ != null)
                succ.prev = pred;
            if (first == null)
                return;
            if (root.parent != null)
                root = root.root();
            if (root == null || root.right == null ||
                    (rl = root.left) == null || rl.left == null) {
                tab[index] = first.untreeify(map);  // too small
                return;
            }
            TreeNode<K, V> p = this, pl = left, pr = right, replacement;
            if (pl != null && pr != null) {
                TreeNode<K, V> s = pr, sl;
                while ((sl = s.left) != null) // find successor
                    s = sl;
                boolean c = s.red;
                s.red = p.red;
                p.red = c; // swap colors
                TreeNode<K, V> sr = s.right;
                TreeNode<K, V> pp = p.parent;
                if (s == pr) { // p was s's direct parent
                    p.parent = s;
                    s.right = p;
                } else {
                    TreeNode<K, V> sp = s.parent;
                    if ((p.parent = sp) != null) {
                        if (s == sp.left)
                            sp.left = p;
                        else
                            sp.right = p;
                    }
                    if ((s.right = pr) != null)
                        pr.parent = s;
                }
                p.left = null;
                if ((p.right = sr) != null)
                    sr.parent = p;
                if ((s.left = pl) != null)
                    pl.parent = s;
                if ((s.parent = pp) == null)
                    root = s;
                else if (p == pp.left)
                    pp.left = s;
                else
                    pp.right = s;
                if (sr != null)
                    replacement = sr;
                else
                    replacement = p;
            } else if (pl != null)
                replacement = pl;
            else if (pr != null)
                replacement = pr;
            else
                replacement = p;
            if (replacement != p) {
                TreeNode<K, V> pp = replacement.parent = p.parent;
                if (pp == null)
                    root = replacement;
                else if (p == pp.left)
                    pp.left = replacement;
                else
                    pp.right = replacement;
                p.left = p.right = p.parent = null;
            }

            TreeNode<K, V> r = p.red ? root : balanceDeletion(root, replacement);

            if (replacement == p) {  // detach
                TreeNode<K, V> pp = p.parent;
                p.parent = null;
                if (pp != null) {
                    if (p == pp.left)
                        pp.left = null;
                    else if (p == pp.right)
                        pp.right = null;
                }
            }
            if (movable)
                moveRootToFront(tab, r);
        }

        /**
         * Splits nodes in a tree bin into lower and upper tree bins,
         * or untreeifies if now too small. Called only from resize;
         * see above discussion about split bits and indices.
         *
         * @param map   the map
         * @param tab   the table for recording bin heads
         * @param index the index of the table being split
         * @param bit   the bit of hash to split on
         */
        final void split(HashMap<K, V> map, Node<K, V>[] tab, int index, int bit) {
            TreeNode<K, V> b = this;
            // Relink into lo and hi lists, preserving order
            TreeNode<K, V> loHead = null, loTail = null;
            TreeNode<K, V> hiHead = null, hiTail = null;
            int lc = 0, hc = 0;
            for (TreeNode<K, V> e = b, next; e != null; e = next) {
                next = (TreeNode<K, V>) e.next;
                e.next = null;
                if ((e.hash & bit) == 0) {
                    if ((e.prev = loTail) == null)
                        loHead = e;
                    else
                        loTail.next = e;
                    loTail = e;
                    ++lc;
                } else {
                    if ((e.prev = hiTail) == null)
                        hiHead = e;
                    else
                        hiTail.next = e;
                    hiTail = e;
                    ++hc;
                }
            }

            if (loHead != null) {
                if (lc <= UNTREEIFY_THRESHOLD)
                    tab[index] = loHead.untreeify(map);
                else {
                    tab[index] = loHead;
                    if (hiHead != null) // (else is already treeified)
                        loHead.treeify(tab);
                }
            }
            if (hiHead != null) {
                if (hc <= UNTREEIFY_THRESHOLD)
                    tab[index + bit] = hiHead.untreeify(map);
                else {
                    tab[index + bit] = hiHead;
                    if (loHead != null)
                        hiHead.treeify(tab);
                }
            }
        }

        /* ------------------------------------------------------------ */
        // Red-black tree methods, all adapted from CLR

        //左旋
        static <K, V> TreeNode<K, V> rotateLeft(TreeNode<K, V> root,
                                                TreeNode<K, V> p) {
            TreeNode<K, V> r, pp, rl;
            if (p != null && (r = p.right) != null) {
                if ((rl = p.right = r.left) != null)
                    rl.parent = p;
                if ((pp = r.parent = p.parent) == null)
                    (root = r).red = false;
                else if (pp.left == p)
                    pp.left = r;
                else
                    pp.right = r;
                r.left = p;
                p.parent = r;
            }
            return root;
        }

        //右旋
        static <K, V> TreeNode<K, V> rotateRight(TreeNode<K, V> root,
                                                 TreeNode<K, V> p) {
            TreeNode<K, V> l, pp, lr;
            if (p != null && (l = p.left) != null) {
                if ((lr = p.left = l.right) != null)
                    lr.parent = p;
                if ((pp = l.parent = p.parent) == null)
                    (root = l).red = false;
                else if (pp.right == p)
                    pp.right = l;
                else
                    pp.left = l;
                l.right = p;
                p.parent = l;
            }
            return root;
        }

        /**
         * @return java.util.HashMap.TreeNode<K, V>
         * @author chentl
         * @description 自平衡的插入
         * @date 2021/4/7 11:21 上午
         * @params [root, x]
         */
        static <K, V> TreeNode<K, V> balanceInsertion(TreeNode<K, V> root,
                                                      TreeNode<K, V> x) {
            //性质1： 插入的节点一定是红色
            x.red = true;
            //声明 xp  xp的父亲xpp，xp父亲的左子树 xppl，xp父亲的右子树 xppr  死循环遍历
            for (TreeNode<K, V> xp, xpp, xppl, xppr; ; ) {
                //valid 1 当前节点的父亲的引用给到 x，结果发现并没有父节点，那么当前节点将被视为根节点，颜色设置黑色。
                if ((xp = x.parent) == null) {
                    //将当前节点 设置为黑色
                    x.red = false;
                    //返回当前节点 作为根节点
                    return x;
                } else if (!xp.red || (xpp = xp.parent) == null)
                    //valid 2
                    return root;
                if (xp == (xppl = xpp.left)) {
                    if ((xppr = xpp.right) != null && xppr.red) {
                        xppr.red = false;
                        xp.red = false;
                        xpp.red = true;
                        x = xpp;
                    } else {
                        if (x == xp.right) {
                            root = rotateLeft(root, x = xp);
                            xpp = (xp = x.parent) == null ? null : xp.parent;
                        }
                        if (xp != null) {
                            xp.red = false;
                            if (xpp != null) {
                                xpp.red = true;
                                root = rotateRight(root, xpp);
                            }
                        }
                    }
                } else {
                    if (xppl != null && xppl.red) {
                        xppl.red = false;
                        xp.red = false;
                        xpp.red = true;
                        x = xpp;
                    } else {
                        if (x == xp.left) {
                            root = rotateRight(root, x = xp);
                            xpp = (xp = x.parent) == null ? null : xp.parent;
                        }
                        if (xp != null) {
                            xp.red = false;
                            if (xpp != null) {
                                xpp.red = true;
                                root = rotateLeft(root, xpp);
                            }
                        }
                    }
                }
            }
        }

        /**
         * @return java.util.HashMap.TreeNode<K, V>
         * @author chentl
         * @description 自平衡的删除
         * @date 2021/4/7 11:22 上午
         * @params [root, x]
         */
        static <K, V> TreeNode<K, V> balanceDeletion(TreeNode<K, V> root,
                                                     TreeNode<K, V> x) {
            for (TreeNode<K, V> xp, xpl, xpr; ; ) {
                if (x == null || x == root)
                    return root;
                else if ((xp = x.parent) == null) {
                    x.red = false;
                    return x;
                } else if (x.red) {
                    x.red = false;
                    return root;
                } else if ((xpl = xp.left) == x) {
                    if ((xpr = xp.right) != null && xpr.red) {
                        xpr.red = false;
                        xp.red = true;
                        root = rotateLeft(root, xp);
                        xpr = (xp = x.parent) == null ? null : xp.right;
                    }
                    if (xpr == null)
                        x = xp;
                    else {
                        TreeNode<K, V> sl = xpr.left, sr = xpr.right;
                        if ((sr == null || !sr.red) &&
                                (sl == null || !sl.red)) {
                            xpr.red = true;
                            x = xp;
                        } else {
                            if (sr == null || !sr.red) {
                                if (sl != null)
                                    sl.red = false;
                                xpr.red = true;
                                root = rotateRight(root, xpr);
                                xpr = (xp = x.parent) == null ?
                                        null : xp.right;
                            }
                            if (xpr != null) {
                                xpr.red = (xp == null) ? false : xp.red;
                                if ((sr = xpr.right) != null)
                                    sr.red = false;
                            }
                            if (xp != null) {
                                xp.red = false;
                                root = rotateLeft(root, xp);
                            }
                            x = root;
                        }
                    }
                } else { // symmetric
                    if (xpl != null && xpl.red) {
                        xpl.red = false;
                        xp.red = true;
                        root = rotateRight(root, xp);
                        xpl = (xp = x.parent) == null ? null : xp.left;
                    }
                    if (xpl == null)
                        x = xp;
                    else {
                        TreeNode<K, V> sl = xpl.left, sr = xpl.right;
                        if ((sl == null || !sl.red) &&
                                (sr == null || !sr.red)) {
                            xpl.red = true;
                            x = xp;
                        } else {
                            if (sl == null || !sl.red) {
                                if (sr != null)
                                    sr.red = false;
                                xpl.red = true;
                                root = rotateLeft(root, xpl);
                                xpl = (xp = x.parent) == null ?
                                        null : xp.left;
                            }
                            if (xpl != null) {
                                xpl.red = (xp == null) ? false : xp.red;
                                if ((sl = xpl.left) != null)
                                    sl.red = false;
                            }
                            if (xp != null) {
                                xp.red = false;
                                root = rotateRight(root, xp);
                            }
                            x = root;
                        }
                    }
                }
            }
        }

        /**
         * 递归不变检验方法 、 传入树的根节点即可开始检验
         */
        static <K, V> boolean checkInvariants(TreeNode<K, V> t) {
            //取父亲、左子树、右子树、祖父节点和链表额下一节点
            TreeNode<K, V> tp = t.parent,
                    tl = t.left,
                    tr = t.right,
                    tb = t.prev,
                    tn = (TreeNode<K, V>) t.next;

            //祖父节点不为空 且 祖父节点的next不是自己。  返回 检查失败
            if (tb != null && tb.next != t)
                return false;
            //next节点不为空，且next节点的祖父节点不是自己  ？？？。 返回 检查失败
            if (tn != null && tn.prev != t)
                return false;
            //父亲不为空 并且 当前节点 既不是父亲的左子树也不是右子树。 返回 检查失败
            if (tp != null && t != tp.left && t != tp.right)
                return false;
            //左子树不为空 并且  左子树的父亲不是自己 或者 左子树的hash值大于自己的hash值(左边应该小于右边)。      返回检查失败。
            if (tl != null && (tl.parent != t || tl.hash > t.hash))
                return false;
            //右子树不为空 并且 右子树得到父亲不是自己 或者 右子树的hash值小于自己的hash值（右边应该总大于左边）。  返回检查失败。
            if (tr != null && (tr.parent != t || tr.hash < t.hash))
                return false;
            if (t.red && tl != null && tl.red && tr != null && tr.red)
                return false;
            if (tl != null && !checkInvariants(tl))
                return false;
            if (tr != null && !checkInvariants(tr))
                return false;
            return true;
        }
    }

}
