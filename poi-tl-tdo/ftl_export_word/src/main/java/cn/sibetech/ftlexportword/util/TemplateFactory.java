package cn.sibetech.ftlexportword.util;

import com.alibaba.fastjson.JSONObject;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 导出模板
 *
 * @author hexl
 * @since 21/05/24
 */
public class TemplateFactory {

    private static Logger log = LoggerFactory.getLogger(TemplateFactory.class);

    private static final String ENCODING = "utf-8";

    private static Configuration conf = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);

    private static Map<String, Template> tempMap = new HashMap<>();

    private static final String TEMPLATE_PATH = "C://templateWord";

    static {
        try {
            conf.setDirectoryForTemplateLoading(new File(TEMPLATE_PATH));
            conf.setDefaultEncoding(ENCODING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过模板文件名称获取指定模板
     */
    private static Template getTemplateByName(String name) throws IOException {
        if (tempMap.containsKey(name)) {
            log.debug("the template is already exist in the map,template name :" + name);
            // 缓存中有该模板直接返回
            return tempMap.get(name);
        }
        // 缓存中没有该模板时，生成新模板并放入缓存中
        Template temp = conf.getTemplate(name, TemplateFactory.ENCODING);
        tempMap.put(name, temp);
        log.debug("the template is not found  in the map,template name :" + name);
        return temp;
    }

    public static void createDoc(Map<String, Object> dataMap, String templateName, String fileName) {
        File outFile = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(outFile);
             OutputStreamWriter oWriter = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
             Writer out = new BufferedWriter(oWriter)) {
            Template t = getTemplateByName(templateName);
            if (t != null) {
                t.process(dataMap, out);
            }
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            JSONObject jsonData = JSONObject.parseObject("{\"address\":\"武汉市江汉区圈外创智中心\",\"faren\":\"Livis\",\"idCard\":\"423094822217364744\",\"deviceList\":[{\"sbllb\":\"巨幕投影仪\",\"mcjxh\":\"NPXL001\",\"num\":\"11\",\"sccj\":\"大风车\",\"ccny\":\"2021年5月24日\",\"mqzk\":\"正常\"},{\"sbllb\":\"承重轴\",\"mcjxh\":\"PK123\",\"num\":\"39\",\"sccj\":\"大风车\",\"ccny\":\"2021年5月24日\",\"mqzk\":\"正常\"},{\"sbllb\":\"幕布\",\"mcjxh\":\"TT001\",\"num\":\"10\",\"sccj\":\"大风车\",\"ccny\":\"2021年5月24日\",\"mqzk\":\"正常\"}],\"managers\":[{\"mName\":\"张三\",\"mSex\":\"13\",\"mContact\":\"13333333333\"},{\"mName\":\"李四\",\"mSex\":\"35\",\"mContact\":\"14444444444\"},{\"mName\":\"王五\",\"mSex\":\"29\",\"mContact\":\"122222222222\"}],\"tList\":[{\"tName\":\"张2\",\"tSex\":\"22\",\"tRemark\":\"备注测试1\"},{\"tName\":\"李2\",\"tSex\":\"11\",\"tRemark\":\"被注册时2\"},{\"tName\":\"王2\",\"tSex\":\"44\",\"tRemark\":\"被注册时3\"}]}");

            Map<String, Object> map = new HashMap<>(16);
            List<Map<String, String>> list = new ArrayList<>();
            Map<String, String> person1 = new HashMap<>(1);
            person1.put("fqr", "张三");
            Map<String, String> person2 = new HashMap<>(1);
            person2.put("fqr", "李四");
            Map<String, String> person3 = new HashMap<>(1);
            person3.put("fqr", "王五");
            list.add(person1);
            list.add(person2);
            list.add(person3);
            map.put("xm", "admin");
            map.put("data", jsonData);
            createDoc(map, "freemarker-test.ftl", "C://templateWord//test.docx");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
