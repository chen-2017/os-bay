<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties"
                              Target="docProps/custom.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId6"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"
                              Target="numbering.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml"
                              Target="../customXml/item1.xml"/>
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                    xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                    mc:Ignorable="w14 w15 wp14">
                <w:body>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="3"/>
                            <w:spacing w:after="0" w:line="500" w:lineRule="exact"/>
                            <w:ind w:left="0" w:leftChars="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:sz w:val="44"/>
                                <w:szCs w:val="44"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:sz w:val="44"/>
                                <w:szCs w:val="44"/>
                            </w:rPr>
                            <w:t>公司登记（备案）申请书</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="7"/>
                            <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text" w:horzAnchor="page"
                                      w:tblpX="1052" w:tblpY="244"/>
                            <w:tblOverlap w:val="never"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="0" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:top w:w="0" w:type="dxa"/>
                                <w:left w:w="108" w:type="dxa"/>
                                <w:bottom w:w="0" w:type="dxa"/>
                                <w:right w:w="108" w:type="dxa"/>
                            </w:tblCellMar>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="2032"/>
                            <w:gridCol w:w="2145"/>
                            <w:gridCol w:w="1350"/>
                            <w:gridCol w:w="4477"/>
                        </w:tblGrid>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="549" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10004" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□基本信息（必填项）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="638" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>名　　称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                                                                          </w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>(集团母公司需填写：集团名称： 集团简称： )</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="670" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>统一社会信用代码（设立登记不填写）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="1692" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>住</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">　  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>所</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50" w:line="480" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>省（市</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>自治区）</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>市（</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>地区</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>盟</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>自治州）</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>县（</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>自治县</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>旗</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>自治旗</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="begin"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:instrText xml:space="preserve"> HYPERLINK &quot;http://baike.baidu.com/view/175012.htm&quot; </w:instrText>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="separate"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>市</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="end"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="begin"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:instrText xml:space="preserve"> HYPERLINK &quot;http://baike.baidu.com/view/267478.htm&quot; </w:instrText>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="separate"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>区</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="end"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>乡（民族乡</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>镇</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>街道）</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>村（路</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>社区）</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>号</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50" w:line="480" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>_______________________________________________________________________
                                        </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="688" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>联系电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2145" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>邮政编码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4477" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="534" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10004" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□设立（仅限</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>设立登记填写</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="744" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> 法定代表人</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">姓 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>　</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2145" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>公司类型</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4477" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="210" w:firstLineChars="100"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">有限责任公司   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">股份有限公司 </w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="210" w:firstLineChars="100"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>外资有限责任公司</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>外资股份有限公司</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="664" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>注册资本</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">       </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>万</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>元</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">         （</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>币种：</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>人民币</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□其他</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">            </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="684" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>投资总额</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>外资</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>公司填写</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">       </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">       </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>万元（币种：</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>） 折</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>美元：</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">         </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>万元</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="710" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>设立方式</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>（股份公司填写）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2145" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□发起设立</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□募集设立</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>营业期限</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>经营期限</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4477" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="210" w:firstLineChars="100"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>长期</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">            </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">       </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>年</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="632" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>申领执照</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>申领</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>纸质执照</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>其中：副本</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>个（电子执照系统自动生成，纸质执照自行勾选）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="2812" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2032" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>经营范围</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>（根据《国民经济行业分类》、有关规定和</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>公司</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>章程填写）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7972" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="bottom"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:after="156" w:afterLines="50"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>(申请人须根据企业自身情况填写《</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>企业登记</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>政府部门共享信息表》相关</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>内容</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>。)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p>
                        <w:pPr>
                            <w:autoSpaceDE w:val="0"/>
                            <w:autoSpaceDN w:val="0"/>
                            <w:adjustRightInd w:val="0"/>
                            <w:spacing w:line="320" w:lineRule="exact"/>
                            <w:ind w:left="-202" w:leftChars="-108" w:hanging="25" w:hangingChars="12"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                            <w:t>注：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>1、</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                            <w:t>本申请</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>书适用于内资、外资公司</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                            <w:t>申请设立、变更、备案。</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:autoSpaceDE w:val="0"/>
                            <w:autoSpaceDN w:val="0"/>
                            <w:adjustRightInd w:val="0"/>
                            <w:spacing w:line="320" w:lineRule="exact"/>
                            <w:ind w:left="-199" w:leftChars="-95" w:firstLine="409" w:firstLineChars="195"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>2、</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                            <w:t>申请书应当使用</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                            <w:t>A4</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="zh-CN"/>
                            </w:rPr>
                            <w:t>纸。依本表打印生成的，使用黑色墨水钢笔或签字笔签署；手工填写的，使用黑色墨水钢笔或签字笔工整填写、签署。</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="7"/>
                            <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text"
                                      w:tblpXSpec="center" w:tblpY="1"/>
                            <w:tblOverlap w:val="never"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:top w:w="0" w:type="dxa"/>
                                <w:left w:w="108" w:type="dxa"/>
                                <w:bottom w:w="0" w:type="dxa"/>
                                <w:right w:w="108" w:type="dxa"/>
                            </w:tblCellMar>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1459"/>
                            <w:gridCol w:w="41"/>
                            <w:gridCol w:w="1339"/>
                            <w:gridCol w:w="73"/>
                            <w:gridCol w:w="1142"/>
                            <w:gridCol w:w="977"/>
                            <w:gridCol w:w="748"/>
                            <w:gridCol w:w="60"/>
                            <w:gridCol w:w="315"/>
                            <w:gridCol w:w="2040"/>
                            <w:gridCol w:w="1805"/>
                        </w:tblGrid>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="646" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:tabs>
                                            <w:tab w:val="left" w:pos="4508"/>
                                        </w:tabs>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> □变更（仅限</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>变更登记填写，只填写与本次申请有关的事项</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>变更事项</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>原登记内容</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>变更</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>后</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>登记内容</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="340" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="方正书宋简体" w:hAnsi="新宋体" w:eastAsia="方正书宋简体"/>
                                            <w:color w:val="000000"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="794" w:hRule="exact"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4380" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4160" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="1396" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>注：变更</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>事项</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>
                                            包括名称、住所、法定代表人（姓名）、注册资本、公司类型、经营范围、营业期限/经营期限、有限责任公司股东（股东姓名或者名称）、股份有限公司发起人的姓名或者名称。
                                        </w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:strike/>
                                            <w:color w:val="C00000"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>申请公司名称变更，在名称中增加“集团或（集团）”字样的，应当填写集团名称、集团简称（无集团简称的可不填）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="588" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:eastAsia="黑体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□备案（仅限</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>备案登记填写</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="1033" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:highlight w:val="yellow"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">事  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>项</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8540" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="360" w:lineRule="auto"/>
                                        <w:ind w:firstLine="210" w:firstLineChars="100"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□董事</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□监事</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□经理</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> □章程</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> □章程修正案</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="360" w:lineRule="auto"/>
                                        <w:ind w:firstLine="210" w:firstLineChars="100"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:strike/>
                                            <w:szCs w:val="21"/>
                                            <w:highlight w:val="yellow"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□联络员</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                             □外国投资者法律文件送达接受人</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="1015" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>清</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>算</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>组</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:spacing w:val="-11"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>(清算委员会)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1453" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>成　 员</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7087" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="973" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:vMerge w:val="continue"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1453" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>负</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>责</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>人</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2119" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1123" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>联系电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3845" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="600" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="141" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>指定代表</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>委托代理人（必填项）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="1759" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1500" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>委托权限</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8499" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>1、同意□不同意□核对登记材料中的复印件并签署核对意见；</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>2、同意□不同意□修改企业自备文件的错误；</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>3、同意□不同意□修改有关表格的填写错误；</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>4、同意□不同意□领取营业执照和有关文书。</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="645" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1500" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>固定电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1339" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="380" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1215" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="340" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>移动电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1725" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2415" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:spacing w:val="-11"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>指定代表/委托代理人签字</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1805" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="105" w:firstLineChars="50"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="3911" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:spacing w:before="156" w:beforeLines="50"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（指定代表或者委托代理人身份证件复、影印件粘贴处）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="华文中宋" w:hAnsi="华文中宋"
                                                      w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="3073" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>全体股东签字或盖章（仅限内资、外资有限责任公司设立登记）：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>董事会成员签字（仅限内资、外资股份有限公司设立登记）：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="664" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:autoSpaceDE w:val="0"/>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□申请人承诺（</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>必填项</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="黑体" w:hAnsi="宋体" w:eastAsia="黑体"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="2" w:space="0"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="3016" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9999" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="18" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:spacing w:line="440" w:lineRule="exact"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>本申请人和签字人承诺提交的材料文件和填报的信息真实有效，并承担相应的法律责任。</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="440" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="FF0000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="440" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>法定代表人签字（限设立、变更及清算组备案以外的备案）：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="440" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>清算组负责人签字（限清算组备案）：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                                                </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="0000FF"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>公司盖章</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:ind w:firstLine="2940" w:firstLineChars="1400"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                        </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">          </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>年</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>月</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>日</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="520" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:ascii="华文中宋" w:hAnsi="华文中宋" w:eastAsia="华文中宋"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>附表</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>1</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="520" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>法定代表人信息</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="520" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:b/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>本表适用于设立及变更法定代表人填写。</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="7"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:top w:w="0" w:type="dxa"/>
                                <w:left w:w="108" w:type="dxa"/>
                                <w:bottom w:w="0" w:type="dxa"/>
                                <w:right w:w="108" w:type="dxa"/>
                            </w:tblCellMar>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="2144"/>
                            <w:gridCol w:w="3189"/>
                            <w:gridCol w:w="1515"/>
                            <w:gridCol w:w="3097"/>
                        </w:tblGrid>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="801" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2144" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3189" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="default" w:ascii="宋体" w:eastAsia="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                        </w:rPr>
                                        <w:t>${xm}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1515" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>国别（地区）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3097" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="814" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2144" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>职 务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3189" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□董事长</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□执行董事</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>□经理</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1515" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>产生方式</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3097" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="829" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2144" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件类型</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3189" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1515" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件号码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3097" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="803" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2144" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>固定电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3189" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1515" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>移动电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3097" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="812" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2144" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>住 所</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3189" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1515" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>电子邮箱</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3097" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="4817" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9945" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（身份证件复、影印件粘贴处）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="2972" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9945" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>拟任</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>法定代表人签字：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:wordWrap w:val="0"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="right"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">年 月 日       </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:strike/>
                                <w:color w:val="000000"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>附表</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>2</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>董事、监事、经理信息</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="320" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>(担任法定代表人的董事长、执行董事、经理不重复填写)</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="7"/>
                            <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text"
                                      w:horzAnchor="margin" w:tblpX="-110" w:tblpY="136"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="0" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:top w:w="0" w:type="dxa"/>
                                <w:left w:w="108" w:type="dxa"/>
                                <w:bottom w:w="0" w:type="dxa"/>
                                <w:right w:w="108" w:type="dxa"/>
                            </w:tblCellMar>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="9946"/>
                        </w:tblGrid>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="454" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9946" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   国别(地区)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>___________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   身份证件类型</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>_____________________</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件号码</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  职务</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  产生方式</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（身份证件复、影印件粘贴处）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:ind w:right="44" w:rightChars="21"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>注：1、“职务”指董事长（执行董事）、董事、经理、监事会主席、监事。上市股份有限公司设置独立董事的应在“职务”栏内注明。</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:numPr>
                                            <w:ilvl w:val="0"/>
                                            <w:numId w:val="1"/>
                                        </w:numPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>“产生方式”按照章程规定填写，董事、监事一般应为“选举”或“委派”；经理一般应为“聘任”。中外合资（合作）企业应当明确上述人员的委派方。</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="454" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9946" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   国别(地区)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>___________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   身份证件类型</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>_____________________</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件号码</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  职务</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  产生方式</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（身份证件复、影印件粘贴处）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>备注事项同上</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="454" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9946" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   国别(地区)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>___________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   身份证件类型</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>_____________________</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件号码</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  职务</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>________________</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  产生方式</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t>____________________</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（身份证件复、影印件粘贴处）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="500" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> 备注事项同上</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="320" w:lineRule="exact"/>
                                        <w:ind w:left="420" w:leftChars="200"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:color w:val="7E7E7E"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:ind w:left="176" w:leftChars="84" w:right="44" w:rightChars="21" w:firstLine="105"
                                   w:firstLineChars="50"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:sectPr>
                                <w:pgSz w:w="11906" w:h="16838"/>
                                <w:pgMar w:top="1327" w:right="1389" w:bottom="1327" w:left="1389" w:header="851"
                                         w:footer="992" w:gutter="0"/>
                                <w:pgNumType w:start="1"/>
                                <w:cols w:space="720" w:num="1"/>
                                <w:docGrid w:type="lines" w:linePitch="312" w:charSpace="0"/>
                            </w:sectPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="left" w:pos="424"/>
                                <w:tab w:val="center" w:pos="4312"/>
                            </w:tabs>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                <w:bCs/>
                                <w:iCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:bCs/>
                                <w:iCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>附表</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:bCs/>
                                <w:iCs/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>3</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="left" w:pos="424"/>
                                <w:tab w:val="center" w:pos="4312"/>
                            </w:tabs>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:iCs/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:iCs/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>股东（发起人）、外国投资者出资情况</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="7"/>
                            <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text" w:horzAnchor="page"
                                      w:tblpX="819" w:tblpY="370"/>
                            <w:tblOverlap w:val="never"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="0" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:top w:w="0" w:type="dxa"/>
                                <w:left w:w="108" w:type="dxa"/>
                                <w:bottom w:w="0" w:type="dxa"/>
                                <w:right w:w="108" w:type="dxa"/>
                            </w:tblCellMar>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="3989"/>
                            <w:gridCol w:w="1020"/>
                            <w:gridCol w:w="1305"/>
                            <w:gridCol w:w="2880"/>
                            <w:gridCol w:w="1380"/>
                            <w:gridCol w:w="1350"/>
                            <w:gridCol w:w="1425"/>
                            <w:gridCol w:w="1050"/>
                            <w:gridCol w:w="1081"/>
                        </w:tblGrid>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="660" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>股东（发起人）、外国投资者</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>名称或姓名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>国别</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>（地区）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>证件类型</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>证件号码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>认缴出资额</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                            <w:color w:val="0000FF"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>实缴出资额</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>出资（认缴）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                            <w:color w:val="0000FF"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体"/>
                                            <w:bCs/>
                                            <w:color w:val="000000"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>出资</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>方式</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>出资</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:bCs/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>比例</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr><#list dataList as item>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="both"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="default" w:hAnsi="宋体" w:eastAsia="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                            <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                            <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                        </w:rPr>
                                        <w:t>${item.fqr}</w:t>
                                    </w:r>
                                    <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                                    <w:bookmarkEnd w:id="0"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc></w:tr></#list>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="610" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:wAfter w:w="0" w:type="dxa"/>
                                <w:cantSplit/>
                                <w:trHeight w:val="633" w:hRule="atLeast"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3989" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2880" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1380" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1350" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1425" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="top"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:ind w:right="480"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:hAnsi="宋体"/>
                                            <w:iCs/>
                                            <w:sz w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p>
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:bCs/>
                                <w:iCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:bCs/>
                                <w:iCs/>
                            </w:rPr>
                            <w:t xml:space="preserve">                                                                                            </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:bCs/>
                                <w:iCs/>
                            </w:rPr>
                            <w:t>单位：万元（币种：□人民币 □其他________）</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                                <w:strike/>
                                <w:color w:val="FF0000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:sectPr>
                                <w:pgSz w:w="16838" w:h="11906" w:orient="landscape"/>
                                <w:pgMar w:top="1797" w:right="1361" w:bottom="1797" w:left="1361" w:header="851"
                                         w:footer="992" w:gutter="0"/>
                                <w:pgNumType w:start="1"/>
                                <w:cols w:space="720" w:num="1"/>
                                <w:docGrid w:type="lines" w:linePitch="312" w:charSpace="0"/>
                            </w:sectPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="520" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="华文中宋" w:hAnsi="华文中宋"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>附表4</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="auto"/>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="520" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                                <w:b/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:b/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>联络员信息</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="520" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="7"/>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:jc w:val="center"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblCellMar>
                                <w:top w:w="0" w:type="dxa"/>
                                <w:left w:w="108" w:type="dxa"/>
                                <w:bottom w:w="0" w:type="dxa"/>
                                <w:right w:w="108" w:type="dxa"/>
                            </w:tblCellMar>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="3129"/>
                            <w:gridCol w:w="2680"/>
                            <w:gridCol w:w="1782"/>
                            <w:gridCol w:w="2524"/>
                        </w:tblGrid>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="982" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3129" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2680" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1782" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>固定电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2524" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="1027" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3129" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>移动电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2680" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1782" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>电子邮箱</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2524" w:type="dxa"/>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="955" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3129" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件类型</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2680" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1782" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证件号码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2524" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr>
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:left w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:right w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    <w:insideH w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                    <w:insideV w:val="single" w:color="auto" w:sz="6" w:space="0"/>
                                </w:tblBorders>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:left w:w="108" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                    <w:right w:w="108" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:wBefore w:w="0" w:type="dxa"/>
                                <w:trHeight w:val="4493" w:hRule="atLeast"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10115" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                                        <w:bottom w:val="single" w:color="auto" w:sz="12" w:space="0"/>
                                    </w:tcBorders>
                                    <w:noWrap w:val="0"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（身份证件复、影印件粘贴处）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="2730" w:firstLineChars="1300"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:ind w:firstLine="2730" w:firstLineChars="1300"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p>
                                    <w:pPr>
                                        <w:pStyle w:val="20"/>
                                        <w:spacing w:line="520" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="auto"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="auto"/>
                            <w:ind w:left="-420" w:leftChars="-200" w:right="-531" w:rightChars="-253"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t>
                                注：1、联络员主要负责本企业与企业登记机关的联系沟通，以本人个人信息登录国家企业信用信息公示系统依法向社会公示本企业有关信息等。联络员应了解企业登记相关法规和企业信息公示有关规定。
                            </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="auto"/>
                            <w:ind w:left="10" w:leftChars="5" w:firstLine="10" w:firstLineChars="5"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                            </w:rPr>
                            <w:t>2、《联络员信息》未变更的不需重填。</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="auto"/>
                            <w:ind w:firstLine="420"/>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="3"/>
                            <w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
                            <w:ind w:left="0" w:leftChars="0" w:right="97" w:rightChars="46"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="20"/>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="华文中宋" w:hAnsi="华文中宋"/>
                                <w:color w:val="FF0000"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>附表5</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="3"/>
                            <w:spacing w:after="0" w:line="400" w:lineRule="exact"/>
                            <w:ind w:left="0" w:leftChars="0" w:right="97" w:rightChars="46"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>承</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>诺</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="36"/>
                                <w:szCs w:val="36"/>
                            </w:rPr>
                            <w:t>书</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="600" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="600" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>_______________________________</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t>（登记机关名称）：</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="600" w:lineRule="exact"/>
                            <w:ind w:firstLine="600" w:firstLineChars="200"/>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋_GB2312" w:eastAsia="仿宋_GB2312"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t>        </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">            </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">    </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                                <w:u w:val="single"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t>（企业名称）</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t>
                                郑重承诺：登记机关已告知相关审批事项和审批部门。在领取营业执照后，本企业将及时到审批部门办理审批手续，在取得行政审批前不从事相关经营活动。如有超出登记经营范围从事后置审批事项经营的需要，也将先行办理经营范围变更登记和相应审批手续，未取得相关审批前不从事相关经营活动。
                            </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="600" w:lineRule="exact"/>
                            <w:ind w:firstLine="600" w:firstLineChars="200"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t>如有违反上述承诺内容情形发生的，愿自行承担相应的法律责任。</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="600" w:lineRule="exact"/>
                            <w:ind w:firstLine="600" w:firstLineChars="200"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="600" w:lineRule="exact"/>
                            <w:ind w:firstLine="600" w:firstLineChars="200"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t xml:space="preserve">签字：                     </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="560" w:lineRule="exact"/>
                            <w:ind w:firstLine="5400" w:firstLineChars="1800"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="560" w:lineRule="exact"/>
                            <w:ind w:firstLine="5400" w:firstLineChars="1800"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t xml:space="preserve">年 </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t xml:space="preserve">   </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t>月</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                <w:color w:val="000000"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t>日</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="560" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="仿宋_GB2312" w:eastAsia="仿宋_GB2312" w:cs="宋体"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="仿宋_GB2312" w:eastAsia="仿宋_GB2312" w:cs="宋体"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="30"/>
                                <w:szCs w:val="30"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:widowControl/>
                            <w:spacing w:line="560" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="仿宋_GB2312" w:eastAsia="仿宋_GB2312" w:cs="宋体"/>
                                <w:kern w:val="0"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>注：1、《承诺书》只在企业设立和经营范围变更时填写。</w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:ind w:firstLine="420"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>
                                2、申请人为公司、非公司企业法人、非公司外商投资企业的，由法定代表人签字，设立时由拟任法定代表人签字；申请人为外国（地区）企业在中国境内从事生产经营活动的，由有权签字人签字；申请人为合伙企业、外商投资合伙企业的，由全体合伙人或委托执行事务合伙人签字；申请人为个人独资企业的，由投资人签字。变更登记时还须加盖公章，外国（地区）企业在中国境内从事生产经营活动除外。
                            </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:ind w:firstLine="420"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>
                                3、有限责任公司和股份有限公司的分公司、非公司企业法人分支机构由隶属企业的法定代表人签字，营业单位由隶属单位的法定代表人签字，个人独资企业分支机构由隶属企业投资人签字，合伙企业分支机构由合伙企业执行事务合伙人或委派代表签字。设立、变更登记时还须加盖隶属企业（单位）公章，外国（地区）企业在中国境内从事生产经营活动除外。
                            </w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia" w:ascii="宋体" w:hAnsi="宋体" w:cs="楷体"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:sectPr>
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1327" w:right="1389" w:bottom="1327" w:left="1389" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="720" w:num="1"/>
                        <w:docGrid w:type="lines" w:linePitch="312" w:charSpace="0"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/_rels/item1.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXmlProps"
                              Target="itemProps1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/item1.xml" pkg:contentType="application/xml">
        <pkg:xmlData>
            <s:customData xmlns="http://www.wps.cn/officeDocument/2013/wpsCustomData"
                          xmlns:s="http://www.wps.cn/officeDocument/2013/wpsCustomData">
                <customSectProps>
                    <customSectPr/>
                    <customSectPr/>
                    <customSectPr/>
                </customSectProps>
            </s:customData>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/itemProps1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.customXmlProperties+xml">
        <pkg:xmlData>
            <ds:datastoreItem ds:itemID="{B1977F7D-205B-4081-913C-38D41E755F92}"
                              xmlns:ds="http://schemas.openxmlformats.org/officeDocument/2006/customXml">
                <ds:schemaRefs>
                    <ds:schemaRef ds:uri="http://www.wps.cn/officeDocument/2013/wpsCustomData"/>
                </ds:schemaRefs>
            </ds:datastoreItem>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal</Template>
                <Company>MS</Company>
                <Pages>9</Pages>
                <Words>680</Words>
                <Characters>3881</Characters>
                <Lines>32</Lines>
                <Paragraphs>9</Paragraphs>
                <TotalTime>1</TotalTime>
                <ScaleCrop>false</ScaleCrop>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>4552</CharactersWithSpaces>
                <Application>WPS Office_11.1.0.10495_F1E327BC-269C-435d-A152-05C5408002CA</Application>
                <DocSecurity>0</DocSecurity>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml"
              pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dcterms:created xsi:type="dcterms:W3CDTF">2017-12-19T12:47:00Z</dcterms:created>
                <dc:creator>lenovo</dc:creator>
                <cp:lastModifiedBy>hexl</cp:lastModifiedBy>
                <cp:lastPrinted>2018-09-29T00:34:00Z</cp:lastPrinted>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2021-05-24T06:39:20Z</dcterms:modified>
                <dc:title>公司登记（备案）申请书</dc:title>
                <cp:revision>7</cp:revision>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/custom.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.custom-properties+xml">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <property fmtid="{D5CDD505-2E9C-101B-9397-08002B2CF9AE}" pid="2" name="KSOProductBuildVer">
                    <vt:lpwstr>2052-11.1.0.10495</vt:lpwstr>
                </property>
                <property fmtid="{D5CDD505-2E9C-101B-9397-08002B2CF9AE}" pid="3" name="ICV">
                    <vt:lpwstr>3E3F4BBAE9A64F8F8C26EF3A6B4ABE2E</vt:lpwstr>
                </property>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     mc:Ignorable="w14">
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="20007A87" w:usb1="80000000" w:usb2="00000008" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000006" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Wingdings">
                    <w:panose1 w:val="05000000000000000000"/>
                    <w:charset w:val="02"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000000" w:usb1="00000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Arial">
                    <w:panose1 w:val="020B0604020202020204"/>
                    <w:charset w:val="01"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="400001FF"
                           w:csb1="FFFF0000"/>
                </w:font>
                <w:font w:name="黑体">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Courier New">
                    <w:panose1 w:val="02070309020205020404"/>
                    <w:charset w:val="01"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C0007843" w:usb2="00000009" w:usb3="00000000" w:csb0="400001FF"
                           w:csb1="FFFF0000"/>
                </w:font>
                <w:font w:name="Symbol">
                    <w:panose1 w:val="05050102010706020507"/>
                    <w:charset w:val="02"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000000" w:usb1="00000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Calibri">
                    <w:panose1 w:val="020F0502020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="E4002EFF" w:usb1="C000247B" w:usb2="00000009" w:usb3="00000000" w:csb0="200001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="方正书宋简体">
                    <w:altName w:val="宋体"/>
                    <w:panose1 w:val="03000509000000000000"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="script"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000001" w:usb1="080E0000" w:usb2="00000010" w:usb3="00000000" w:csb0="00040000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="新宋体">
                    <w:panose1 w:val="02010609030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000283" w:usb1="288F0000" w:usb2="00000006" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="华文中宋">
                    <w:altName w:val="宋体"/>
                    <w:panose1 w:val="02010600040101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000287" w:usb1="080F0000" w:usb2="00000010" w:usb3="00000000" w:csb0="0004009F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋_GB2312">
                    <w:altName w:val="仿宋"/>
                    <w:panose1 w:val="02010609030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000001" w:usb1="080E0000" w:usb2="00000000" w:usb3="00000000" w:csb0="00040000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="楷体">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="微软雅黑">
                    <w:panose1 w:val="020B0503020204020204"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="80000287" w:usb1="2ACF3C50" w:usb2="00000016" w:usb3="00000000" w:csb0="0004001F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Tahoma">
                    <w:panose1 w:val="020B0604030504040204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="E1002EFF" w:usb1="C000605B" w:usb2="00000029" w:usb3="00000000" w:csb0="200101FF"
                           w:csb1="20280000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/numbering.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
        <pkg:xmlData>
            <w:numbering
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                    xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                    mc:Ignorable="w14 wp14">
                <w:abstractNum w:abstractNumId="0">
                    <w:nsid w:val="017CFF5F"/>
                    <w:multiLevelType w:val="singleLevel"/>
                    <w:tmpl w:val="017CFF5F"/>
                    <w:lvl w:ilvl="0" w:tentative="0">
                        <w:start w:val="2"/>
                        <w:numFmt w:val="decimal"/>
                        <w:suff w:val="nothing"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                </w:abstractNum>
                <w:num w:numId="1">
                    <w:abstractNumId w:val="0"/>
                </w:num>
            </w:numbering>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        mc:Ignorable="w14">
                <w:zoom w:percent="130"/>
                <w:bordersDoNotSurroundHeader w:val="1"/>
                <w:bordersDoNotSurroundFooter w:val="1"/>
                <w:stylePaneFormatFilter w:val="3F01" w:allStyles="1" w:customStyles="0" w:latentStyles="0"
                                         w:stylesInUse="0" w:headingStyles="0" w:numberingStyles="0" w:tableStyles="0"
                                         w:directFormattingOnRuns="1" w:directFormattingOnParagraphs="1"
                                         w:directFormattingOnNumbering="1" w:directFormattingOnTables="1"
                                         w:clearFormatting="1" w:top3HeadingStyles="1" w:visibleStyles="0"
                                         w:alternateStyleNames="0"/>
                <w:documentProtection w:enforcement="0"/>
                <w:defaultTabStop w:val="420"/>
                <w:hyphenationZone w:val="360"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:noLineBreaksAfter w:lang="zh-CN" w:val="([{·‘“〈《「『【〔〖（．［｛￡￥"/>
                <w:noLineBreaksBefore w:lang="zh-CN" w:val="!),.:;?]}¨·ˇˉ―‖’”…∶、。〃々〉》」』】〕〗！＂＇），．：；？］｀｜｝～￠"/>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:useNormalStyleForList/>
                    <w:doNotUseIndentAsNumberingTabStop/>
                    <w:useAltKinsokuLineBreakRules/>
                    <w:allowSpaceOfSameStyleInTable/>
                    <w:doNotSuppressIndentation/>
                    <w:doNotAutofitConstrainedTables/>
                    <w:autofitToFirstFixedWidthCell/>
                    <w:underlineTabInNumList/>
                    <w:displayHangulFixedWidth/>
                    <w:splitPgBreakAndParaMark/>
                    <w:doNotVertAlignCellWithSp/>
                    <w:doNotBreakConstrainedForcedTable/>
                    <w:doNotVertAlignInTxbx/>
                    <w:useAnsiKerningPairs/>
                    <w:cachedColBalance/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="11"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="004763F8"/>
                    <w:rsid w:val="00007CCC"/>
                    <w:rsid w:val="00010CF8"/>
                    <w:rsid w:val="00016686"/>
                    <w:rsid w:val="0002671C"/>
                    <w:rsid w:val="0003137E"/>
                    <w:rsid w:val="000351FD"/>
                    <w:rsid w:val="000444B3"/>
                    <w:rsid w:val="00063EDB"/>
                    <w:rsid w:val="00083D18"/>
                    <w:rsid w:val="00085ABC"/>
                    <w:rsid w:val="00090DC6"/>
                    <w:rsid w:val="000B1173"/>
                    <w:rsid w:val="000B29AC"/>
                    <w:rsid w:val="000C3A33"/>
                    <w:rsid w:val="000C46CC"/>
                    <w:rsid w:val="000D23B2"/>
                    <w:rsid w:val="000E3C37"/>
                    <w:rsid w:val="000F5F59"/>
                    <w:rsid w:val="00101D9B"/>
                    <w:rsid w:val="00104977"/>
                    <w:rsid w:val="00106E10"/>
                    <w:rsid w:val="00107AFD"/>
                    <w:rsid w:val="00114807"/>
                    <w:rsid w:val="00120941"/>
                    <w:rsid w:val="00125502"/>
                    <w:rsid w:val="00126626"/>
                    <w:rsid w:val="00126830"/>
                    <w:rsid w:val="00136A98"/>
                    <w:rsid w:val="00143C92"/>
                    <w:rsid w:val="001467F7"/>
                    <w:rsid w:val="00150AC8"/>
                    <w:rsid w:val="0015400D"/>
                    <w:rsid w:val="001564A3"/>
                    <w:rsid w:val="0017131E"/>
                    <w:rsid w:val="0018195C"/>
                    <w:rsid w:val="00185D83"/>
                    <w:rsid w:val="00195EED"/>
                    <w:rsid w:val="001A040D"/>
                    <w:rsid w:val="001A096B"/>
                    <w:rsid w:val="001C4ACC"/>
                    <w:rsid w:val="001C6D04"/>
                    <w:rsid w:val="001D04C4"/>
                    <w:rsid w:val="001D480C"/>
                    <w:rsid w:val="001E2BD0"/>
                    <w:rsid w:val="00203424"/>
                    <w:rsid w:val="00205115"/>
                    <w:rsid w:val="0020660A"/>
                    <w:rsid w:val="00220589"/>
                    <w:rsid w:val="00222D80"/>
                    <w:rsid w:val="002301B8"/>
                    <w:rsid w:val="0023184D"/>
                    <w:rsid w:val="002446A2"/>
                    <w:rsid w:val="00252876"/>
                    <w:rsid w:val="00252C20"/>
                    <w:rsid w:val="00257838"/>
                    <w:rsid w:val="002712DB"/>
                    <w:rsid w:val="002740CF"/>
                    <w:rsid w:val="00280A2F"/>
                    <w:rsid w:val="00283A9C"/>
                    <w:rsid w:val="00296909"/>
                    <w:rsid w:val="00297727"/>
                    <w:rsid w:val="002B4E9D"/>
                    <w:rsid w:val="002C501D"/>
                    <w:rsid w:val="002C65F9"/>
                    <w:rsid w:val="002D22CB"/>
                    <w:rsid w:val="002D27D1"/>
                    <w:rsid w:val="002D570C"/>
                    <w:rsid w:val="002D6E54"/>
                    <w:rsid w:val="002E162F"/>
                    <w:rsid w:val="002E48A4"/>
                    <w:rsid w:val="002F08AC"/>
                    <w:rsid w:val="002F0DEE"/>
                    <w:rsid w:val="002F3C08"/>
                    <w:rsid w:val="00312CEB"/>
                    <w:rsid w:val="003218BC"/>
                    <w:rsid w:val="00327F21"/>
                    <w:rsid w:val="0033432B"/>
                    <w:rsid w:val="00341D78"/>
                    <w:rsid w:val="00345FB5"/>
                    <w:rsid w:val="003530C4"/>
                    <w:rsid w:val="003557CD"/>
                    <w:rsid w:val="00371B3E"/>
                    <w:rsid w:val="00374D25"/>
                    <w:rsid w:val="00381177"/>
                    <w:rsid w:val="003A3DA4"/>
                    <w:rsid w:val="003A456E"/>
                    <w:rsid w:val="003A6190"/>
                    <w:rsid w:val="003B7358"/>
                    <w:rsid w:val="003C42EB"/>
                    <w:rsid w:val="003D1E9D"/>
                    <w:rsid w:val="003D5AED"/>
                    <w:rsid w:val="003E01B0"/>
                    <w:rsid w:val="003E7FBE"/>
                    <w:rsid w:val="0040423B"/>
                    <w:rsid w:val="0040706A"/>
                    <w:rsid w:val="004160E1"/>
                    <w:rsid w:val="00430B2D"/>
                    <w:rsid w:val="00433A4B"/>
                    <w:rsid w:val="00441B4C"/>
                    <w:rsid w:val="00446099"/>
                    <w:rsid w:val="00446D11"/>
                    <w:rsid w:val="00454018"/>
                    <w:rsid w:val="004635E5"/>
                    <w:rsid w:val="0047250B"/>
                    <w:rsid w:val="004763F8"/>
                    <w:rsid w:val="00477C43"/>
                    <w:rsid w:val="00484FE1"/>
                    <w:rsid w:val="004972D6"/>
                    <w:rsid w:val="00497A84"/>
                    <w:rsid w:val="004A0EBA"/>
                    <w:rsid w:val="004A2903"/>
                    <w:rsid w:val="004A2E00"/>
                    <w:rsid w:val="004A4E30"/>
                    <w:rsid w:val="004A5A45"/>
                    <w:rsid w:val="004B191C"/>
                    <w:rsid w:val="004B5677"/>
                    <w:rsid w:val="004B5E1A"/>
                    <w:rsid w:val="004C0181"/>
                    <w:rsid w:val="004C1F5B"/>
                    <w:rsid w:val="004C1F8B"/>
                    <w:rsid w:val="004C38E2"/>
                    <w:rsid w:val="004C52CF"/>
                    <w:rsid w:val="004D3351"/>
                    <w:rsid w:val="004F0878"/>
                    <w:rsid w:val="004F7018"/>
                    <w:rsid w:val="004F7268"/>
                    <w:rsid w:val="00511C05"/>
                    <w:rsid w:val="00513EFC"/>
                    <w:rsid w:val="0051504A"/>
                    <w:rsid w:val="005169A4"/>
                    <w:rsid w:val="005276A0"/>
                    <w:rsid w:val="005277F0"/>
                    <w:rsid w:val="00530491"/>
                    <w:rsid w:val="00532EC4"/>
                    <w:rsid w:val="00541F16"/>
                    <w:rsid w:val="005709F9"/>
                    <w:rsid w:val="005A3787"/>
                    <w:rsid w:val="005A49CD"/>
                    <w:rsid w:val="005A4E0D"/>
                    <w:rsid w:val="005A7B47"/>
                    <w:rsid w:val="005B04AE"/>
                    <w:rsid w:val="005B7266"/>
                    <w:rsid w:val="005C4803"/>
                    <w:rsid w:val="005C4B06"/>
                    <w:rsid w:val="005D0965"/>
                    <w:rsid w:val="005E38C5"/>
                    <w:rsid w:val="005E3A36"/>
                    <w:rsid w:val="005E7BC5"/>
                    <w:rsid w:val="005F1B48"/>
                    <w:rsid w:val="0061485C"/>
                    <w:rsid w:val="0064117F"/>
                    <w:rsid w:val="00672D35"/>
                    <w:rsid w:val="00674D6E"/>
                    <w:rsid w:val="00677966"/>
                    <w:rsid w:val="006901C7"/>
                    <w:rsid w:val="006A7028"/>
                    <w:rsid w:val="006B5ADD"/>
                    <w:rsid w:val="006E00D8"/>
                    <w:rsid w:val="006E14E1"/>
                    <w:rsid w:val="006E1BC6"/>
                    <w:rsid w:val="006F29C6"/>
                    <w:rsid w:val="00701CAE"/>
                    <w:rsid w:val="00706E35"/>
                    <w:rsid w:val="00707C51"/>
                    <w:rsid w:val="00707F12"/>
                    <w:rsid w:val="00710D5B"/>
                    <w:rsid w:val="00713F53"/>
                    <w:rsid w:val="007154DE"/>
                    <w:rsid w:val="00720668"/>
                    <w:rsid w:val="00732D8D"/>
                    <w:rsid w:val="00733CBD"/>
                    <w:rsid w:val="00734EFF"/>
                    <w:rsid w:val="00734FE9"/>
                    <w:rsid w:val="007353B5"/>
                    <w:rsid w:val="00754316"/>
                    <w:rsid w:val="00762984"/>
                    <w:rsid w:val="00766721"/>
                    <w:rsid w:val="00771646"/>
                    <w:rsid w:val="00771AB2"/>
                    <w:rsid w:val="00775B5E"/>
                    <w:rsid w:val="00781610"/>
                    <w:rsid w:val="0078406F"/>
                    <w:rsid w:val="00794746"/>
                    <w:rsid w:val="007B552E"/>
                    <w:rsid w:val="007D4AD0"/>
                    <w:rsid w:val="007D595F"/>
                    <w:rsid w:val="007D613A"/>
                    <w:rsid w:val="007E26D1"/>
                    <w:rsid w:val="007F5F4D"/>
                    <w:rsid w:val="007F643B"/>
                    <w:rsid w:val="00802963"/>
                    <w:rsid w:val="0080704C"/>
                    <w:rsid w:val="00810452"/>
                    <w:rsid w:val="0081285C"/>
                    <w:rsid w:val="00815BAC"/>
                    <w:rsid w:val="00827980"/>
                    <w:rsid w:val="00830676"/>
                    <w:rsid w:val="00843820"/>
                    <w:rsid w:val="00846AA0"/>
                    <w:rsid w:val="008479A8"/>
                    <w:rsid w:val="008635E3"/>
                    <w:rsid w:val="00865447"/>
                    <w:rsid w:val="00872A05"/>
                    <w:rsid w:val="00882164"/>
                    <w:rsid w:val="008823BA"/>
                    <w:rsid w:val="0089287E"/>
                    <w:rsid w:val="0089615E"/>
                    <w:rsid w:val="008B323E"/>
                    <w:rsid w:val="008E110D"/>
                    <w:rsid w:val="008F376A"/>
                    <w:rsid w:val="008F6377"/>
                    <w:rsid w:val="0090092D"/>
                    <w:rsid w:val="00904916"/>
                    <w:rsid w:val="00915AD0"/>
                    <w:rsid w:val="009227CC"/>
                    <w:rsid w:val="009308B9"/>
                    <w:rsid w:val="0094467B"/>
                    <w:rsid w:val="00955139"/>
                    <w:rsid w:val="00977455"/>
                    <w:rsid w:val="00981EAC"/>
                    <w:rsid w:val="0098689E"/>
                    <w:rsid w:val="00995C28"/>
                    <w:rsid w:val="009979F6"/>
                    <w:rsid w:val="009A1FC8"/>
                    <w:rsid w:val="009A409B"/>
                    <w:rsid w:val="009B14B0"/>
                    <w:rsid w:val="009B6908"/>
                    <w:rsid w:val="009C2148"/>
                    <w:rsid w:val="009C4C78"/>
                    <w:rsid w:val="009E18AD"/>
                    <w:rsid w:val="009E6E49"/>
                    <w:rsid w:val="00A003B4"/>
                    <w:rsid w:val="00A06097"/>
                    <w:rsid w:val="00A07D01"/>
                    <w:rsid w:val="00A1594F"/>
                    <w:rsid w:val="00A173FF"/>
                    <w:rsid w:val="00A2390F"/>
                    <w:rsid w:val="00A267FE"/>
                    <w:rsid w:val="00A269E2"/>
                    <w:rsid w:val="00A41C2A"/>
                    <w:rsid w:val="00A44AFB"/>
                    <w:rsid w:val="00A530BF"/>
                    <w:rsid w:val="00A6167B"/>
                    <w:rsid w:val="00A658C1"/>
                    <w:rsid w:val="00A7321F"/>
                    <w:rsid w:val="00A768E1"/>
                    <w:rsid w:val="00AA343E"/>
                    <w:rsid w:val="00AA36EF"/>
                    <w:rsid w:val="00AB0D28"/>
                    <w:rsid w:val="00AB2E3D"/>
                    <w:rsid w:val="00AB3AAD"/>
                    <w:rsid w:val="00AB3F6A"/>
                    <w:rsid w:val="00AC2625"/>
                    <w:rsid w:val="00AC6859"/>
                    <w:rsid w:val="00AE76B9"/>
                    <w:rsid w:val="00AF2661"/>
                    <w:rsid w:val="00AF3618"/>
                    <w:rsid w:val="00AF5297"/>
                    <w:rsid w:val="00B01433"/>
                    <w:rsid w:val="00B01F65"/>
                    <w:rsid w:val="00B02575"/>
                    <w:rsid w:val="00B03095"/>
                    <w:rsid w:val="00B13CBE"/>
                    <w:rsid w:val="00B22BEB"/>
                    <w:rsid w:val="00B244A5"/>
                    <w:rsid w:val="00B32476"/>
                    <w:rsid w:val="00B376C6"/>
                    <w:rsid w:val="00B60C21"/>
                    <w:rsid w:val="00B635B1"/>
                    <w:rsid w:val="00B65A56"/>
                    <w:rsid w:val="00B837A9"/>
                    <w:rsid w:val="00B83E6D"/>
                    <w:rsid w:val="00BA0FE3"/>
                    <w:rsid w:val="00BA7300"/>
                    <w:rsid w:val="00BB04C5"/>
                    <w:rsid w:val="00BB472F"/>
                    <w:rsid w:val="00BB483E"/>
                    <w:rsid w:val="00BB48BE"/>
                    <w:rsid w:val="00BB5BCC"/>
                    <w:rsid w:val="00BC2DDD"/>
                    <w:rsid w:val="00BD6A0F"/>
                    <w:rsid w:val="00BE05AF"/>
                    <w:rsid w:val="00BE7A6A"/>
                    <w:rsid w:val="00C0191D"/>
                    <w:rsid w:val="00C05FE3"/>
                    <w:rsid w:val="00C1087B"/>
                    <w:rsid w:val="00C1134F"/>
                    <w:rsid w:val="00C11709"/>
                    <w:rsid w:val="00C175A5"/>
                    <w:rsid w:val="00C21132"/>
                    <w:rsid w:val="00C22E78"/>
                    <w:rsid w:val="00C24A24"/>
                    <w:rsid w:val="00C25853"/>
                    <w:rsid w:val="00C37BD6"/>
                    <w:rsid w:val="00C479BC"/>
                    <w:rsid w:val="00C543DE"/>
                    <w:rsid w:val="00C60CE5"/>
                    <w:rsid w:val="00C617B4"/>
                    <w:rsid w:val="00CC1C4B"/>
                    <w:rsid w:val="00CD116C"/>
                    <w:rsid w:val="00CD3882"/>
                    <w:rsid w:val="00CE11AF"/>
                    <w:rsid w:val="00CE76F0"/>
                    <w:rsid w:val="00CF6560"/>
                    <w:rsid w:val="00D03C3C"/>
                    <w:rsid w:val="00D0651D"/>
                    <w:rsid w:val="00D10CD5"/>
                    <w:rsid w:val="00D12E94"/>
                    <w:rsid w:val="00D265A4"/>
                    <w:rsid w:val="00D37AFD"/>
                    <w:rsid w:val="00D522AE"/>
                    <w:rsid w:val="00D56A56"/>
                    <w:rsid w:val="00D5784E"/>
                    <w:rsid w:val="00D57C43"/>
                    <w:rsid w:val="00D61C07"/>
                    <w:rsid w:val="00D649B4"/>
                    <w:rsid w:val="00D756C7"/>
                    <w:rsid w:val="00D829DF"/>
                    <w:rsid w:val="00D95E13"/>
                    <w:rsid w:val="00DA6BCB"/>
                    <w:rsid w:val="00DA6E2B"/>
                    <w:rsid w:val="00DB033D"/>
                    <w:rsid w:val="00DC4317"/>
                    <w:rsid w:val="00DD2069"/>
                    <w:rsid w:val="00DD6776"/>
                    <w:rsid w:val="00DE191F"/>
                    <w:rsid w:val="00DE7E3C"/>
                    <w:rsid w:val="00DF2686"/>
                    <w:rsid w:val="00DF7F27"/>
                    <w:rsid w:val="00E13966"/>
                    <w:rsid w:val="00E268B9"/>
                    <w:rsid w:val="00E27958"/>
                    <w:rsid w:val="00E3268B"/>
                    <w:rsid w:val="00E347D5"/>
                    <w:rsid w:val="00E370C1"/>
                    <w:rsid w:val="00E37BA0"/>
                    <w:rsid w:val="00E4095F"/>
                    <w:rsid w:val="00E4636D"/>
                    <w:rsid w:val="00E7218B"/>
                    <w:rsid w:val="00E72EA9"/>
                    <w:rsid w:val="00E82ED0"/>
                    <w:rsid w:val="00E83179"/>
                    <w:rsid w:val="00EC0B9E"/>
                    <w:rsid w:val="00EC20F8"/>
                    <w:rsid w:val="00ED6E72"/>
                    <w:rsid w:val="00EE7437"/>
                    <w:rsid w:val="00EF1F2D"/>
                    <w:rsid w:val="00EF6969"/>
                    <w:rsid w:val="00EF7A43"/>
                    <w:rsid w:val="00F01DA4"/>
                    <w:rsid w:val="00F10CF6"/>
                    <w:rsid w:val="00F1267F"/>
                    <w:rsid w:val="00F13967"/>
                    <w:rsid w:val="00F23149"/>
                    <w:rsid w:val="00F24FBE"/>
                    <w:rsid w:val="00F26760"/>
                    <w:rsid w:val="00F3457D"/>
                    <w:rsid w:val="00F35DB5"/>
                    <w:rsid w:val="00F41ED8"/>
                    <w:rsid w:val="00F43053"/>
                    <w:rsid w:val="00F600F8"/>
                    <w:rsid w:val="00F65D12"/>
                    <w:rsid w:val="00F664D3"/>
                    <w:rsid w:val="00F77BED"/>
                    <w:rsid w:val="00F82686"/>
                    <w:rsid w:val="00F8579A"/>
                    <w:rsid w:val="00F877AE"/>
                    <w:rsid w:val="00FA4A6F"/>
                    <w:rsid w:val="00FB6A71"/>
                    <w:rsid w:val="00FD616E"/>
                    <w:rsid w:val="00FE04C6"/>
                    <w:rsid w:val="00FF27D9"/>
                    <w:rsid w:val="00FF5CDC"/>
                    <w:rsid w:val="00FF7162"/>
                    <w:rsid w:val="01460C82"/>
                    <w:rsid w:val="024B1A7D"/>
                    <w:rsid w:val="057258AF"/>
                    <w:rsid w:val="05BA4DF0"/>
                    <w:rsid w:val="0A962CD5"/>
                    <w:rsid w:val="0B716E94"/>
                    <w:rsid w:val="0C773B4A"/>
                    <w:rsid w:val="0D51609D"/>
                    <w:rsid w:val="0ED7190B"/>
                    <w:rsid w:val="0F3A3FB4"/>
                    <w:rsid w:val="11385DCB"/>
                    <w:rsid w:val="11405431"/>
                    <w:rsid w:val="12D91116"/>
                    <w:rsid w:val="135012E5"/>
                    <w:rsid w:val="136F1EF1"/>
                    <w:rsid w:val="15367BD6"/>
                    <w:rsid w:val="15F95564"/>
                    <w:rsid w:val="165717DC"/>
                    <w:rsid w:val="16BF42BC"/>
                    <w:rsid w:val="19857358"/>
                    <w:rsid w:val="1BA50B24"/>
                    <w:rsid w:val="1ECB6898"/>
                    <w:rsid w:val="21901F37"/>
                    <w:rsid w:val="22513C82"/>
                    <w:rsid w:val="236D0E80"/>
                    <w:rsid w:val="24D4440D"/>
                    <w:rsid w:val="27BA4F1B"/>
                    <w:rsid w:val="28302D2C"/>
                    <w:rsid w:val="2ABE5A33"/>
                    <w:rsid w:val="2AC303B7"/>
                    <w:rsid w:val="2B0A4640"/>
                    <w:rsid w:val="2C1B63D3"/>
                    <w:rsid w:val="2E7A43F2"/>
                    <w:rsid w:val="2F246217"/>
                    <w:rsid w:val="2F2D010E"/>
                    <w:rsid w:val="3235394C"/>
                    <w:rsid w:val="327843F3"/>
                    <w:rsid w:val="331A6CEB"/>
                    <w:rsid w:val="336E1CB6"/>
                    <w:rsid w:val="354D3A1A"/>
                    <w:rsid w:val="39A87A08"/>
                    <w:rsid w:val="39D204D0"/>
                    <w:rsid w:val="3A216D7F"/>
                    <w:rsid w:val="3AA16CEF"/>
                    <w:rsid w:val="3C8366D6"/>
                    <w:rsid w:val="3C863753"/>
                    <w:rsid w:val="3CC86D2B"/>
                    <w:rsid w:val="3D6A3032"/>
                    <w:rsid w:val="3E341F68"/>
                    <w:rsid w:val="40A70A58"/>
                    <w:rsid w:val="475B7823"/>
                    <w:rsid w:val="48637FA1"/>
                    <w:rsid w:val="4B5C24FB"/>
                    <w:rsid w:val="4BEA49E3"/>
                    <w:rsid w:val="4C6B107E"/>
                    <w:rsid w:val="4D681C5E"/>
                    <w:rsid w:val="4E820170"/>
                    <w:rsid w:val="5067570D"/>
                    <w:rsid w:val="5246502E"/>
                    <w:rsid w:val="547B0FDF"/>
                    <w:rsid w:val="55B12492"/>
                    <w:rsid w:val="56D81538"/>
                    <w:rsid w:val="56F34406"/>
                    <w:rsid w:val="594863A4"/>
                    <w:rsid w:val="59EA1FDE"/>
                    <w:rsid w:val="59F0401B"/>
                    <w:rsid w:val="5FEE4360"/>
                    <w:rsid w:val="605160C7"/>
                    <w:rsid w:val="60C24C90"/>
                    <w:rsid w:val="61795B17"/>
                    <w:rsid w:val="631A0BA5"/>
                    <w:rsid w:val="64FB498A"/>
                    <w:rsid w:val="650438E4"/>
                    <w:rsid w:val="672373F2"/>
                    <w:rsid w:val="677E044E"/>
                    <w:rsid w:val="6C461A8B"/>
                    <w:rsid w:val="6C464601"/>
                    <w:rsid w:val="6F5F6547"/>
                    <w:rsid w:val="6FD74373"/>
                    <w:rsid w:val="6FDC47F0"/>
                    <w:rsid w:val="70CB5007"/>
                    <w:rsid w:val="7142411E"/>
                    <w:rsid w:val="71FC34C6"/>
                    <w:rsid w:val="73C9400C"/>
                    <w:rsid w:val="74985F7D"/>
                    <w:rsid w:val="75AD1D0F"/>
                    <w:rsid w:val="766C2024"/>
                    <w:rsid w:val="76D532F6"/>
                    <w:rsid w:val="77426FFA"/>
                    <w:rsid w:val="77847D1E"/>
                    <w:rsid w:val="77A356E7"/>
                    <w:rsid w:val="78473FD5"/>
                    <w:rsid w:val="7A5A70EE"/>
                    <w:rsid w:val="7AC869D7"/>
                    <w:rsid w:val="7AD45A23"/>
                    <w:rsid w:val="7BD16174"/>
                    <w:rsid w:val="7C1E28C6"/>
                    <w:rsid w:val="7D3504AD"/>
                    <w:rsid w:val="7EB362B7"/>
                </w:rsids>
                <m:mathPr>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:uiCompat97To2003/>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:doNotIncludeSubdocsInStats/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      mc:Ignorable="w14">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:eastAsia="宋体" w:cs="Times New Roman"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:count="260" w:defQFormat="0" w:defUnhideWhenUsed="1" w:defSemiHidden="1"
                                w:defUIPriority="99" w:defLockedState="0">
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0"
                                    w:name="Normal"/>
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0"
                                    w:name="heading 1" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 2" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 3" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 4" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 5" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 6" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 7" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 8" w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="heading 9" w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="index 1"/>
                    <w:lsdException w:uiPriority="99" w:name="index 2"/>
                    <w:lsdException w:uiPriority="99" w:name="index 3"/>
                    <w:lsdException w:uiPriority="99" w:name="index 4"/>
                    <w:lsdException w:uiPriority="99" w:name="index 5"/>
                    <w:lsdException w:uiPriority="99" w:name="index 6"/>
                    <w:lsdException w:uiPriority="99" w:name="index 7"/>
                    <w:lsdException w:uiPriority="99" w:name="index 8"/>
                    <w:lsdException w:uiPriority="99" w:name="index 9"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 1" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 2" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 3" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 4" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 5" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 6" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 7" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 8" w:locked="1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="toc 9" w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="Normal Indent"/>
                    <w:lsdException w:uiPriority="99" w:name="footnote text"/>
                    <w:lsdException w:uiPriority="99" w:name="annotation text"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="99" w:name="header"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="99" w:semiHidden="0" w:name="footer"/>
                    <w:lsdException w:uiPriority="99" w:name="index heading"/>
                    <w:lsdException w:qFormat="1" w:uiPriority="0" w:name="caption" w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="table of figures"/>
                    <w:lsdException w:uiPriority="99" w:name="envelope address"/>
                    <w:lsdException w:uiPriority="99" w:name="envelope return"/>
                    <w:lsdException w:uiPriority="99" w:name="footnote reference"/>
                    <w:lsdException w:uiPriority="99" w:name="annotation reference"/>
                    <w:lsdException w:uiPriority="99" w:name="line number"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="99" w:semiHidden="0" w:name="page number"/>
                    <w:lsdException w:uiPriority="99" w:name="endnote reference"/>
                    <w:lsdException w:uiPriority="99" w:name="endnote text"/>
                    <w:lsdException w:uiPriority="99" w:name="table of authorities"/>
                    <w:lsdException w:uiPriority="99" w:name="macro"/>
                    <w:lsdException w:uiPriority="99" w:name="toa heading"/>
                    <w:lsdException w:uiPriority="99" w:name="List"/>
                    <w:lsdException w:uiPriority="99" w:name="List Bullet"/>
                    <w:lsdException w:uiPriority="99" w:name="List Number"/>
                    <w:lsdException w:uiPriority="99" w:name="List 2"/>
                    <w:lsdException w:uiPriority="99" w:name="List 3"/>
                    <w:lsdException w:uiPriority="99" w:name="List 4"/>
                    <w:lsdException w:uiPriority="99" w:name="List 5"/>
                    <w:lsdException w:uiPriority="99" w:name="List Bullet 2"/>
                    <w:lsdException w:uiPriority="99" w:name="List Bullet 3"/>
                    <w:lsdException w:uiPriority="99" w:name="List Bullet 4"/>
                    <w:lsdException w:uiPriority="99" w:name="List Bullet 5"/>
                    <w:lsdException w:uiPriority="99" w:name="List Number 2"/>
                    <w:lsdException w:uiPriority="99" w:name="List Number 3"/>
                    <w:lsdException w:uiPriority="99" w:name="List Number 4"/>
                    <w:lsdException w:uiPriority="99" w:name="List Number 5"/>
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="Title"
                                    w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="Closing"/>
                    <w:lsdException w:uiPriority="99" w:name="Signature"/>
                    <w:lsdException w:uiPriority="1" w:semiHidden="0" w:name="Default Paragraph Font"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="99" w:semiHidden="0" w:name="Body Text"/>
                    <w:lsdException w:uiPriority="99" w:name="Body Text Indent"/>
                    <w:lsdException w:uiPriority="99" w:name="List Continue"/>
                    <w:lsdException w:uiPriority="99" w:name="List Continue 2"/>
                    <w:lsdException w:uiPriority="99" w:name="List Continue 3"/>
                    <w:lsdException w:uiPriority="99" w:name="List Continue 4"/>
                    <w:lsdException w:uiPriority="99" w:name="List Continue 5"/>
                    <w:lsdException w:uiPriority="99" w:name="Message Header"/>
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0"
                                    w:name="Subtitle" w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="Salutation"/>
                    <w:lsdException w:uiPriority="99" w:name="Date"/>
                    <w:lsdException w:uiPriority="99" w:name="Body Text First Indent"/>
                    <w:lsdException w:uiPriority="99" w:name="Body Text First Indent 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Note Heading"/>
                    <w:lsdException w:uiPriority="99" w:name="Body Text 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Body Text 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="99" w:semiHidden="0"
                                    w:name="Body Text Indent 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Body Text Indent 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Block Text"/>
                    <w:lsdException w:uiPriority="99" w:name="Hyperlink"/>
                    <w:lsdException w:uiPriority="99" w:name="FollowedHyperlink"/>
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="Strong"
                                    w:locked="1"/>
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0"
                                    w:name="Emphasis" w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="Document Map"/>
                    <w:lsdException w:uiPriority="99" w:name="Plain Text"/>
                    <w:lsdException w:uiPriority="99" w:name="E-mail Signature"/>
                    <w:lsdException w:uiPriority="99" w:name="Normal (Web)"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Acronym"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Address"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Cite"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Code"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Definition"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Keyboard"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Preformatted"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Sample"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Typewriter"/>
                    <w:lsdException w:uiPriority="99" w:name="HTML Variable"/>
                    <w:lsdException w:uiPriority="99" w:semiHidden="0" w:name="Normal Table"/>
                    <w:lsdException w:uiPriority="99" w:name="annotation subject"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Simple 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Simple 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Simple 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Classic 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Classic 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Classic 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Classic 4"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Colorful 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Colorful 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Colorful 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Columns 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Columns 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Columns 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Columns 4"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Columns 5"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 4"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 5"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 6"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 7"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Grid 8"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 4"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 5"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 6"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 7"/>
                    <w:lsdException w:uiPriority="99" w:name="Table List 8"/>
                    <w:lsdException w:uiPriority="99" w:name="Table 3D effects 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table 3D effects 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table 3D effects 3"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Contemporary"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Elegant"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Professional"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Subtle 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Subtle 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Web 1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Web 2"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Web 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="99" w:name="Balloon Text"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="0" w:semiHidden="0" w:name="Table Grid"
                                    w:locked="1"/>
                    <w:lsdException w:uiPriority="99" w:name="Table Theme"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0" w:name="Light Shading"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0" w:name="Light List"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0" w:name="Light Grid"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0" w:name="Medium Shading 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0" w:name="Medium Shading 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0" w:name="Medium List 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0" w:name="Medium List 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0" w:name="Medium Grid 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0" w:name="Medium Grid 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0" w:name="Medium Grid 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0" w:name="Dark List"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0" w:name="Colorful Shading"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0" w:name="Colorful List"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0" w:name="Colorful Grid"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0"
                                    w:name="Light Shading Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0"
                                    w:name="Light List Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0"
                                    w:name="Light Grid Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0"
                                    w:name="Medium Shading 1 Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0"
                                    w:name="Medium Shading 2 Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0"
                                    w:name="Medium List 1 Accent 1"/>
                    <w:lsdException w:qFormat="1" w:unhideWhenUsed="0" w:uiPriority="99" w:semiHidden="0"
                                    w:name="List Paragraph"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0"
                                    w:name="Medium List 2 Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0"
                                    w:name="Medium Grid 1 Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0"
                                    w:name="Medium Grid 2 Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0"
                                    w:name="Medium Grid 3 Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0"
                                    w:name="Dark List Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0"
                                    w:name="Colorful Shading Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0"
                                    w:name="Colorful List Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0"
                                    w:name="Colorful Grid Accent 1"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0"
                                    w:name="Light Shading Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0"
                                    w:name="Light List Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0"
                                    w:name="Light Grid Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0"
                                    w:name="Medium Shading 1 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0"
                                    w:name="Medium Shading 2 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0"
                                    w:name="Medium List 1 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0"
                                    w:name="Medium List 2 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0"
                                    w:name="Medium Grid 1 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0"
                                    w:name="Medium Grid 2 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0"
                                    w:name="Medium Grid 3 Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0"
                                    w:name="Dark List Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0"
                                    w:name="Colorful Shading Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0"
                                    w:name="Colorful List Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0"
                                    w:name="Colorful Grid Accent 2"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0"
                                    w:name="Light Shading Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0"
                                    w:name="Light List Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0"
                                    w:name="Light Grid Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0"
                                    w:name="Medium Shading 1 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0"
                                    w:name="Medium Shading 2 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0"
                                    w:name="Medium List 1 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0"
                                    w:name="Medium List 2 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0"
                                    w:name="Medium Grid 1 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0"
                                    w:name="Medium Grid 2 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0"
                                    w:name="Medium Grid 3 Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0"
                                    w:name="Dark List Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0"
                                    w:name="Colorful Shading Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0"
                                    w:name="Colorful List Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0"
                                    w:name="Colorful Grid Accent 3"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0"
                                    w:name="Light Shading Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0"
                                    w:name="Light List Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0"
                                    w:name="Light Grid Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0"
                                    w:name="Medium Shading 1 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0"
                                    w:name="Medium Shading 2 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0"
                                    w:name="Medium List 1 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0"
                                    w:name="Medium List 2 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0"
                                    w:name="Medium Grid 1 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0"
                                    w:name="Medium Grid 2 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0"
                                    w:name="Medium Grid 3 Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0"
                                    w:name="Dark List Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0"
                                    w:name="Colorful Shading Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0"
                                    w:name="Colorful List Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0"
                                    w:name="Colorful Grid Accent 4"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0"
                                    w:name="Light Shading Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0"
                                    w:name="Light List Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0"
                                    w:name="Light Grid Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0"
                                    w:name="Medium Shading 1 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0"
                                    w:name="Medium Shading 2 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0"
                                    w:name="Medium List 1 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0"
                                    w:name="Medium List 2 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0"
                                    w:name="Medium Grid 1 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0"
                                    w:name="Medium Grid 2 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0"
                                    w:name="Medium Grid 3 Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0"
                                    w:name="Dark List Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0"
                                    w:name="Colorful Shading Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0"
                                    w:name="Colorful List Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0"
                                    w:name="Colorful Grid Accent 5"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="60" w:semiHidden="0"
                                    w:name="Light Shading Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="61" w:semiHidden="0"
                                    w:name="Light List Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="62" w:semiHidden="0"
                                    w:name="Light Grid Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="63" w:semiHidden="0"
                                    w:name="Medium Shading 1 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="64" w:semiHidden="0"
                                    w:name="Medium Shading 2 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="65" w:semiHidden="0"
                                    w:name="Medium List 1 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="66" w:semiHidden="0"
                                    w:name="Medium List 2 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="67" w:semiHidden="0"
                                    w:name="Medium Grid 1 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="68" w:semiHidden="0"
                                    w:name="Medium Grid 2 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="69" w:semiHidden="0"
                                    w:name="Medium Grid 3 Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="70" w:semiHidden="0"
                                    w:name="Dark List Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="71" w:semiHidden="0"
                                    w:name="Colorful Shading Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="72" w:semiHidden="0"
                                    w:name="Colorful List Accent 6"/>
                    <w:lsdException w:unhideWhenUsed="0" w:uiPriority="73" w:semiHidden="0"
                                    w:name="Colorful Grid Accent 6"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="1">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:uiPriority w:val="0"/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="21"/>
                        <w:szCs w:val="24"/>
                        <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="8">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:unhideWhenUsed/>
                    <w:uiPriority w:val="1"/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="7">
                    <w:name w:val="Normal Table"/>
                    <w:unhideWhenUsed/>
                    <w:uiPriority w:val="99"/>
                    <w:tblPr>
                        <w:tblStyle w:val="7"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="2">
                    <w:name w:val="Body Text"/>
                    <w:basedOn w:val="1"/>
                    <w:link w:val="18"/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:spacing w:after="120"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="0"/>
                        <w:sz w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="3">
                    <w:name w:val="Body Text Indent 2"/>
                    <w:basedOn w:val="1"/>
                    <w:link w:val="17"/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:spacing w:after="120" w:line="480" w:lineRule="auto"/>
                        <w:ind w:left="420" w:leftChars="200"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="0"/>
                        <w:sz w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="4">
                    <w:name w:val="Balloon Text"/>
                    <w:basedOn w:val="1"/>
                    <w:link w:val="13"/>
                    <w:semiHidden/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:kern w:val="0"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="5">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="1"/>
                    <w:link w:val="12"/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="0"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="6">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="1"/>
                    <w:link w:val="15"/>
                    <w:semiHidden/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:bottom w:val="single" w:color="auto" w:sz="6" w:space="1"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="0"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:styleId="9">
                    <w:name w:val="page number"/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:cs="Times New Roman"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="10">
                    <w:name w:val="apple-style-span"/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:cs="Times New Roman"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="11">
                    <w:name w:val="Body Text Indent 2 Char1"/>
                    <w:semiHidden/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="12">
                    <w:name w:val="页脚 Char"/>
                    <w:link w:val="5"/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:eastAsia="宋体"
                                  w:cs="Times New Roman"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="13">
                    <w:name w:val="批注框文本 Char"/>
                    <w:link w:val="4"/>
                    <w:semiHidden/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:eastAsia="宋体"
                                  w:cs="Times New Roman"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="14">
                    <w:name w:val="Body Text Char"/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:sz w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="15">
                    <w:name w:val="页眉 Char"/>
                    <w:link w:val="6"/>
                    <w:semiHidden/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:eastAsia="宋体"
                                  w:cs="Times New Roman"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="16">
                    <w:name w:val="Body Text Char1"/>
                    <w:semiHidden/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="17">
                    <w:name w:val="正文文本缩进 2 Char"/>
                    <w:link w:val="3"/>
                    <w:semiHidden/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:eastAsia="宋体"
                                  w:cs="Times New Roman"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="18">
                    <w:name w:val="正文文本 Char"/>
                    <w:link w:val="2"/>
                    <w:semiHidden/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="19">
                    <w:name w:val="Body Text Indent 2 Char"/>
                    <w:locked/>
                    <w:uiPriority w:val="99"/>
                    <w:rPr>
                        <w:rFonts w:eastAsia="宋体"/>
                        <w:sz w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:customStyle="1" w:styleId="20">
                    <w:name w:val="正文 New"/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="21"/>
                        <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="21">
                    <w:name w:val="List Paragraph"/>
                    <w:basedOn w:val="1"/>
                    <w:qFormat/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:ind w:firstLine="420" w:firstLineChars="200"/>
                    </w:pPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="1F497D"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="EEECE1"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4F81BD"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="C0504D"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="9BBB59"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="8064A2"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="4BACC6"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="F79646"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0000FF"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="800080"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="Cambria"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ ゴシック"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="Calibri"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ 明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="50000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="35000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="37000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="15000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="16200000" scaled="1"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="51000"/>
                                            <a:satMod val="130000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="80000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="93000"/>
                                            <a:satMod val="130000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="94000"/>
                                            <a:satMod val="135000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="16200000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="9525" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr">
                                        <a:shade val="95000"/>
                                        <a:satMod val="105000"/>
                                    </a:schemeClr>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                            <a:ln w="25400" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                            <a:ln w="38100" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="20000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="38000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="35000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="35000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                                <a:scene3d>
                                    <a:camera prst="orthographicFront">
                                        <a:rot lat="0" lon="0" rev="0"/>
                                    </a:camera>
                                    <a:lightRig rig="threePt" dir="t">
                                        <a:rot lat="0" lon="0" rev="1200000"/>
                                    </a:lightRig>
                                </a:scene3d>
                                <a:sp3d>
                                    <a:bevelT w="63500" h="25400"/>
                                </a:sp3d>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="40000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="40000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="45000"/>
                                            <a:satMod val="350000"/>
                                            <a:shade val="99000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="20000"/>
                                            <a:satMod val="255000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:path path="circle">
                                    <a:fillToRect l="50000" t="-80000" r="50000" b="180000"/>
                                </a:path>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="80000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="30000"/>
                                            <a:satMod val="200000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:path path="circle">
                                    <a:fillToRect l="50000" t="50000" r="50000" b="50000"/>
                                </a:path>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>