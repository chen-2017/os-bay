package com.wordprint.wordprintdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordprintDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WordprintDemoApplication.class, args);
    }

}
