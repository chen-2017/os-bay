package com.wordprint.wordprintdemo.modules.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.deepoove.poi.XWPFTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public final class WordUtils {

    public static void printWord() throws IOException {
        Resource resource = new ClassPathResource("template/buchaifen/test-table47.docx");
        File file = resource.getFile();

        XWPFTemplate template = XWPFTemplate.compile(file);
//        JSONObject data = defaultData();
//        String jstr = "{\"zyfzrzjhm\":\"ceshi\",\"ylcssqlxr\":\"ceshi\",\"ylcssqsqlx\":{\"label\":\" 港澳合资合作独资\",\"value\":\"2\",\"key\":2},\"zyfzrdh\":\"ceshi\",\"zjhm\":\"ceshi\",\"hdrs\":\"ceshi\",\"bjsl\":\"ceshi\",\"ylcssqzs\":\"ceshi\",\"sqr\":\"ceshi\",\"cyrs\":\"ceshi\",\"csdh\":\"ceshi\",\"sfzjlx\":\"身份证\",\"table\":[{\"wy\":\"ceshi\",\"tzfmchxm\":\"ceshi\",\"gjhdq\":\"ceshi\",\"czbl\":\"ceshi\"},{\"wy\":\"ceshi\",\"tzfmchxm\":\"ceshi\",\"gjhdq\":\"ceshi\",\"czbl\":\"ceshi\"}],\"fddbrxm\":\"ceshi\",\"fddbryddh\":\"ceshi\",\"zczb\":\"ceshi\",\"sdfs\":{\"label\":\"挂号信\",\"value\":\"2\",\"key\":2},\"zyfzrhjszdz\":\"ceshi\",\"zyffzrsfzjlx\":\"身份证\",\"fddbrdh\":\"ceshi\",\"symj\":\"ceshi\",\"sqsx\":{\"label\":\"延续\",\"value\":\"2\",\"key\":2},\"zyfzryddh\":\"ceshi\",\"hjszdz\":\"ceshi\",\"jjlx\":\"ceshi\",\"zyfzrxm\":\"ceshi\"}";
//        String jstr = "{\"jbrsfzh\":\"111122198212250079\",\"sqdz\":\"2\",\"A\":[{\"label\":\"道路普通货物运输\",\"value\":\"1\",\"key\":1},{\"label\":\"货物专用运输（罐式）\",\"value\":\"2\",\"key\":2},{\"label\":\"货物专用运输（集装箱）\",\"value\":\"3\",\"key\":\"b43b957774576c69b9170c0d80279780\"},{\"label\":\"货物专用运输（冷藏保鲜）\",\"value\":\"4\",\"key\":\"dd39ceb0df036aa48143f70a3c13848f\"},{\"label\":\"大型物件运输\",\"value\":\"5\",\"key\":\"c5511f442d016c6daf6bc98801158b35\"}],\"sqmc\":\"1\",\"jbrlxdh\":\"6\",\"fddbrxm\":\"3\",\"frdbsfzh\":\"111122198212250079\",\"jbrxm\":\"5\",\"Id\":\"11420100MA4KQKAN0B\",\"fddbrlxdh\":\"4\"}";
//        String jstr = "{\"kfdz\":\"11仓库（库房）地\",\"zyjsry\":\"11质量负责人专业技术人员（人）\",\"jyhw2002_2012\":[{\"label\":\"6815注射穿刺器械（仅含胰岛素注射笔针头及无针注射器）\",\"value\":\"1\",\"key\":1},{\"label\":\"6822医用光学器具、仪器及内窥镜设备(仅含护理液)\",\"value\":\"2\",\"key\":2}],\"yb\":\"123邮政编码\",\"kfmj\":\"11质量负责人库房面积\",\"qymc\":\"武汉大数据产业发展有限公司\",\"jycstj\":\"11经营场所条件（包括用房性质、设施设\",\"zlglry\":\"11质量负责人质量管理人员（人）\",\"sfzh1\":\"111122198212250079法定代表人身份证号\",\"sfzh2\":\"111122198212250079\",\"shfwy\":\"11质量负责人售后服务人员\",\"sfzh3\":\"111122198212250079质量负责人\",\"xm2\":\"11企业负责人姓名\",\"xm1\":\"11法定代表人姓名\",\"jymj\":\"11质量负责人经营面积\",\"xm3\":\"11质量负责人姓名\",\"lxdh\":\"15927054611\",\"kftj\":\"11库房条件（包括环境控制、设施设备等\",\"lxr\":\"lx\",\"jyhw2017\":[{\"label\":\"05-放射治疗器械\",\"value\":\"5\",\"key\":\"5831c20c99a940c0b12745c554d6322a\"},{\"label\":\"07-医用诊察和监护器械\",\"value\":\"7\",\"key\":\"a9b64c46e41a03529497f40a15d8283e\"}],\"zc2\":\"11企业负责人职称\",\"zc1\":\"11法定代表人职称\",\"zc3\":\"11质量负责人职称\",\"jyfs\":{\"label\":\"单体零售\",\"value\":\"1\",\"key\":1},\"input\":\"江汉区唐家墩路32号B栋7层2室\",\"ryzs\":\"11质量负责人人员总数（人）\",\"zs\":\"江汉区唐家墩路32号B栋7层2室\",\"xl1\":\"11法定代表人学历\",\"xl3\":\"11质量负责人学历\",\"xl2\":\"11企业负责人学历\"}";
        String jstr = "{\"sqdz\":\"4\",\"A\":[{\"label\":\"道路普通货物运输\",\"value\":\"1\",\"key\":1},{\"label\":\"大型物件运输\",\"value\":\"5\",\"key\":\"c5511f442d016c6daf6bc98801158b35\"}],\"sqmc\":\"1\",\"fddbrxm\":\"2\",\"frdbsfzh\":\"420101199001013111\",\"fddbrlxdh\":\"5\",\"Id\":\"420101199010013111\"}";
        JSONObject data = JSONObject.parseObject(jstr);
        template.render(data);
        // 输出
        Path outPath = Paths.get("/Users/scylla" +
                "/IdeaProjects/nutschan-gitee/os-bay/poi-tl-tdo/wordprint-demo/src/main/resources/out/table_pattern_out.docx");
        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(outPath.toFile()))) {
            template.write(os);
        } catch (IOException e) {
            log.error("render tpl error", e);
        } finally {
            try {
                template.close();
            } catch (IOException e) {
                log.error("close template error", e);
            }
        }

    }


    /*
     * @Author chentl
     * @Description 生成测试数据
     * @Date 16:51 2021/5/22
     * @Param []
     * @Return com.alibaba.fastjson.JSONObject
     **/
    public static JSONObject defaultData() {
        JSONObject re = new JSONObject();
        JSONArray users = new JSONArray();
        re.put("users", users);
        re.put("theme", "测试主题");
        for (int i = 0; i < 5; i++) {
            JSONObject one = new JSONObject();
            one.put("name", "学生" + i);
            one.put("age", i);
            one.put("idCard", "42019230884720000" + i);

            users.add(one);
        }

        return re;
    }

    public static void main(String[] args) throws IOException {
        printWord();
    }


}
