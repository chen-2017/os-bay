package com.deepoove.poi.policy;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.deepoove.poi.NiceXWPFDocument;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.PatternCell;
import com.deepoove.poi.exception.RenderException;
import com.deepoove.poi.template.ElementTemplate;
import com.deepoove.poi.template.run.RunTemplate;
import com.deepoove.poi.util.PoiStyleCopyUtils;
import com.deepoove.poi.util.TableTools;
import com.deepoove.poi.util.TagsUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 表格模式匹配RenderPolicy
 * table|5:var
 *
 * @author chentlS
 * @since 1.6.0
 */
public class TablePatternRenderPolicy extends AbstractPatternRenderPolicy {

	public static final String CURRENT_DATE = "currentdate";

	public static final String CURRENT_TIME = "currenttime";

	public static final String NAME = "table";

	private static final int ROW_LIMIT = 2;

	private static final Pattern PATTERN = Pattern.compile("\\{(.+)}");
	public static final String LABEL = "label";

	private final ExpressionParser parser = new SpelExpressionParser();

	private int tablePos;

	private int generateLength;

	@Override
	public void render(ElementTemplate eleTemplate, Object data, XWPFTemplate template) {
		parseAttr();

		NiceXWPFDocument doc = template.getXWPFDocument();
		RunTemplate runTemplate = (RunTemplate) eleTemplate;
		XWPFRun run = runTemplate.getRun();

		try {
			if (!TableTools.isInsideTable(run)) {
				throw new IllegalStateException("The template tag " + runTemplate.getSource() + " must be inside a table");
			}
			// w:tbl-w:tr-w:tc-w:p-w:tr
			XmlCursor newCursor = ((XWPFParagraph) run.getParent()).getCTP().newCursor();
			newCursor.toParent();
			newCursor.toParent();
			newCursor.toParent();
			XmlObject object = newCursor.getObject();
			XWPFTable table = doc.getTableByCTTbl((CTTbl) object);

			List<XWPFTableRow> rows = table.getRows();
			String currentTableTag = eleTemplate.getSource();

			for (int i = 0; i < rows.size(); i++) {
				List<ICell> cells = rows.get(i).getTableICells();
				if (CollectionUtils.isNotEmpty(cells)) {
					for (int j = 0; j < cells.size(); j++) {
						String rowKey = ((XWPFTableCell) cells.get(j)).getParagraphs().get(0).getText();
						if (rowKey.contains(currentTableTag)) {
							tablePos = i + 1;
//							System.err.println(MessageFormat.format("标签内容:{0} 表格所在行:{1}", rowKey, tablePos));
						}
					}
				}
			}


			doRender(table, data);
			run.setText("", 0);
		} catch (Exception e) {
			throw new RenderException("dynamic table error:" + e.getMessage(), e);
		}
	}

	private void parseAttr() {
		String attr = getAttr();
		try {
			generateLength = Integer.valueOf(attr);
		} catch (NumberFormatException e) {
			throw new RenderException("Table render attr must be number: " + attr);
		}
	}

	private void doRender(XWPFTable table, Object data) {
		List<XWPFTableRow> rows = table.getRows();
		if (rows.size() < ROW_LIMIT) {
			throw new RenderException("The render table must be " + ROW_LIMIT + " rows");
		}


		XWPFTableRow row = rows.get(tablePos);
		List<PatternCell> patternCells = getPatternCells(row);
		List list = wrapper(data);
		int dataSize = list.size();
		// 如果为-1，则根据实际的数据条数展示
		if (generateLength == -1) {
			generateLength = dataSize;
		}
		for (int i = 0; i < generateLength; i++) {
			// 插入新行
			XWPFTableRow currentRow = table.insertNewTableRow(tablePos + 1 + i);
			// 复制行样式
			PoiStyleCopyUtils.copyRowStyle(row, currentRow);
			// 超过数据容量，则只填充空行
			if (i >= dataSize) {
				continue;
			}
			Object obj = list.get(i);

			// 为cell填充值
			for (int j = 0; j < patternCells.size(); j++) {
				PatternCell cell = patternCells.get(j);
				StringBuilder value = new StringBuilder(cell.getName());

				StringBuilder finalVal = new StringBuilder();
				// 如果是模式匹配，则通过EL计算值
				if (value.toString().contains("}{")) {
					String[] values = value.toString().split("}\\{");
					for (String v : values) {
						finalVal.append(parseValues(cell, v, obj));
					}
				} else {
					finalVal = new StringBuilder(parseValues(cell, value.toString(), obj));
				}


				XWPFTableCell currentCell = currentRow.getCell(j);
				currentCell.setText(TagsUtils.getCustomTags(finalVal.toString()));
			}
		}
		// 删除第二行的模板
		table.removeRow(tablePos);
	}

	private String parseValues(PatternCell cell, String value, Object obj) {
//		//不合法的表达式配置无需渲染 保持原样
		String expressionRegex = "^[0-9a-zA-Z_]{1,}$";
		Pattern pattern = Pattern.compile(expressionRegex);
		boolean unValidExpression = !pattern.matcher(value).matches();
		if (unValidExpression) {
			return value;
		}

		if (cell.isPattern() && !TagsUtils.containsCustomTags(value)) {
			if (StringUtils.isEmpty(obj.toString())) {
				value = "";
			} else if (obj instanceof Boolean) {
				value = (Boolean) obj ? "true" : "false";
			} else if (obj instanceof JSONObject) {
				// 单独针对JSONOBject处理
				value = renderJsonObject(obj, value);

			} else {
				//字符串
				value = parser.parseExpression(value).getValue(obj, String.class);
			}
		}
		return StringUtils.isEmpty(value) ? "" : value;
	}

	private String renderJsonObject(Object obj, String key) {
		String value = "";
		if (obj instanceof Boolean) {
			value = (Boolean) obj ? "是" : "否";
			return value;
		} else if (obj instanceof JSONObject) {
			//遍历每一个key，对单选和多选做反显处理
			value = parseObj(obj).getString(key);
		} else if (obj instanceof String) {
			value = StringUtils.strip(JSON.toJSONString(((JSONObject) obj).get(key)), "\"\"");
		}
		return value;
	}

	private JSONObject parseObj(Object obj) {
		JSONObject after = new JSONObject();
		JSONObject jo = JSONObject.parseObject(obj.toString());

		AtomicReference<String> val = new AtomicReference<>("");

		jo.forEach((k, v) -> {
			if (v instanceof String) {
				after.put(k, v);
			} else if (v instanceof JSONObject) {
				String label = ((JSONObject) v).getString(LABEL);
				after.put(k, label);
			} else if (v instanceof JSONArray) {
				//todo extend jsonarr

			}

		});

		return after;
	}

	private List<PatternCell> getPatternCells(XWPFTableRow patternRow) {
		return patternRow.getTableCells().stream().map(cell -> {
			String text = cell.getText();
			Matcher matcher = PATTERN.matcher(text);
			if (matcher.find()) {
				String group = matcher.group(1);
				return new PatternCell(true, group, cell);
			}
			return new PatternCell(true, text, cell);
		}).collect(Collectors.toList());
	}

	private List wrapper(Object data) {
		List list;
		if (data instanceof List) {
			list = (List) data;
		} else {
			list = Collections.singletonList(data);
		}
		return list;
	}

	@Override
	public String getName() {
		return NAME;
	}

}
