package com.deepoove.poi.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public final class TagsUtils {

	public static final String CURRENT_DATE = "currentdate";

	public static final String CURRENT_TIME = "currenttime";

	public static String getCustomTags(String value) {
		if (CURRENT_DATE.equalsIgnoreCase(value)) {
			return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		} else if (CURRENT_TIME.equalsIgnoreCase(value)) {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}
		return Objects.isNull(value) ? "" : value;
	}


	public static boolean containsCustomTags(String value) {
		List<String> tags = new ArrayList<>();
		tags.add(CURRENT_DATE);
		tags.add(CURRENT_TIME);
		return tags.contains(value);
	}


}
