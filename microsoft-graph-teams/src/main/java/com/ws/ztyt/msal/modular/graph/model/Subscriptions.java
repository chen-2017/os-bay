package com.ws.ztyt.msal.modular.graph.model;

import java.util.Date;

/**
 * <p>
 * teams  订阅表
 * </p>
 *
 * @author chentl123
 * @since 2020-01-08
 */
public class Subscriptions {

    public Subscriptions() {
    }

    public Subscriptions(Integer channelsId, String msId, Date expirationDateTime) {
        this.channelsId = channelsId;
        this.msId = msId;
        this.expirationDateTime = expirationDateTime;
    }

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 频道业务id
     */
    private Integer channelsId;
    /**
     * 订阅业务id
     */
    private String msId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;

    private Date expirationDateTime;


    public Date getExpirationDateTime() {
        return expirationDateTime;
    }

    public void setExpirationDateTime(Date expirationDateTime) {
        this.expirationDateTime = expirationDateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChannelsId() {
        return channelsId;
    }

    public void setChannelsId(Integer channelsId) {
        this.channelsId = channelsId;
    }

    public String getMsId() {
        return msId;
    }

    public void setMsId(String msId) {
        this.msId = msId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "Subscriptions{" +
                ", id=" + id +
                ", channelsId=" + channelsId +
                ", msId=" + msId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", expirationDateTime=" + expirationDateTime +
                "}";
    }
}
