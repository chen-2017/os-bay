package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @Project MsgUser
 * @Title MsgUser.java
 * @Description 消息相关用户对象
 * @Package com.ws.ztyt.msal.core.graph.model
 * @author chentl
 * @date 2020/1/8 22:02
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 * @version V1.0
 */
@Data
public class MsgUser {
    @Expose
    @JsonProperty(value = "id")
    private String id;

    @Expose
    @JsonProperty(value = "displayName")
    private String displayName;

    @Expose
    @JsonProperty(value = "userIdentityType")
    private String userIdentityType;


}
