package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @Project EncryptedContent
 * @Title EncryptedContent.java
 * @Description 消息结果中包含加密信息的对象
 * @Package com.ws.ztyt.msal.core.graph.model
 * @author chentl
 * @date 2020/1/8 20:37
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 * @version V1.0
 */
@Data
public class EncryptedContent {

    @Expose
    @JsonProperty(value="data")
    private String data;

    @Expose
    @JsonProperty(value = "dataSignature")
    private String dataSignature;

    @Expose
    @JsonProperty(value="dataKey")
    private String dataKey;

    @Expose
    @JsonProperty(value="encryptionCertificateId")
    private String encryptionCertificateId;

    @Expose
    @JsonProperty(value="encryptionCertificateThumbprint")
    private String encryptionCertificateThumbprint;

}
