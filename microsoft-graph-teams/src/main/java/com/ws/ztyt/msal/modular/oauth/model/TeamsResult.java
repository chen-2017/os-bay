package com.ws.ztyt.msal.modular.oauth.model;

import com.microsoft.graph.models.extensions.Group;
import com.microsoft.graph.models.extensions.Team;
import lombok.Data;

import java.nio.channels.Channel;
import java.util.List;

@Data
public class TeamsResult {

    private List<Channel> channels;

    private Team team;

    private Group group;
}
