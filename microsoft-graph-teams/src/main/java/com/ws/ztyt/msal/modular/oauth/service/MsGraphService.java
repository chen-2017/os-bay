package com.ws.ztyt.msal.modular.oauth.service;

import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.graph.http.GraphServiceException;
import com.microsoft.graph.models.extensions.Channel;
import com.microsoft.graph.models.extensions.Team;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.requests.extensions.IGroupCollectionPage;
import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.modular.graph.model.Subscriptions;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;

import java.util.List;

/**
 * @author chentl
 * @version V1.0
 * @Project MsGraphService
 * @Title MsGraphService.java
 * @Description Graph api 的所有服务
 * @Package com.ws.ztyt.msal.modular.oauth.service
 * @date 2019/12/27 10:00
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
public interface MsGraphService {


    /**
     * @return java.lang.Object
     * @author chentl
     * @description 向指定团队中的指定频道中发送消息
     * @date 2019/12/30 13:46
     * @params [teamsId, channelId,messageText]
     * @since JDK 1.7
     */
    ResponseData sendMessage(TeamsParam teamsParam);

    /**
     * 创建一个Team
     *
     * @param teamsParam
     * @param generateDefaultChannel
     * @return
     */
    ResponseData generateTeam(TeamsParam teamsParam, boolean generateDefaultChannel);

    /**
     * @return void
     * @author chentl
     * @description 注册 Graph客户端
     * @date 2019/12/27 10:26
     * @params [accessToken, clientToken]
     * @since JDK 1.7
     */
    void registerGraphClient(String accessToken, String clientToken);

    /**
     * @return com.microsoft.aad.msal4j.IAuthenticationResult
     * @author chentl
     * @description 根据用户名密码获取 授权结构
     * @date 2019/12/27 10:28
     * @params [account, password]
     * @since JDK 1.7
     */
    IAuthenticationResult getAccessTokenByUserBase(String account, String password);

    /**
     * @return com.microsoft.aad.msal4j.IAuthenticationResult
     * @author chentl
     * @description 获取客户端权限的 令牌
     * @date 2020/1/6 12:19
     * @params []
     * @since JDK 1.7
     */
    IAuthenticationResult getAccessTokenForClient();

    /**
     * @return com.microsoft.aad.msal4j.IAuthenticationResult
     * @author chentl
     * @description 根据 refreshToken获取accesstoken
     * @date 2019/12/27 16:33
     * @params [refreshAccessToken]
     * @since JDK 1.7
     */
    IAuthenticationResult refreshAccessToken(String refreshAccessToken);

    /**
     * @return com.microsoft.graph.models.extensions.User
     * @author chentl
     * @description 获取身份令牌所属的用户信息
     * @date 2019/12/30 17:03
     * @params []
     * @since JDK 1.7
     */
    User getUserInfo();

    /**
     * @return User
     * @author chentl
     * @description 根据用户id 获取用户信息
     * @date 2019/12/27 10:03
     * @params [accessToken]
     * @since JDK 1.7
     */
    User getUserInfo(String userId);

    /**
     * @return com.microsoft.graph.requests.extensions.IGroupCollectionPage
     * @author chentl
     * @description 获取用户所有组织
     * @date 2019/12/27 16:08
     * @params []
     * @since JDK 1.7
     */
    IGroupCollectionPage getGroups();

    /**
     * @return void
     * @author chentl
     * @description 缓存token
     * @date 2019/12/27 17:14
     * @params [accessToken, refreshToken,clientToken]
     * @since JDK 1.7
     */
    void storeToken(String accessToken, String refreshToken, String clientToken);

    /**
     * @return java.lang.String
     * @author chentl
     * @description 确认token有效性
     * @date 2019/12/27 17:14
     * @params []
     * @since JDK 1.7
     */
    String keepToken();


    /**
     * 在 Teams下面创建 四个默认频道
     * Shipment Status ， Cargo , Billing , Quote
     *
     * @param teamsParam
     * @return
     */
    List<Channel> generateDefaultChannel(TeamsParam teamsParam);

    /**
     * 创建一个新频道
     *
     * @param teamsParam
     * @return
     */
    ResponseData createChannel(TeamsParam teamsParam);

    /**
     * 构造 从 组到team对象
     *
     * @return
     */
    Team buildTeam();

    /**
     * @return com.ws.ztyt.core.reqres.response.ResponseData
     * @author chentl
     * @description 根据频道id  订阅频道消息
     * @date 2020/1/8 14:36
     * @params [teamsParam]
     * @since JDK 1.7
     */
    ResponseData subscriptionChannel(TeamsParam teamsParam);

    /**
     * @return com.ws.ztyt.core.reqres.response.ResponseData
     * @author chentl
     * @description 续订业务系统中所有订阅
     * @date 2020/1/9 11:01
     * @params []
     * @since JDK 1.7
     */
    void renewAllSubscriptions();

    /**
     * @return void
     * @author chentl
     * @description 处理订阅失效的异常
     * @date 2020/1/19 12:29
     * @params [e, subscription]
     * @since JDK 1.7
     */
    void subscriptionExpirationCleanup(GraphServiceException e, Subscriptions subscription);


}
