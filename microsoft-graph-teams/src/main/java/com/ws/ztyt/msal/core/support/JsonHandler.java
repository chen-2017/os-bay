package com.ws.ztyt.msal.core.support;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonHandler {


    /**
     * 转换成Json格式，使用fastjson转换会报错
     *
     * @param o
     * @return
     */
    public static Object parseJson(Object o) {
        return new Gson().toJson(o);
    }

    /**
     * 转换成 JsonElement
     *
     * @param obj
     * @return
     */
    public static JsonElement parseJsonElement(Object obj) {
        return new JsonParser().parse(JSONObject.toJSONString(obj));
    }
}
