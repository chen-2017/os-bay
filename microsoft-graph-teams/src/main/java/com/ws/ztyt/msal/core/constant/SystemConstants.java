package com.ws.ztyt.msal.core.constant;

/**
 * 系统管理常量
 *
 * @author tft
 */
public interface SystemConstants {

    /**
     * MStoken 超时时间 50min（单位：秒）  * 官方授权token超时时间 一个小时
     */
    Long DEFAULT_MS_TOKEN_TIME_OUT_SECS = 3000L;

    /**
     * 刷新令牌过期时间
     */
    Long DEFAULT_REFRESH_TOKEN_TIME_OUT_SECS = 7200L;
    /**
     * Teams消息 resourceId 缓存时间
     */
    Long RESOURCE_ID_OUT_SECS = 30L;
}
