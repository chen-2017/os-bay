package com.ws.ztyt.msal.core.graph.extensions;

import com.microsoft.graph.concurrency.ICallback;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.core.IBaseClient;
import com.microsoft.graph.http.BaseRequest;
import com.microsoft.graph.http.HttpMethod;
import com.microsoft.graph.models.extensions.Channel;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.IChannelRequest;
import com.ws.ztyt.msal.modular.oauth.model.ChannelMessage;

/**
 * @author chentl
 * @version V1.0
 * @Project ChannelRequestExpand
 * @Title ChannelRequestExpand.java
 * @Description 集成ChannelRequest类，扩展发送消息接口
 * @Package com.ws.ztyt.msal.core.graph
 * @date 2019/12/30 14:03
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
public class ChannelRequestExpand extends BaseRequest implements IChannelRequestExpand {


    /**
     * The request for the Channel
     *
     * @param requestUrl     the request URL
     * @param client         the service client
     * @param requestOptions the options for this request
     */
    public ChannelRequestExpand(final String requestUrl, final IBaseClient client, final java.util.List<? extends Option> requestOptions) {
        super(requestUrl, client, requestOptions, Channel.class);
    }

    /**
     * Gets the Channel from the service
     *
     * @param callback the callback to be called after success or failure
     */
    @Override
    public void get(final ICallback<Channel> callback) {
        send(HttpMethod.GET, callback, null);
    }

    /**
     * Gets the Channel from the service
     *
     * @return the Channel from the request
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    @Override
    public Channel get() throws ClientException {
        return send(HttpMethod.GET, null);
    }

    /**
     * Delete this item from the service
     *
     * @param callback the callback when the deletion action has completed
     */
    @Override
    public void delete(final ICallback<Channel> callback) {
        send(HttpMethod.DELETE, callback, null);
    }

    /**
     * Delete this item from the service
     *
     * @throws ClientException if there was an exception during the delete operation
     */
    @Override
    public void delete() throws ClientException {
        send(HttpMethod.DELETE, null);
    }

    /**
     * Patches this Channel with a source
     *
     * @param sourceChannel the source object with updates
     * @param callback      the callback to be called after success or failure
     */
    @Override
    public void patch(final Channel sourceChannel, final ICallback<Channel> callback) {
        send(HttpMethod.PATCH, callback, sourceChannel);
    }

    /**
     * Patches this Channel with a source
     *
     * @param sourceChannel the source object with updates
     * @return the updated Channel
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    @Override
    public Channel patch(final Channel sourceChannel) throws ClientException {
        return send(HttpMethod.PATCH, sourceChannel);
    }

    /**
     * Creates a Channel with a new object
     *
     * @param newChannel the new object to create
     * @param callback   the callback to be called after success or failure
     */
    @Override
    public void post(final Channel newChannel, final ICallback<Channel> callback) {
        send(HttpMethod.POST, callback, newChannel);
    }

    /**
     * Creates a Channel with a new object
     *
     * @param newChannel the new object to create
     * @return the created Channel
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    @Override
    public Channel post(final Channel newChannel) throws ClientException {
        return send(HttpMethod.POST, newChannel);
    }

    /**
     * Sets the select clause for the request
     *
     * @param value the select clause
     * @return the updated request
     */
    @Override
    public IChannelRequest select(final String value) {
        getQueryOptions().add(new QueryOption("$select", value));
        return this;
    }

    /**
     * Sets the expand clause for the request
     *
     * @param value the expand clause
     * @return the updated request
     */
    @Override
    public IChannelRequest expand(final String value) {
        getQueryOptions().add(new QueryOption("$expand", value));
        return this;
    }

    /**
     * Send Message to Channels
     *
     * @param channelMessage the expand clause
     * @return the updated request
     */
    @Override
    public Object sendMessage(Object channelMessage) throws ClientException {
        return send(HttpMethod.POST, channelMessage);
    }
}
