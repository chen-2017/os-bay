package com.ws.ztyt.msal.modular.oauth.model;

import lombok.Data;

import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

/**
 * teams 参数实体
 */
@Data
public class TeamsParam {

    public TeamsParam() {
    }

    /**
     * 用于订阅消息入库的构造
     *
     * @param msMessageId
     * @param msChannelId
     * @param content
     * @param contentType
     * @param createName
     */
    public TeamsParam(String msMessageId, String msChannelId, String content, String contentType, String createName) {
        this.msMessageId = msMessageId;
        this.msChannelId = msChannelId;
        this.content = content;
        this.contentType = contentType;
        this.createName = createName;
    }

    public TeamsParam(String msTeamId, String msChannelId, Integer channelId) {
        this.msTeamId = msTeamId;
        this.msChannelId = msChannelId;
        this.channelId = channelId;
    }

    public TeamsParam(String msMessageId, String content, String contentType, String createName, Integer channelId) {
        this.msMessageId = msMessageId;
        this.content = content;
        this.contentType = contentType;
        this.createName = createName;
        this.channelId = channelId;
    }

    /**
     * MS 相关参数
     */
    LinkedList<String> groupTypes = new LinkedList<String>() {{
        add("Unified");
    }};

    private String msTeamId;

    private String msChannelId;

    private String msMessageId;

    private String msSubscriptionsId;

    private String displayName;

    private String description = "Default generated team description " +
            "when creating a team using graphapi";

    private boolean mailEnabled = true;

    private boolean securityEnabled = false;

    private String mailNickname = UUID.randomUUID().toString();


    /**
     * 业务相关参数
     */
    private String groupId;

    private String memberIds;

    private String msgText;

    private String memberId;

    private String content;

    private String contentType;

    private String createName;

    private Integer createBy;

    private Integer userId;

    private Integer channelId;

    private Integer teamId;

    private Integer deptId;

    private String nextUrl;

    private String type;

    private Integer sourceId;

    private Date expirationDateTime;

}
