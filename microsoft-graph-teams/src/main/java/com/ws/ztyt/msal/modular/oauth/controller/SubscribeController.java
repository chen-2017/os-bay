package com.ws.ztyt.msal.modular.oauth.controller;

import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.core.rabbitmq.RabbitSender;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * @author chentl
 * @version V1.0
 * @Project SubscribeController
 * @Title SubscribeController.java
 * @Description 订阅管理
 * @Package com.microsoft.azure.msalwebsample
 * @date 2019/12/23 15:13
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@Slf4j
@RestController
public class SubscribeController {

    @Value("${aad.encryptionCertificate}")
    private String encryptionCertificate;

    @Value("${aad.encryptionCertificateId}")
    private String encryptionCertificateId;

    @Autowired
    private MsGraphService graphService;

    @Autowired
    private RabbitSender rabbitSender;


    @RequestMapping(value = "/ms/subscription/recieve", method = RequestMethod.POST)
    public Object recieveSubscriptionMsg(HttpServletResponse response, HttpServletRequest request) {
        String validationToken = request.getParameter("validationToken");
        response.setStatus(200);
        if (validationToken != null) {
            log.info("/recieve 获取到的 validationToken" + validationToken);
        } else {
            log.info("接收到订阅消息，准备开始解析");
            try {
                BufferedReader reader = request.getReader();
                char[] buf = new char[512];
                int len = 0;
                StringBuilder contentBuffer = new StringBuilder();
                while ((len = reader.read(buf)) != -1) {
                    contentBuffer.append(buf, 0, len);
                }
                String content = contentBuffer.toString();
                log.info("接收到的回执消息:" + content);
                rabbitSender.sendSubscriptionStoreMsg(content);
                log.info("回执消息已发往待入库消息队列");
            } catch (IOException e) {
                log.error("读取订阅消息异常", e);
            }
        }
        return validationToken;
    }

    @RequestMapping("/ms/subscription/lifecycle")
    public Object recieveLifeSycleSubs(HttpServletResponse response, HttpServletRequest request) {
        String validationToken = request.getParameter("validationToken");
        log.info("/lifecycle 获取到的 validationToken" + validationToken);

        try {
            BufferedReader reader = request.getReader();
            char[] buf = new char[512];
            int len = 0;
            StringBuffer contentBuffer = new StringBuffer();
            while ((len = reader.read(buf)) != -1) {
                contentBuffer.append(buf, 0, len);
            }
            String content = contentBuffer.toString();
            log.info("接收到的生命周期消息:" + content);
        } catch (IOException e) {
            log.error("生命周期请求处理异常", e);
        }
        return validationToken;
    }

    @RequestMapping(value = "/ms/subscription/channel", method = RequestMethod.POST)
    public ResponseData subscribeChannels(@RequestBody TeamsParam teamsParam) {
        ResponseData res = graphService.subscriptionChannel(teamsParam);
        return res;
    }

}