package com.ws.ztyt.msal.modular.graph.service;

import com.baomidou.mybatisplus.service.IService;
import com.ws.ztyt.msal.core.graph.model.AcceptMessage;
import com.ws.ztyt.msal.modular.graph.model.Channels;

import java.util.List;

public interface IChannelsService extends IService<Channels> {

    List<Channels> getList(Integer maxId);


    /**
     * 解析返回消息信息
     *
     * @param message
     * @return
     */
    List<AcceptMessage> parseMessage(String message);
}
