package com.ws.ztyt.msal.core.tools;

import cn.hutool.core.collection.CollectionUtil;

import java.util.*;

/**
 * 字符串处理工具类
 *
 * @author chentl
 * @version V1.0.0
 * @date 2012-4-13 上午09:56:51
 * @Copyright Copyright © 2011 bonree. All rights reserved.
 */
public class StringUtils {


    /**
     * 判断 List<String> 集合是否为空，或者元素全为空串则返回 false,集合为空或至少有一个非空串元素,则返回true
     *
     * @param list
     * @return
     */
    public static final Boolean isAllEmpty(List<String> list) {
        if (CollectionUtil.isEmpty(list)) {
            return true;
        }
        for (String element : list) {
            if (isNotBlank(element)) {
                return false;
            }
        }
        return true;
    }

    ;

    /**
     * 判断字符串是否为空
     *
     * @param args 字符串
     * @return 空 true,不为空 false
     * @author
     * @date 2012-4-13 上午10:05:55
     * @version V1.0.0
     * @ModifyRecord 修改记录 <br>
     * 1、 ; 2012-4-13 上午10:05:55; 初始化
     */
    public static final Boolean isEmpty(String args) {
        Boolean bool = false;
        //空对象
        if (null == args) {
            bool = true;
        } else if ("".equals(args.trim())) {//空字符串
            bool = true;
        } else if ("　".equals(args)) {//全角
            bool = true;
        } else if ("null".equals(args)) {//null字符串
            bool = true;
        } else if ("[]".equals(args)) {
            bool = true;
        }
        return bool;
    }

    /**
     * @param cs
     * @return boolean
     * @Title: isBlank
     * @Description: 判断字符串是否为空
     */
    public static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(cs.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    /**
     * 判断字符串是否不为空
     *
     * @param args 字符串
     * @return 空 false,不为空 true
     * @author
     * @date 2012-4-13 上午10:14:29
     * @version V1.0.0
     * @ModifyRecord 修改记录 <br>
     * 1、 ; 2012-4-13 上午10:14:29; 初始化
     */
    public static final Boolean isNotEmpty(String args) {
        return !isEmpty(args);
    }

    /**
     * 验证数组数据是否为空
     *
     * @param args 字符串数组
     * @return 空 true,不为空 false
     * @author
     * @date 2012-4-13 上午10:43:48
     * @version V1.0.0
     * @ModifyRecord 修改记录 <br>
     * 1、 ; 2012-4-13 上午10:43:48; 初始化
     */
    public static final Boolean arrayIsEmpty(String... args) {
        if (null == args) {
            return true;
        }
        for (int i = 0; i < args.length; i++) {
            if (StringUtils.isEmpty(args[i])) {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证数组数据是否不为空
     *
     * @param args 字符串数组
     * @return 空 false,不为空 true
     * @author
     * @date 2012-4-13 上午10:53:30
     * @version V1.0.0
     * @ModifyRecord 修改记录 <br>
     * 1、 ; 2012-4-13 上午10:53:30; 初始化
     */
    public static final Boolean arrayIsNotEmpty(String[] args) {
        return !arrayIsEmpty(args);
    }

    /**
     * 概述：验证字符串是否为数字
     *
     * @param cs
     * @return boolean
     * @Title: isNumeric
     * @user <a href=mailto:></a>
     */
    public static final boolean isNumeric(final String cs) {
        if (isEmpty(cs)) {
            return false;
        }
        int n = 0;
        final int sz = cs.length();
        for (int i = 0; i < sz; i++) {
            char chars = cs.charAt(i);
            if (chars == '.') {
                n++;
            }
            if (n >= 2) {
                return false;
            }
            if (Character.isDigit(chars) == false && chars != '.') {
                return false;
            }
        }
        return true;
    }

    /**
     * 概述：验证UUID格式
     *
     * @param uuid
     * @return boolean
     * @Title: isUUID
     * @user <a href=mailto:></a>
     */
    public static final boolean isUUID(String uuid) {
        try {
            UUID.fromString(uuid);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * 概述：连接字符串
     *
     * @param key 关键字符串集合
     * @return String
     * @Title: assembleKey
     * @user <a href=mailto:></a>
     */
    public static String assembleKey(Object... key) {
        StringBuilder keyBuf = new StringBuilder();
        for (int i = 0; i < key.length; i++) {
            keyBuf.append(key[i]);
        }
        return keyBuf.toString();
    }

    /**
     * 概述：根据分割符连接字符串
     *
     * @param delim 分割符
     * @param key   关键字
     * @return String
     * @Title: assembleKey
     * @user <a href=mailto:></a>
     */
    public static String assembleKeyByDelim(String delim, Object... key) {
        if (delim == null) {
            delim = "_";
        }
        StringBuilder keyBuf = new StringBuilder();
        for (int i = 0; i < key.length; i++) {
            keyBuf.append(key[i]);
            if (i < key.length - 1) {
                keyBuf.append(delim);
            }
        }
        return keyBuf.toString();
    }

    /**
     * 概述：分割字符串
     *
     * @param str   字段串
     * @param delim 分割符号
     * @return String[]
     * @Title: getSplit
     * @user <a href=mailto:></a>
     */
    public static final String[] getSplit(String str, String delim) {
        if (str == null || delim == null) {
            return null;
        }
        StringTokenizer token = new StringTokenizer(str, delim);
        int num = token.countTokens();
        String[] result = new String[num];
        int i = 0;
        while (token.hasMoreTokens()) {
            result[i++] = token.nextToken();
        }
        return result;
    }

    /**
     * @return
     * @description 去掉末尾字符
     * @date 2018/4/13 14:52
     * @params
     */
    public static String picSuffix(String str, String character) {
        if (str.endsWith(character)) {
            str = str.substring(0, str.length() - character.length());
        }
        return str;
    }


    /**
     * 去除String数组中的重复项，将结果存入List
     *
     * @param arr 源数组
     * @return 返回List
     */
    public static final List<String> arr2set(String[] arr) {
        List<String> set = new ArrayList<String>();
        if (arr == null || arr.length == 0) {
            return set;
        }
        for (String str : arr) {
            if (set.indexOf(str) >= 0) {
                continue;
            }
            set.add(str);
        }
        return set;
    }

    public static final String setToString(Set<String> set) {
        return setToString(set, ",");
    }

    public static final String setToString(Set<String> set, String delim) {
        String str = "";
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            str += iterator.next() + ",";
        }
        if (str.length() > 0) {
            str = str.substring(0, str.length() - delim.length());
        }
        return str;
    }

    /**
     * 截取字符串str中指定字符 strStart、strEnd之间的字符串
     *
     * @param string
     * @param str1
     * @param str2
     * @return
     */
    public static String subString(String str, String strStart, String strEnd) {

        /* 找出指定的2个字符在 该字符串里面的 位置 */
        int strStartIndex = str.indexOf(strStart);
        int strEndIndex = str.indexOf(strEnd);

        /* 开始截取 */
        String result = str.substring(strStartIndex, strEndIndex).substring(strStart.length());
        return result;
    }

}
