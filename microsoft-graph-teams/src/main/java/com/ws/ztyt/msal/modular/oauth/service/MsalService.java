package com.ws.ztyt.msal.modular.oauth.service;

import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author chentl
 * @version V1.0
 * @Project MsalService
 * @Title MsalService.java
 * @Description 对 微服务 开放的 所有api
 * @Package com.ws.ztyt.msal.modular.oauth.service
 * @date 2019/12/30 17:43
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@RequestMapping("/api/msal")
public interface MsalService {

    @RequestMapping(value = "/createTeam", method = RequestMethod.POST)
    @ResponseBody
    ResponseData createTeam(@RequestBody TeamsParam teamsParam);


    @RequestMapping(value = "/createChannel", method = RequestMethod.POST)
    @ResponseBody
    ResponseData createChannel(@RequestBody TeamsParam teamsParam);

    @RequestMapping(value = "/sendMessages", method = RequestMethod.POST)
    @ResponseBody
    ResponseData sendMessages(@RequestBody TeamsParam teamsParam);


}
