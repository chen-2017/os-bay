package com.ws.ztyt.msal.core.constant.redis;

/**
 * @author chentl
 * @version V1.0
 * @Project RediKey
 * @Title RediKey.java
 * @Description redis中用到的键
 * @Package com.ws.ztyt.msal.core.constant.redis
 * @date 2019/12/27 13:54
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
public interface RedisConstants {
    /**
     * 身份验证令牌
     */
    String MS_ACCESS_TOKEN = "microsoft:token:MS_ACCESS_TOKEN";
    /**
     * 刷新令牌
     */
    String MS_REFRESH_TOKEN = "microsoft:token:MS_REFRESH_TOKEN";
    /**
     * 应用程序-客户端令牌
     */
    String MS_CLIENT_TOKEN = "microsoft:token:MS_CLIENT_TOKEN";


    //==============key==================/
    String PREFIX_MS_RESOUCE_ID = "microsoft:resource:id_";

}
