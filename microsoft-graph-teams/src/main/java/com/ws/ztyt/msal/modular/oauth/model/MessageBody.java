package com.ws.ztyt.msal.modular.oauth.model;

import lombok.Data;

@Data
public class MessageBody {
    private String content;
    /**
     * html
     */
    private String contentType;
}
