package com.ws.ztyt.msal.modular.oauth.model;

import com.microsoft.graph.models.extensions.Channel;
import lombok.Data;

/**
 * @author chentl
 * @version V1.0
 * @Project ChannelMessage
 * @Title ChannelMessage.java
 * @Description graph 接口中没有 java的 聊天对象，所以自定义一个
 * @Package com.ws.ztyt.msal.modular.oauth.model
 * @date 2019/12/30 14:31
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@Data
public class ChannelMessage extends Channel {
    private MessageBody body;
}
