package com.ws.ztyt.msal.core.task;

import com.alibaba.fastjson.JSON;
import com.ws.ztyt.msal.config.RedisService;
import com.ws.ztyt.msal.core.graph.model.AcceptMessage;
import com.ws.ztyt.msal.core.rabbitmq.RabbitSender;
import com.ws.ztyt.msal.core.tools.GraphBatchRequestParam;
import com.ws.ztyt.msal.core.tools.RestClient;
import com.ws.ztyt.msal.modular.graph.model.Channels;
import com.ws.ztyt.msal.modular.graph.service.IChannelsService;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class ScheduledTasks {


    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RedisService redisService;

    @Autowired
    private RabbitSender rabbitSender;

    @Autowired
    private MsGraphService msGraphService;

    @Autowired
    private RestClient restClient;

    @Autowired
    private IChannelsService channelsService;

    @Value("${aad.sync.enable}")
    private boolean syncEnable;

    @Value("${aad.subscription.enable}")
    private boolean subscriptionEnable;


    /**
     * 每15分钟检查一次是否存在 MS_ACCESS_TOKEN
     */
    @Scheduled(cron = "0 0/15 * * * ? ")
    @Async("graphTaskExecutor")
    public void checkMsToken() {
        try {
            String error = msGraphService.keepToken();
            if (StringUtils.isBlank(error)) {
                log.info("==========身份信息已更新==========");
            } else {
                log.error(error);
            }
        } catch (Exception e) {
            log.error("刷新令牌时异常， {}", e);
        }
    }

    /**
     * 每2 分钟 扫描一次，续订将要到期的订阅
     */
    @Scheduled(cron = "0 0/2 * * * ? ")
    @Async("graphTaskExecutor")
    public void renewSubscriptions() {
        if (!subscriptionEnable) {
            return;
        }
        try {
            log.info("==========即将开始续订所有订阅内容==========");
            msGraphService.renewAllSubscriptions();
            log.info("==========续订结束==========");

        } catch (Exception e) {
            log.error("续订业务系统中的订阅时出现异常", e);
        }
    }


    /**
     * 测试环境设置每30秒钟获取一次通道消息
     */
    @Scheduled(cron = "0/30 * * * * ?")
    @Async("graphTaskExecutor")
    public void checkTeamChannelMessage() {
        if (!syncEnable) {
            return;
        }
        log.info("消息同步开始");
        List<Channels> channelList = channelsService.getList(null);
        Integer maxId = null;

        while (channelList != null && channelList.size() > 0) {
            try {
                Map<Integer, Channels> map = new HashMap<Integer, Channels>();
                //循环获取需要获取消息的频道
                List<GraphBatchRequestParam> requestList = new ArrayList<GraphBatchRequestParam>();
                for (Channels channel : channelList) {
                    maxId = channel.getId();
                    GraphBatchRequestParam request = new GraphBatchRequestParam();
                    request.setMethod(GraphBatchRequestParam.HttpMethod.GET);
                    request.setId(String.valueOf(channel.getId()));
                    if (StringUtils.isBlank(channel.getNextUrl())) {
                        request.setUrl("teams/" + channel.getMsTeamsId() + "/channels/" + channel.getMsId() + "/messages/delta");
                    } else {
                        request.setUrl(channel.getNextUrl());
                    }
                    requestList.add(request);
                    map.put(channel.getId(), channel);
                }
                //批量请求
                String result = restClient.createBatch(requestList, String.class);
//                log.info("response result:\n {}", result);

                //解析消息,并修改数据库中nextUrl字段
                List<AcceptMessage> messageList = channelsService.parseMessage(result);
                if (messageList.size() > 0) {
                    //封装接受消息，并添加至队列中
                    String messageJson = JSON.toJSONString(messageList);
                    rabbitSender.sendAcceptMsgToMQ(messageJson);
                }
                //防止请求过快
                Thread.sleep(1000);
            } catch (Exception e) {
                log.error("get Microsoft team channel message failed：{}", e);
            }
            channelList = channelsService.getList(maxId);
        }
        log.info("消息同步结束");
    }

    //自定义线程池
    @Bean(name = "graphTaskExecutor")
    public TaskExecutor getMyThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setMaxPoolSize(200);
        taskExecutor.setQueueCapacity(25);
        taskExecutor.setKeepAliveSeconds(200);
        taskExecutor.setThreadNamePrefix("Graph-");
        // 线程池对拒绝任务（无线程可用）的处理策略，目前只支持AbortPolicy、CallerRunsPolicy；默认为后者
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //调度器shutdown被调用时等待当前被调度的任务完成
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        //等待时长
        taskExecutor.setAwaitTerminationSeconds(60);
        taskExecutor.initialize();
        return taskExecutor;
    }


}
