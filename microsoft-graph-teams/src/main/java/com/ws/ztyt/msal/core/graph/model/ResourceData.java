package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * @Project ResourceData
 * @Title ResourceData.java
 * @Description 返回消息的 resource对象
 * @Package com.ws.ztyt.msal.core.graph.model
 * @author chentl
 * @date 2020/1/8 20:35
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 * @version V1.0
 */
@Data
public class ResourceData {

    @Expose
    @JsonProperty(value="id")
    private String id;

    @Expose
    @JsonProperty(value="@odata.type")
    private String dataType;

    @Expose
    @JsonProperty(value="@odata.id")
    private String dataId;

}
