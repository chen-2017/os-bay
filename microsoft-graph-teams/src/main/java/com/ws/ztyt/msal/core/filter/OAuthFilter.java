// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.ws.ztyt.msal.core.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Processes incoming requests based on auth status
 */
@Component
public class OAuthFilter implements Filter {

    private List<String> excludedUrls = Arrays.asList("/",
            "/ms/",
            "/ms/subscription/recieve",
            "/ms/subscription/lifecycle",
            "/identity");


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            //TODO 过滤器不需要在这里写，直接在gateway中判断 缓存中是否存在 MS_ACCESS_TOKEN
            //TODO                  存在则放过
            //TODO                  不存在则重新调用msal的获取令牌
            //TODO 此外还需要在 msal项目中使用定时器判断缓每隔一个小时检查一次是否存在 MS_ACCESS_TOKEN ,
            // todo                 存在则获取刷新令牌并调用刷新请求重新刷新令牌，然后更新缓存中的令牌；
            // todo                 不存在则重新获取token令牌并放入缓存

        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}
