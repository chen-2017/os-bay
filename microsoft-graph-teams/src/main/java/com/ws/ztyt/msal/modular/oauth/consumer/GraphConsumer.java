package com.ws.ztyt.msal.modular.oauth.consumer;

import com.ws.ztyt.msal.modular.oauth.service.GraphService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "apex-one")
public interface GraphConsumer extends GraphService {

}
