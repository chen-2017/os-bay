package com.ws.ztyt.msal.core.rabbitmq;

import com.ws.ztyt.msal.core.constant.MqQueueConstant;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Value("${spring.rabbitmq.host}")
    public String host;

    @Value("${spring.rabbitmq.port}")
    public int port;

    @Value("${spring.rabbitmq.username}")
    public String userName;

    @Value("${spring.rabbitmq.password}")
    public String password;

    @Value("${spring.rabbitmq.virtual-host}")
    public String virtualHost;

    @Bean(name = "printConnectionFactory")
    public ConnectionFactory inConnectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(userName);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);
        return connectionFactory;
    }

    @Bean(name = "printRabbitTemplate")
    public RabbitTemplate printRabbitTemplate(
            @Qualifier("printConnectionFactory") ConnectionFactory connectionFactory) throws Exception {
        RabbitTemplate printRabbitTemplate = new RabbitTemplate(connectionFactory);
        return printRabbitTemplate;
    }

    @Bean(name = "printContainerFactory")
    public SimpleRabbitListenerContainerFactory printFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            @Qualifier("printConnectionFactory") ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @Bean
    public Queue initChannelDispatch() {
        return new Queue(MqQueueConstant.CHANNEL_MESSAGE_QUEUE);
    }

    @Bean
    public Queue initTeamDispatch() {
        return new Queue(MqQueueConstant.TEAMS_QUEUE);
    }

    @Bean
    public Queue initChannelAcceptMQ() {
        return new Queue(MqQueueConstant.CHANNEL_MESSAGE_ACCEPT_QUEUE);
    }

    @Bean
    public Queue initChannelSubscriptionMQ() {
        return new Queue(MqQueueConstant.CHANNEL_SUBSCRIPTION_QUEUE);
    }

    @Bean
    public Queue initChannelMsgSubscriptionMQ() {
        return new Queue(MqQueueConstant.CHANNEL_MESSAGE_SUBSCRIPTION_QUEUE);
    }

    @Bean
    public Queue initSubscriptionRenewMQ() {
        return new Queue(MqQueueConstant.SUBSCRIPTION_RENEW_QUEUE);
    }
}
