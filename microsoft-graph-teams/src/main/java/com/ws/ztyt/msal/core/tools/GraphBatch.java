/**
 * @Project：
 * @Title：GraphBatch.java
 * @Description：
 * @Package com.ws.ztyt.msal.core.tools
 * @author：yangyucheng
 * @date：2019/12/30
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ws.ztyt.msal.core.tools;

import java.util.List;

public class GraphBatch {

    private List<GraphBatchRequestParam> requests;

    public List<GraphBatchRequestParam> getRequests() {
        return requests;
    }

    public void setRequests( List<GraphBatchRequestParam> requests ) {
        this.requests = requests;
    }
}
