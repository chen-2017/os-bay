package com.ws.ztyt.msal.core.graph.extensions;

import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.extensions.Team;
import com.microsoft.graph.requests.extensions.ITeamRequest;

public interface ITeamRequestExpand extends ITeamRequest {
    /**
     * Patches this Team with a source
     *
     * @param sourceTeam the source object with updates
     * @return the updated Team
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    Team put(final Team sourceTeam) throws ClientException;
}
