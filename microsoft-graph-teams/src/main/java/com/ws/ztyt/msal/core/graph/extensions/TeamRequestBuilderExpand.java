package com.ws.ztyt.msal.core.graph.extensions;

import com.microsoft.graph.core.IBaseClient;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.extensions.TeamRequestBuilder;

import java.util.List;

public class TeamRequestBuilderExpand extends TeamRequestBuilder {
    /**
     * The request builder for the Team
     *
     * @param requestUrl     the request URL
     * @param client         the service client
     * @param requestOptions the options for this request
     */
    public TeamRequestBuilderExpand(String requestUrl, IBaseClient client, List<? extends Option> requestOptions) {
        super(requestUrl, client, requestOptions);
    }

    /**
     * Creates the request with specific requestOptions instead of the existing requestOptions
     *
     * @param requestOptions the options for this request
     * @return the ITeamRequest instance
     */
    public ITeamRequestExpand buildPutRequest(final List<? extends Option> requestOptions) {
        return new TeamRequestExpand(getRequestUrl(), getClient(), requestOptions);
    }

    /**
     * Creates the request
     *
     * @return the ITeamRequest instance
     */
    public ITeamRequestExpand buildPutRequest() {
        return buildPutRequest(getOptions());
    }


}
