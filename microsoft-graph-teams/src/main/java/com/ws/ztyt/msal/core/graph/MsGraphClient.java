package com.ws.ztyt.msal.core.graph;

import com.microsoft.aad.msal4j.*;
import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.ws.ztyt.msal.core.support.SimpleAuthProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

/**
 * @author chentl
 * @version V1.0
 * @Project Graph
 * @Title Graph.java
 * @Description Graph API 客户端
 * @Package com.ws.ztyt.msal.core.graph
 * @date 2019/12/27 10:13
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@Slf4j
@Component
public class MsGraphClient {

    /**
     * The default endpoint for the Microsoft Graph Service
     */
    public static final String BETA_GRAPH_ENDPOINT = "https://graph.microsoft.com/beta";

    /**
     * 客户端id
     */
    @Value("${aad.clientId}")
    private String CLIENT_ID;

    /**
     * 客户端id
     */
    @Value("${aad.clientSecret}")
    private String CLIENT_SECRET;

    /**
     * 授权地址
     */
    @Value("${aad.authority}")
    private String AUTHORITY;

    /**
     * 授权地址
     */
    @Value("${aad.clientAuthority}")
    private String CLIENT_AUTHORITY;

    /**
     * 静态Graph客户端 v1.0
     */
    public static IGraphServiceClient graphClient = null;
    /**
     * 静态Graph客户端 beta
     */
    public static IGraphServiceClient graphClientBeta = null;
    /**
     * 静态Graph客户端 client（仅拥有应用程序权限，主要用于订阅）
     */
    public static IGraphServiceClient graphClientBase = null;


    /**
     * @return void
     * @author chentl
     * @description 获取 token 并 构造静态 graphClient
     * @date 2019/12/27 10:14
     * @params [accessToken]
     * @since JDK 1.7
     */
    public void ensureGraphClient(String accessToken) {
        // Create the auth provider
        SimpleAuthProvider authProvider = new SimpleAuthProvider(accessToken);

        // Create default logger to only log errors
        DefaultLogger logger = new DefaultLogger();
        logger.setLoggingLevel(LoggerLevel.DEBUG);

        // Build a Graph client
        graphClient = GraphServiceClient.builder()
                .authenticationProvider(authProvider)
                .logger(logger)
                .buildClient();
    }

    /**
     * 注册beta客户端
     *
     * @param accessToken
     */
    public void ensureGrapClientBeta(String accessToken) {
        // Create the auth provider
        SimpleAuthProvider authProvider = new SimpleAuthProvider(accessToken);

        // Create default logger to only log errors
        DefaultLogger logger = new DefaultLogger();
        logger.setLoggingLevel(LoggerLevel.DEBUG);

        // Build a Graph client
        graphClientBeta = GraphServiceClient.builder()
                .authenticationProvider(authProvider)
                .logger(logger)
                .buildClient();
        //设置测试版终结点
        graphClientBeta.setServiceRoot(BETA_GRAPH_ENDPOINT);
    }

    /**
     * 注册beta客户端
     *
     * @param clientToken
     */
    public void ensureGrapClientBase(String clientToken) {
        // Create the auth provider
        SimpleAuthProvider authProvider = new SimpleAuthProvider(clientToken);

        // Create default logger to only log errors
        DefaultLogger logger = new DefaultLogger();
        logger.setLoggingLevel(LoggerLevel.DEBUG);

        // Build a Graph client
        graphClientBase = GraphServiceClient.builder()
                .authenticationProvider(authProvider)
                .logger(logger)
                .buildClient();
        graphClientBase.setServiceRoot(BETA_GRAPH_ENDPOINT);

    }

    /**
     * 凭用户名 获取 access_token
     *
     * @param userName
     * @param password
     * @return
     * @throws MalformedURLException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public IAuthenticationResult getAccessToken(String userName, String password) {

        PublicClientApplication pca = null;
        try {
            pca = PublicClientApplication.builder(
                    CLIENT_ID).
                    authority(AUTHORITY).build();


            String scopes = "user.read openid profile  offline_access";
            UserNamePasswordParameters parameters = UserNamePasswordParameters.builder(
                    Collections.singleton(scopes),
                    userName,
                    password.toCharArray()).build();

            IAuthenticationResult result = pca.acquireToken(parameters).get();
            return result;
        } catch (MalformedURLException | InterruptedException | ExecutionException e) {
            log.error("根据用户名和密码获取授权时发生异常", e);

        }
        return null;
    }

    /**
     * 由配置好的 目录信息直接获取应用程序令牌
     *
     * @return
     */
    public IAuthenticationResult getAccessToken() {
        try {
            IClientSecret clientSecret = ClientCredentialFactory.createFromSecret(CLIENT_SECRET);
            ConfidentialClientApplication pca = ConfidentialClientApplication.builder(
                    CLIENT_ID, clientSecret)
                    .authority(CLIENT_AUTHORITY)
                    .build();

            //默认应用权限
            String scopes = "https://graph.microsoft.com/.default";

            ClientCredentialParameters parameters = ClientCredentialParameters.builder(
                    Collections.singleton(scopes)).build();


            IAuthenticationResult result = pca.acquireToken(parameters).get();
            return result;
        } catch (MalformedURLException | InterruptedException | ExecutionException e) {
            log.error("根据客戶端凭据流获取授权时发生异常", e);

        }
        return null;
    }

    /**
     * @return com.microsoft.aad.msal4j.IAuthenticationResult
     * @author chentl
     * @description 根据刷新令牌获取授权
     * @date 2019/12/27 16:32
     * @params [refreshToken]
     * @since JDK 1.7
     */
    public IAuthenticationResult refreshAccessToken(String refreshToken) {

        PublicClientApplication pca = null;
        try {
            pca = PublicClientApplication.builder(
                    CLIENT_ID).
                    authority(AUTHORITY).build();
            String scopes = "user.read openid profile  offline_access";
            RefreshTokenParameters parameters = RefreshTokenParameters.builder(
                    Collections.singleton(scopes),
                    refreshToken).build();

            IAuthenticationResult result = pca.acquireToken(parameters).get();
            return result;
        } catch (MalformedURLException | InterruptedException | ExecutionException e) {
            log.error("使用刷新令牌获取授权时发生异常", e);

        }
        return null;
    }


}