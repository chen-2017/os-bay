package com.ws.ztyt.msal.modular.oauth.service;

import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.modular.graph.model.Subscriptions;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chentl
 * @version V1.0
 * @Project GraphService
 * @Title GraphService.java
 * @Description 提供给msal服务调用的本地graph数据接口
 * @Package com.ws.ztyt.portal.modular.graph.service
 * @date 2019/12/31 13:38
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@RestController
@RequestMapping("/api/graph")
public interface GraphService {

    /**
     * @return void
     * @author chentl
     * @description 添加一个 Team
     * @date 2019/12/30 17:15
     * @params [teamsParam]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/addTeam", method = RequestMethod.POST)
    ResponseData addTeam(@RequestBody TeamsParam teamsParam);


    /**
     * @Project GraphService
     * @Title GraphService.java
     * @Description 添加一个频道
     * @Package com.ws.ztyt.portal.modular.graph.service
     * @author chentl
     * @date 2019/12/31 13:43
     * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
     * @version V1.0
     */
    @RequestMapping(value = "/addChannel", method = RequestMethod.POST)
    ResponseData addChannel(@RequestBody TeamsParam teamsParam);

    /**
     * @return com.ws.ztyt.core.reqres.response.ResponseData
     * @author chentl
     * @description 新增一条消息
     * @date 2020/1/1 14:11
     * @params [teamsParam]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/addMessages", method = RequestMethod.POST)
    ResponseData addMessages(@RequestBody TeamsParam teamsParam);

    /**
     * @return com.ws.ztyt.core.reqres.response.ResponseData
     * @author chentl
     * @description 新增一条来自订阅webHook接口的消息
     * @date 2020/1/8 23:13
     * @params [teamsParam]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/addSubMessages", method = RequestMethod.POST)
    ResponseData addSubMessages(@RequestBody TeamsParam teamsParam);

    /**
     * @return com.ws.ztyt.core.reqres.response.ResponseData
     * @author chentl
     * @description 新增一条消息
     * @date 2020/1/1 14:11
     * @params [teamsParam]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/addSubscriptions", method = RequestMethod.POST)
    ResponseData addSubscriptions(@RequestBody TeamsParam teamsParam);

    /**
     * @return com.ws.ztyt.core.reqres.response.ResponseData
     * @author chentl
     * @description 获取所有订阅
     * @date 2020/1/9 10:52
     * @params [expireMinutes]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/getAllSubscriptions", method = RequestMethod.POST)
    ResponseData getAllSubscriptions();

    /**
     * 更新订阅业务对象
     *
     * @param subscriptions
     * @return
     */
    @RequestMapping(value = "/updateSub", method = RequestMethod.POST)
    boolean updateSub(@RequestBody Subscriptions subscriptions);

    /**
     * @return boolean
     * @author chentl
     * @description 根据订阅id删除订阅信息
     * @date 2020/1/19 11:52
     * @params [subId]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/deleteSubById", method = RequestMethod.POST)
    boolean deleteSubById(Integer subId);

    /**
     * @return boolean
     * @author chentl
     * @description 获取 MsTeamsId
     * @date 2020/1/19 11:52
     * @params [subId]
     * @since JDK 1.7
     */
    @RequestMapping(value = "/getTeamById", method = RequestMethod.POST)
    String getTeamsMsId(Integer teamId);
}
