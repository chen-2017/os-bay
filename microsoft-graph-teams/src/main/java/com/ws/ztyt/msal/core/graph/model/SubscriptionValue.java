package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

import javax.annotation.Resource;

/**
 * @Project SubscriptionValue
 * @Title SubscriptionValue.java
 * @Description value对象
 * @Package com.ws.ztyt.msal.core.graph.model
 * @author chentl
 * @date 2020/1/8 20:34
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 * @version V1.0
 */
@Data
public class SubscriptionValue {

    @Expose
    @JsonProperty(value="subscriptionId")
    private String subscriptionId;

    @Expose
    @JsonProperty(value="changeType")
    private String changeType;

    @Expose
    @JsonProperty(value="tenantId")
    private String tenantId;

    @Expose
    @JsonProperty(value="clientState")
    private String clientState;

    @Expose
    @JsonProperty(value="subscriptionExpirationDateTime")
    private String subscriptionExpirationDateTime;

    @Expose
    @JsonProperty(value="resource")
    private String resource;

    @Expose
    @JsonProperty(value="resourceData")
    private ResourceData resourceData;

    @Expose
    @JsonProperty(value="encryptedContent")
    private EncryptedContent encryptedContent;

}
