// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.ws.ztyt.msal.modular.oauth.model;

import lombok.Getter;

import java.util.Date;

@Getter
public class StateData {
    private String nonce;
    private Date expirationDate;

    public StateData(String nonce, Date expirationDate) {
        this.nonce = nonce;
        this.expirationDate = expirationDate;
    }
}
