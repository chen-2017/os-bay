package com.ws.ztyt.msal.modular.oauth.provider;

import com.ws.ztyt.kernel.model.api.ResourceService;
import com.ws.ztyt.kernel.model.resource.ResourceDefinition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
public class ResourceServiceProvider implements ResourceService {

    @Override
    public void reportResources(String appCode, Map<String, Map<String, ResourceDefinition>> resourceDefinitions) {

    }

    @Override
    public Set<String> getUserResourceUrls(String accountId) {
        return null;
    }

    @Override
    public ResourceDefinition getResourceByUrl(String url) {
        return null;
    }

}
