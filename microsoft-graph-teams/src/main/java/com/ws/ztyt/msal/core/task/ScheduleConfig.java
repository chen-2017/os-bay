package com.ws.ztyt.msal.core.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.ThreadPoolExecutor;


/**
 * 所有的定时任务都放在一个线程池中，定时任务启动时使用不同都线程。
 */
@Configuration
@EnableAsync
public class ScheduleConfig implements SchedulingConfigurer {


    @Autowired
    private TaskScheduler myThreadPoolTaskScheduler;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        //简单粗暴的方式直接指定
        //scheduledTaskRegistrar.setScheduler(Executors.newScheduledThreadPool(5));
        //也可以自定义的线程池，方便线程的使用与维护，这里不多说了
        scheduledTaskRegistrar.setTaskScheduler(myThreadPoolTaskScheduler);
    }


    @Bean(name = "graphTaskScheduler")
    public TaskScheduler getMyThreadPoolTaskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(10);
        taskScheduler.setThreadNamePrefix("Graph-Scheduled-");
        taskScheduler.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //调度器shutdown被调用时等待当前被调度的任务完成
        taskScheduler.setWaitForTasksToCompleteOnShutdown(true);
        //等待时长
        taskScheduler.setAwaitTerminationSeconds(60);
        return taskScheduler;
    }
}