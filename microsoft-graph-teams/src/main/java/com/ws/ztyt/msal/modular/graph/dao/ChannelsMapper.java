package com.ws.ztyt.msal.modular.graph.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ws.ztyt.msal.modular.graph.model.Channels;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelsMapper extends BaseMapper<Channels> {


    List<Channels> getList(@Param("maxId") Integer maxId);
}
