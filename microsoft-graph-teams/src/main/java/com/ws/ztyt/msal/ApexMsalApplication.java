package com.ws.ztyt.msal;

import com.ws.ztyt.core.config.MybaitsPlusAutoConfiguration;
import com.ws.ztyt.core.util.PropertiesUtils;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.Resource;

/**
 * @author chentl
 * @version V1.0
 * @Project FfbaMsalApplication
 * @Title ApexOneApplication.java
 * @Description FFBA身份验证 & websocket服务
 * @Package com.ws.ztyt.msal
 * @date 2019/12/26 10:13
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@EnableFeignClients
@EnableAsync
@EnableCaching
@EnableDiscoveryClient
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"com.ws.**"})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, MybaitsPlusAutoConfiguration.class})
@Import(PropertiesUtils.class)
public class ApexMsalApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ApexMsalApplication.class, args);
    }

    @Resource
    private MsGraphService graphService;

    @Override
    public void run(String... args) {
        graphService.keepToken();
    }
}

