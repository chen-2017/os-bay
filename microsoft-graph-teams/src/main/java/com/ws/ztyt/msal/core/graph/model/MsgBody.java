package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @author chentl
 * @version V1.0
 * @Project MsgBody
 * @Title MsgBody.java
 * @Description 消息体对象（核心内容）
 * @Package com.ws.ztyt.msal.core.graph.model
 * @date 2020/1/8 22:03
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@Data
public class MsgBody {
    @Expose
    @JsonProperty(value = "contentType")
    private String contentType;

    @Expose
    @JsonProperty(value = "content")
    private String content;


}
