package com.ws.ztyt.msal.core.graph.extensions;

import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.requests.extensions.IChannelRequest;

public interface IChannelRequestExpand extends IChannelRequest {

    /**
     * Posts a Channel with a new object
     *
     * @param channelMessage the new object to create
     * @return the created Channel
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    Object sendMessage(final Object channelMessage) throws ClientException;

}
