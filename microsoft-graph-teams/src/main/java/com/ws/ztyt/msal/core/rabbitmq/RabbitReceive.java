package com.ws.ztyt.msal.core.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.config.RedisService;
import com.ws.ztyt.msal.core.constant.MqQueueConstant;
import com.ws.ztyt.msal.core.constant.SystemConstants;
import com.ws.ztyt.msal.core.constant.redis.RedisConstants;
import com.ws.ztyt.msal.core.graph.model.EncryptedContent;
import com.ws.ztyt.msal.core.graph.model.MsgResult;
import com.ws.ztyt.msal.core.graph.model.SubscriptionResult;
import com.ws.ztyt.msal.core.graph.model.SubscriptionValue;
import com.ws.ztyt.msal.core.tools.Pair;
import com.ws.ztyt.msal.core.tools.RSA;
import com.ws.ztyt.msal.core.tools.StringUtils;
import com.ws.ztyt.msal.core.tools.ValueUtils;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import com.ws.ztyt.msal.modular.oauth.service.GraphService;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URLDecoder;

/**
 * @version V1.0
 * @Project：ffba-portal
 * @Title：RabbitReceive.java
 * @Description：
 * @Package com.ws.ztyt.portal.core.rabbitmq
 * @author：xiehao
 * @date：2019年07月08日
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 */
@Slf4j
@Component
public class RabbitReceive {

    @Autowired
    private RedisService redisService;

    /**
     * 目录创建者的id cert@metoor.onmicrosoft.com
     */
    @Value("${aad.excludeTenantId}")
    private String excludeTenantId;

    @Autowired
    private MsGraphService msGraphService;

    @Autowired
    private GraphService graphService;

    @RabbitListener(queues = MqQueueConstant.CHANNEL_MESSAGE_QUEUE)
    @RabbitHandler
    public void process(String msgText) {
        try {
            log.info("收到频道消息 :{}", msgText);
            ResponseData result = msGraphService.sendMessage(JSONObject.parseObject(msgText, TeamsParam.class));
            if (result != null) {
                log.info(result.getMessage());
                log.info("频道消息处理结束");
            } else {
                log.error("频道消息处理失败，返回结果为空");
            }

        } catch (Exception e) {
            log.error("从MQ获取频道消息时发生异常", e);
            e.printStackTrace();
        }
    }

    @RabbitListener(queues = MqQueueConstant.TEAMS_QUEUE)
    @RabbitHandler
    public void teamMessageRecieve(String msgText) {
        try {
            log.info("收到团队消息 :{}", msgText);
            ResponseData result = msGraphService.generateTeam(JSONObject.parseObject(msgText, TeamsParam.class), true);
            if (result != null) {
                log.info("团队消息处理结束,返回参数:{} ", result);
            } else {
                log.error("团队消息处理失败，返回结果为空");
            }

        } catch (Exception e) {
            log.error("从MQ获取频道消息时发生异常", e);
        }
    }


    @RabbitListener(queues = MqQueueConstant.CHANNEL_SUBSCRIPTION_QUEUE)
    @RabbitHandler
    public void subscriptionMessages(String msgText) {
        try {
            log.info("收到待订阅频道ID 消息 :{}", msgText);
            log.info("频道订阅开始");
            ResponseData result = msGraphService.subscriptionChannel(JSONObject.parseObject(msgText, TeamsParam.class));
            if (result != null) {
                log.info("订阅消息处理结束,返回参数:{} ", result.getData());
            } else {
                log.error("订阅消息处理失败，返回结果为空");
            }

        } catch (Exception e) {
            log.error("从MQ获取订阅消息时发生异常", e);
        }
    }


    @RabbitListener(queues = MqQueueConstant.CHANNEL_MESSAGE_SUBSCRIPTION_QUEUE)
    @RabbitHandler
    public void storeSubscriptionMessages(String msgText) {
        try {
            log.info("收到待入库消息 :{}", msgText);
            log.info("订阅的频道消息准备入库");

            //解析接收到的消息
            ObjectMapper objectMapper = new ObjectMapper();
            SubscriptionResult result = objectMapper.readValue(msgText, SubscriptionResult.class);
            log.info("解析成功,本次接收到的消息数量为: " + ValueUtils.parseString(result.getValue().size()));

            //遍历消息列表
            for (SubscriptionValue value : result.getValue()) {
                //过滤掉自己发送的消息
                EncryptedContent cryptContent = value.getEncryptedContent();
                String resourceId = value.getResourceData().getId();
                String cacheResourceId = ValueUtils.parseString(redisService.getValue(RedisConstants.PREFIX_MS_RESOUCE_ID + resourceId));
                if (StringUtils.isNotBlank(cacheResourceId)) {
                    log.error("收到Teams发送的resourceId重复的消息，后者将被忽略");
                    continue;
                } else {
                    redisService.cacheValue(RedisConstants.PREFIX_MS_RESOUCE_ID + resourceId, resourceId, SystemConstants.RESOURCE_ID_OUT_SECS);
                    log.info("resourceId：{}，已缓存", resourceId);
                }
                String data = cryptContent.getData();
                String dataKey = cryptContent.getDataKey();
                String dataSignature = cryptContent.getDataSignature();
                //解密
                Pair<String, MsgResult> pair = RSA.decryptMsg(data, dataKey);
                if (pair != null) {
                    String realSignature = pair.first;
                    MsgResult realMsg = pair.second;
                    String userMsId = realMsg.getFrom().getUser().getId();
                    String ownerId = excludeTenantId;
                    //签名校验成功，且不是来自租户自己的消息，将不会被保存
                    if (dataSignature.equals(realSignature) && !ownerId.equals(userMsId)) {
                        log.info("签名校验成功，消息将被保存");
                        String msgContent = realMsg.getBody().getContent();
                        String msgContentType = realMsg.getBody().getContentType();
                        String msMessageId = realMsg.getId();
                        String createName = realMsg.getFrom().getUser().getDisplayName();
                        String webUrl = realMsg.getWebUrl();
                        String msChannelId = URLDecoder.decode(StringUtils.subString(webUrl, "message/", "thread.tacv2") + "thread.tacv2", "UTF-8");
                        log.info("消息解密成功，创建人: {},内容: {},类型: {},id: {},msChannelId: {}", createName, msgContent, msgContentType, msMessageId, msChannelId);
                        TeamsParam teamsParam = new TeamsParam(msMessageId,
                                msChannelId,
                                msgContent,
                                msgContentType,
                                createName);
                        ResponseData res = graphService.addSubMessages(teamsParam);
                        if (res != null && res.getCode() == 200) {
                            log.info("消息已保存至业务系统");
                        } else {
                            log.info("消息入库业务系统失败");
                        }
                    } else {
                        log.info("签名校验失败 or 本次消息由目录管理员发送将不会记录到业务系统中，本次消息将不被保存");
                    }
                } else {
                    log.error("解析订阅结果时出错，本次消息将被忽略，消息内容:" + msgText);
                }
            }

            log.info("订阅的频道消息处理结束");
        } catch (Exception e) {
            log.error("解析待入库的订阅消息时发生异常", e);
        }
    }

}
