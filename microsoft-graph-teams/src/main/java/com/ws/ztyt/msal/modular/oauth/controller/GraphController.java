package com.ws.ztyt.msal.modular.oauth.controller;

import cn.hutool.core.util.ReflectUtil;
import com.alibaba.fastjson.JSONObject;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.requests.extensions.IGroupCollectionPage;
import com.ws.ztyt.core.reqres.response.ErrorResponseData;
import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.kernel.scanner.modular.stereotype.ApiResource;
import com.ws.ztyt.msal.config.RedisService;
import com.ws.ztyt.msal.core.constant.SystemConstants;
import com.ws.ztyt.msal.core.constant.redis.RedisConstants;
import com.ws.ztyt.msal.core.rabbitmq.RabbitSender;
import com.ws.ztyt.msal.core.support.JsonHandler;
import com.ws.ztyt.msal.core.tools.StringUtils;
import com.ws.ztyt.msal.core.tools.ValueUtils;
import com.ws.ztyt.msal.modular.oauth.consumer.GraphConsumer;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chentl
 * @version V1.0
 * @Project GraphController
 * @Title GraphController.java
 * @Description TODO 此类仅供msal服务 接口测试  随时可以删除
 * @Package com.ws.ztyt.msal.modular.oauth.controller
 * @date 2020/1/1 14:40
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@RestController
@ApiResource(name = "Identity", path = "/identity")
public class GraphController {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());


    @Value("${aad.username}")
    private String username;

    @Value("${aad.password}")
    private String password;

    @Autowired
    private RedisService redisService;

    @Autowired
    private MsGraphService graphService;

    @Autowired
    private RabbitSender rabbitSender;

    @Autowired
    private GraphConsumer graphConsumer;

    @RequestMapping(value = "/test")
    public ResponseData identityTest() {
        log.info("==========Microsoft Azure 身份认证流程开始==========");
        IAuthenticationResult result = graphService.getAccessTokenByUserBase(username, password);
        IAuthenticationResult client = graphService.getAccessTokenForClient();
        String accessToken = result.accessToken();
        String clientToken = client.accessToken();
        log.info("当前可用的身份令牌：" + accessToken);
        log.info("当前可用的客户端令牌：" + clientToken);
        String refreshToken = ValueUtils.parseString(ReflectUtil.getFieldValue(result, "refreshToken"));
        redisService.cacheValue(RedisConstants.MS_ACCESS_TOKEN, accessToken, SystemConstants.DEFAULT_MS_TOKEN_TIME_OUT_SECS);
        redisService.cacheValue(RedisConstants.MS_REFRESH_TOKEN, refreshToken, SystemConstants.DEFAULT_MS_TOKEN_TIME_OUT_SECS);
        redisService.cacheValue(RedisConstants.MS_CLIENT_TOKEN, refreshToken, SystemConstants.DEFAULT_MS_TOKEN_TIME_OUT_SECS);
        graphService.registerGraphClient(accessToken, clientToken);
        log.info("GraphClient 注册成功，现在开始使用Microsoft Graph提供的api服务吧~");
        return ResponseData.success(200, "接口调用成功", JsonHandler.parseJson(result));
    }


    @RequestMapping(value = "/user")
    public ResponseData getUserInfo() {
        User user = graphService.getUserInfo();
        return ResponseData.success(200, "获取用户信息成功", JsonHandler.parseJson(user.getRawObject()));
    }

    @RequestMapping(value = "/groups")
    public Object getTeams() {
        IGroupCollectionPage groupPage = graphService.getGroups();
        return ResponseData.success(200, "获取团队信息成功", JsonHandler.parseJson(groupPage.getRawObject()));
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public ResponseData createGroup(TeamsParam teamsParam) {
        ResponseData result = graphService.generateTeam(teamsParam, true);
        return ResponseData.success(200, "团队已生成", result);
    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public ResponseData sendMessage(TeamsParam teamsParam) {
        if (StringUtils.isBlank(teamsParam.getMsTeamId())) {
            log.debug("团队标识不能为空");
            return new ErrorResponseData("团队标识不能为空");
        }
        if (StringUtils.isBlank(teamsParam.getMsChannelId())) {
            log.debug("频道标识不能为空");
            return new ErrorResponseData("频道标识不能为空");
        }
        if (StringUtils.isBlank(teamsParam.getContent())) {
            log.debug("消息内容不能为空");
            return new ErrorResponseData("消息内容不能为空");
        }
        rabbitSender.sendChannelMsg(JSONObject.toJSONString(teamsParam));
//        Object result = graphService.sendMessage(teamsParam);
        return ResponseData.success(200, "消息已发送到队列中", null);
    }

}
