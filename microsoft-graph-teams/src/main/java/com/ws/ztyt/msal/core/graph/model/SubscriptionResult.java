package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

import java.util.List;

/**
 * @author chentl
 * @version V1.0
 * @Project SubscriptionResult
 * @Title SubscriptionResult.java
 * @Description 接收订阅的消息通知对象
 * @Package com.ws.ztyt.msal.core.graph.model
 * @date 2020/1/8 20:32
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@Data
public class SubscriptionResult {

    @Expose
    @JsonProperty(value = "value")
    private List<SubscriptionValue> value;

    @Expose
    @JsonProperty(value = "validationTokens")
    private String[] validationTokens;


}
