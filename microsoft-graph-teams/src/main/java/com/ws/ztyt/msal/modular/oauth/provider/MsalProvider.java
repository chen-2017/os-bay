package com.ws.ztyt.msal.modular.oauth.provider;

import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.core.rabbitmq.RabbitSender;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import com.ws.ztyt.msal.modular.oauth.service.MsalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class MsalProvider implements MsalService {


    @Autowired
    private MsGraphService graphService;

    @Override
    public ResponseData createTeam(@RequestBody TeamsParam teamsParam) {
        ResponseData result = graphService.generateTeam(teamsParam, false);
        return result;
    }

    @Override
    public ResponseData createChannel(@RequestBody TeamsParam teamsParam) {
        ResponseData result = graphService.createChannel(teamsParam);
        return result;
    }

    @Override
    public ResponseData sendMessages(@RequestBody TeamsParam teamsParam) {
        ResponseData result = graphService.sendMessage(teamsParam);
        return result;
    }
}
