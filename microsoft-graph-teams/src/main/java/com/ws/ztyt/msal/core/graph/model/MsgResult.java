package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

import java.util.List;

/**
 * @author chentl
 * @version V1.0
 * @Project MsgResult
 * @Title MsgResult.java
 * @Description 消息真实内容模板
 * @Package com.ws.ztyt.msal.core.graph.model
 * @date 2020/1/8 21:59
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
@Data
public class MsgResult {
    @Expose
    @JsonProperty(value = "id")
    private String id;

    @Expose
    @JsonProperty(value = "replyToId")
    private String replyToId;

    @Expose
    @JsonProperty(value = "etag")
    private String etag;

    @Expose
    @JsonProperty(value = "messageType")
    private String messageType;

    @Expose
    @JsonProperty(value = "createdDateTime")
    private String createdDateTime;

    @Expose
    @JsonProperty(value = "lastModifiedDateTime")
    private String lastModifiedDateTime;

    @Expose
    @JsonProperty(value = "deletedDateTime")
    private String deletedDateTime;

    @Expose
    @JsonProperty(value = "subject")
    private String subject;

    @Expose
    @JsonProperty(value = "summary")
    private String summary;

    @Expose
    @JsonProperty(value = "importance")
    private String importance;

    @Expose
    @JsonProperty(value = "locale")
    private String locale;

    @Expose
    @JsonProperty(value = "webUrl")
    private String webUrl;

    @Expose
    @JsonProperty(value = "from")
    private MsgFrom from;

    @Expose
    @JsonProperty(value = "body")
    private MsgBody body;

    @Expose
    @JsonProperty(value = "attachments")
    private List<String> attachments;

    @Expose
    @JsonProperty(value = "mentions")
    private List<String> mentions;

    @Expose
    @JsonProperty(value = "reactions")
    private List<String> reactions;

    @Expose
    @JsonProperty(value = "replies")
    private List<String> replies;

    @Expose
    @JsonProperty(value = "hostedContents")
    private List<String> hostedContents;

    @Expose
    @JsonProperty(value = "policyViolation")
    private String policyViolation;

}
