package com.ws.ztyt.msal.modular.graph.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

@TableName("ms_graph_channels")
public class Channels extends Model<Channels> {

    private static final long serialVersionUID = 1L;

    public Channels(){

    }

    public Channels(String msId, String readStatus, Integer teamsId, Integer userId, Integer deptId, String displayName, String description, String type, String nextUrl, Integer sourceId) {
        this.msId = msId;
        this.readStatus = readStatus;
        this.teamsId = teamsId;
        this.userId = userId;
        this.deptId = deptId;
        this.displayName = displayName;
        this.description = description;
        this.type = type;
        this.nextUrl = nextUrl;
        this.sourceId = sourceId;
    }

    private Integer id;
    /**
     * 对应微软的频道id
     */
    private String msId;
    /**
     * 是否有未读消息 | 0 否 1 是
     */
    private String readStatus;
    /**
     * 对应的业务teamsId
     */
    private Integer teamsId;

    /**
     * microsoft team Id
     */
    @TableField(exist = false)
    private String msTeamsId;

    /**
     * 用户业务id
     */
    private Integer userId;
    /**
     * 用户部门id
     */
    private Integer deptId;
    /**
     * 频道的名称
     */
    private String displayName;
    /**
     * 频道的描述
     */
    private String description;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 下一次请求获取频道消息的URL
     */
    private String nextUrl;
    /**
     * 频道类型
     */
    private String type;
    /**
     * 元数据id | 即单号对应的Id
     */
    private Integer sourceId;

    @TableField(exist = false)
    private String content;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsId() {
        return msId;
    }

    public void setMsId(String msId) {
        this.msId = msId;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public Integer getTeamsId() {
        return teamsId;
    }

    public void setTeamsId(Integer teamsId) {
        this.teamsId = teamsId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public String getMsTeamsId() {
        return msTeamsId;
    }

    public void setMsTeamsId( String msTeamsId ) {
        this.msTeamsId = msTeamsId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Channels{" +
                ", id=" + id +
                ", msId=" + msId +
                ", readStatus=" + readStatus +
                ", teamsId=" + teamsId +
                ", userId=" + userId +
                ", deptId=" + deptId +
                ", displayName=" + displayName +
                ", description=" + description +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", nextUrl=" + nextUrl +
                "}";
    }
}
