/**
 * @Project：
 * @Title：MqQueueConstant.java
 * @Description：
 * @Package com.ws.ztyt.core.common.constant
 * @author：yangyucheng
 * @date：2019/3/13
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ws.ztyt.msal.core.constant;

public interface MqQueueConstant {


    String CHANNEL_MESSAGE_QUEUE = "CHANNEL_MESSAGE_QUEUE";

    String TEAMS_QUEUE = "TEAMS_QUEUE";

    /**
     * 频道消息接受队列
     */
    String CHANNEL_MESSAGE_ACCEPT_QUEUE = "CHANNEL_MESSAGE_ACCEPT_QUEUE";
    /**
     * 待订阅的频道队列
     */
    String CHANNEL_SUBSCRIPTION_QUEUE = "CHANNEL_SUBSCRIPTION_QUEUE";
    /**
     * 订阅消息入库队列
     */
    String CHANNEL_MESSAGE_SUBSCRIPTION_QUEUE = "CHANNEL_MESSAGE_SUBSCRIPTION_QUEUE";
    /**
     * 业务系统订阅续订队列
     */
    String SUBSCRIPTION_RENEW_QUEUE = "SUBSCRIPTION_RENEW_QUEUE";


}
