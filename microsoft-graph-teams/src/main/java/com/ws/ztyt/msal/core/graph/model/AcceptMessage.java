/**
 * @Project：
 * @Title：AcceptMessage.java
 * @Description：
 * @Package com.ws.ztyt.msal.core.graph.model
 * @author：yangyucheng
 * @date：2019/12/31
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ws.ztyt.msal.core.graph.model;

import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;

import java.io.Serializable;

public class AcceptMessage implements Serializable {

    public AcceptMessage(Integer channelId, String contentType, String content, String userName, String msTeamId, String msChannelId, String msMessageId) {
        this.channelId = channelId;
        this.contentType = contentType;
        this.content = content;
        this.userName = userName;
        this.msTeamId = msTeamId;
        this.msChannelId = msChannelId;
        this.msMessageId = msMessageId;
    }

    public TeamsParam convertToMessages() {
        TeamsParam teamsParam = new TeamsParam(
                this.msMessageId,
                this.content,
                this.contentType,
                this.userName,
                this.channelId
        );
        return teamsParam;
    }

    public AcceptMessage() {

    }

    private Integer channelId;

    private String contentType;

    private String content;

    private String userName;

    private String msTeamId;

    private String msChannelId;

    private String msMessageId;

    public Integer getChannelId() {
        return channelId;
    }

    public String getMsMessageId() {
        return msMessageId;
    }

    public void setMsMessageId(String msMessageId) {
        this.msMessageId = msMessageId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMsTeamId() {
        return msTeamId;
    }

    public void setMsTeamId(String msTeamId) {
        this.msTeamId = msTeamId;
    }

    public String getMsChannelId() {
        return msChannelId;
    }

    public void setMsChannelId(String msChannelId) {
        this.msChannelId = msChannelId;
    }
}
