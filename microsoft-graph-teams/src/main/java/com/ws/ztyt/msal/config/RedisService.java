package com.ws.ztyt.msal.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisService {
    /**
     * 前缀
     */

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 日志记录
     */
    private final static Logger logger = LoggerFactory.getLogger(RedisService.class);

    /**
     * 缓存value操作
     *
     * @param k    key
     * @param v    value
     * @param time time
     * @return boolean
     */
    public boolean cacheValue(String k, Object v, long time) {
        try {
            ValueOperations valueOps = redisTemplate.opsForValue();
            valueOps.set(k, v);
            if (time > 0) {
                redisTemplate.expire(k, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            logger.error("缓存[" + k + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存value操作
     *
     * @param k key
     * @param v value
     * @return boolean
     */
    public boolean cacheValue(String k, Object v) {
        return cacheValue(k, v, -1);
    }

    /**
     * 判断缓存是否存在
     *
     * @param k key
     * @return boolean
     */
    public boolean containsValueKey(String k) {
        return containsKey(k);
    }

    /**
     * 判断缓存是否存在
     *
     * @param k key
     * @return boolean
     */
    public boolean containsSetKey(String k) {
        return containsKey(k);
    }

    /**
     * 判断缓存是否存在
     *
     * @param k key
     * @return boolean
     */
    public boolean containsListKey(String k) {
        return containsKey(k);
    }

    public boolean containsKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Throwable t) {
            logger.error("判断缓存存在失败key[" + key + ", Codeor[" + t + "]");
        }
        return false;
    }

    /**
     * 获取缓存
     *
     * @param k key
     * @return string
     */
    public Object getValue(String k) {
        try {
            ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();
            return valueOps.get(k);
        } catch (Throwable t) {
            logger.error("获取缓存失败key[" + k + ", Codeor[" + t + "]");
        }
        return null;
    }

    /**
     * 移除缓存
     *
     * @param k key
     * @return boolean
     */
    public boolean removeValue(String k) {
        return remove(k);
    }

    public boolean removeSet(String k) {
        return remove(k);
    }

    public boolean removeList(String k) {
        return remove(k);
    }


    /**
     * 缓存set操作
     *
     * @param key  key
     * @param v    value
     * @param time time
     * @return boolean
     */
    public boolean cacheSet(String key, String v, long time) {
        //String key = k;
        try {
            SetOperations<String, Object> valueOps = redisTemplate.opsForSet();
            valueOps.add(key, v);
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            logger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存set
     *
     * @param k key
     * @param v value
     * @return boolean
     */
    public boolean cacheSet(String k, String v) {
        return cacheSet(k, v, -1);
    }

    public boolean removeAllByPrefix(String fix) {
        try {
            Set<String> keys = redisTemplate.keys(fix + "*");
            redisTemplate.delete(keys);
            return true;
        } catch (Exception e) {
            logger.error("删除失败");
        }
        return false;
    }

    /**
     * 缓存set
     *
     * @param key  key
     * @param v    value
     * @param time time
     * @return boolean
     */
    public boolean cacheSet(String key, Set<String> v, long time) {
        try {
            SetOperations<String, Object> setOps = redisTemplate.opsForSet();
            setOps.add(key, v.toArray(new String[v.size()]));
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            logger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存set
     *
     * @param k key
     * @param v value
     * @return boolean
     */
    public boolean cacheSet(String k, Set<String> v) {
        return cacheSet(k, v, -1);
    }

    /**
     * 获取缓存set数据
     *
     * @param k key
     * @return set
     */
    public Set<Object> getSet(String k) {
        try {
            SetOperations<String, Object> setOps = redisTemplate.opsForSet();
            return setOps.members(k);
        } catch (Throwable t) {
            logger.error("获取set缓存失败key[" + k + ", Codeor[" + t + "]");
        }
        return null;
    }

    /**
     * list缓存
     *
     * @param key  key
     * @param v    value
     * @param time time
     * @return boolean
     */
    public boolean cacheList(String key, String v, long time) {
        try {
            ListOperations<String, Object> listOps = redisTemplate.opsForList();
            listOps.rightPush(key, v);
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            logger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存list
     *
     * @param k key
     * @param v value
     * @return boolean
     */
    public boolean cacheList(String k, String v) {
        return cacheList(k, v, -1);
    }

    /**
     * 缓存list
     *
     * @param key  key
     * @param v    value
     * @param time time
     * @return boolean
     */
    public boolean cacheList(String key, List<String> v, long time) {
        try {
            ListOperations<String, Object> listOps = redisTemplate.opsForList();
            listOps.rightPushAll(key, v);
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            logger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存list
     *
     * @param k key
     * @param v value
     * @return boolean
     */
    public boolean cacheList(String k, List<String> v) {
        return cacheList(k, v, -1);
    }

    /**
     * 获取list缓存
     *
     * @param k     key
     * @param start start
     * @param end   end
     * @return list
     */
    public List<Object> getList(String k, long start, long end) {
        try {
            ListOperations<String, Object> listOps = redisTemplate.opsForList();
            return listOps.range(k, start, end);
        } catch (Throwable t) {
            logger.error("获取list缓存失败key[" + k + ", Codeor[" + t + "]");
        }
        return null;
    }

    /**
     * 获取总条数, 可用于分页
     *
     * @param k key
     * @return long
     */
    public long getListSize(String k) {
        try {
            ListOperations<String, Object> listOps = redisTemplate.opsForList();
            return listOps.size(k);
        } catch (Throwable t) {
            logger.error("获取list长度失败key[" + k + "], Codeor[" + t + "]");
        }
        return 0;
    }

    /**
     * 获取总条数, 可用于分页
     *
     * @param listOps listOps
     * @param k       k
     * @return long
     */
    public long getListSize(ListOperations<String, String> listOps, String k) {
        try {
            return listOps.size(k);
        } catch (Throwable t) {
            logger.error("获取list长度失败key[" + k + "], Codeor[" + t + "]");
        }
        return 0;
    }

    /**
     * 移除list缓存
     *
     * @param key k
     * @return boolean
     */
    public boolean removeOneOfList(String key) {
        try {
            ListOperations<String, Object> listOps = redisTemplate.opsForList();
            listOps.rightPop(key);
            return true;
        } catch (Throwable t) {
            logger.error("移除list缓存失败key[" + key + ", Codeor[" + t + "]");
        }
        return false;
    }

    /**
     * 移除缓存
     *
     * @param key key
     * @return boolean
     */
    private boolean remove(String key) {
        try {
            redisTemplate.delete(key);
            return true;
        } catch (Throwable t) {
            logger.error("获取缓存失败key[" + key + ", Codeor[" + t + "]");
        }
        return false;
    }

    /**
     * expire:(重置KEY存活时间).
     *
     * @param key
     * @param time
     * @author taofangtao
     * @since JDK 1.7
     */
    public void expire(String key, long time) {
        redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

}

