/**   
* @Project：ffba-portal
* @Title：RestTemplateConfig.java   
* @Description：   
* @Package com.ws.config.properties
* @author：xiehao
* @date：2019年7月3日
* @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
* @version V1.0
*/ 
package com.ws.ztyt.msal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
    
    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory factory){
        return new RestTemplate(factory);
    }

    @Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        //以下单位均为ms
        factory.setReadTimeout(8000);
        factory.setConnectTimeout(8000);
        return factory;
    }

}
