package com.ws.ztyt.msal.core.constant;

public interface GraphConst {

    String GET_USER = "https://graph.microsoft.com/v1.0/users/";


    String MEMBERS_BIND = "members@odata.bind";

    String CHANNEL_DEFAULT_DESCRIPTION = "This channel is created by default";

    //来自Teams的异常
    String RESOURCE_NOT_FOUND = "ResourceNotFound";
}
