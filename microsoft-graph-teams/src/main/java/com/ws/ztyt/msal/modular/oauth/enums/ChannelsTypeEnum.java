package com.ws.ztyt.msal.modular.oauth.enums;

/**
 * @author chentl
 * @version V1.0
 * @Project ReadEnum
 * @Title ReadEnum.java
 * @Description 阅读枚举类
 * @Package com.ws.ztyt.portal.modular.graph.enums
 * @date 2019/12/31 14:37
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 */
public enum ChannelsTypeEnum {
    BOOKING("0", "Booking"),
    SHIPMENT("1", "Shipment"),
    QUOTE("2", "Customs"),
    BILLING("3", "Billing");

    ChannelsTypeEnum(String code, String body) {
        this.code = code;
        this.body = body;
    }

    private String code;

    private String body;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
