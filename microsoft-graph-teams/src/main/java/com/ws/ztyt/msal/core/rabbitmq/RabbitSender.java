package com.ws.ztyt.msal.core.rabbitmq;

import com.ws.ztyt.msal.core.constant.MqQueueConstant;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * @return void
     * @author chentl
     * @description 发送频道消息到队列中
     * @date 2019/12/30 16:15
     * @params [messageText]
     * @since JDK 1.7
     */
    public void sendChannelMsg(String messageText) {
        rabbitTemplate.convertAndSend(MqQueueConstant.CHANNEL_MESSAGE_QUEUE, messageText);
    }

    /**
     * @return void
     * @author chentl
     * @description 发送团队消息到队列
     * @date 2019/12/30 17:20
     * @params [messageText]
     * @since JDK 1.7
     */
    public void sendTeamMsg(String messageText) {
        rabbitTemplate.convertAndSend(MqQueueConstant.TEAMS_QUEUE, messageText);
    }

    /**
     * 发送接受队列到消息对队列
     *
     * @param message
     */
    public void sendAcceptMsgToMQ(String message) {
        rabbitTemplate.convertAndSend(MqQueueConstant.CHANNEL_MESSAGE_ACCEPT_QUEUE, message);
    }


    /**
     * 发送消息到订阅请求队列
     *
     * @param messageText
     */
    public void sendSubscriptionMsg(String messageText) {
        rabbitTemplate.convertAndSend(MqQueueConstant.CHANNEL_SUBSCRIPTION_QUEUE, messageText);
    }

    /**
     * 发送消息到 订阅消息的入库队列
     *
     * @param messageText
     */
    public void sendSubscriptionStoreMsg(String messageText) {
        rabbitTemplate.convertAndSend(MqQueueConstant.CHANNEL_MESSAGE_SUBSCRIPTION_QUEUE, messageText);
    }

    /**
     * 发送消息到 订阅消息的续订队列
     *
     * @param messageText
     */
    public void sendSubscriptionRenewMsg(String messageText) {
        rabbitTemplate.convertAndSend(MqQueueConstant.SUBSCRIPTION_RENEW_QUEUE, messageText);
    }
}
