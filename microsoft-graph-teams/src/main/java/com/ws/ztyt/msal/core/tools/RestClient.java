package com.ws.ztyt.msal.core.tools;

import com.alibaba.fastjson.JSON;
import com.ws.ztyt.msal.config.RedisService;
import com.ws.ztyt.msal.core.constant.redis.RedisConstants;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Slf4j
@Component
public class RestClient {

    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private MsGraphService graphService;

    @Autowired
    private RedisService redisService;

    private static final HttpHeaders HEADERS = new HttpHeaders();

    /**
     * 批量请求URL(固定)
     */
    public final static String URL_REQUEST_BATCH = "https://graph.microsoft.com/beta/$batch";

    public void initGraph() {
        HEADERS.clear();
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }


    /**
     * 批量调用Graph接口请求
     * @param requestList : 变量请求对象封装
     * @param responseType
     * @param <F>
     * @param <T>
     * @return
     */
    public <F, T> F createBatch( List<GraphBatchRequestParam> requestList, Class<F> responseType ) {
        try {
            initGraph();
            HEADERS.add("Authorization", "Bearer "+ValueUtils.parseString(redisService.getValue( RedisConstants.MS_ACCESS_TOKEN)));
            GraphBatch batch = new GraphBatch();
            batch.setRequests( requestList );
            String jsonStr = JSON.toJSONString( batch );
//            log.info( "request json:\n {}",jsonStr );
            HttpEntity formEntity = new HttpEntity( jsonStr, HEADERS );
            ResponseEntity<F> responseEntity = this.restTemplate.postForEntity( URL_REQUEST_BATCH, formEntity, responseType );
            return responseEntity.getBody();
        } catch ( Exception e ) {
            log.error( "batch request error:", e );
            e.printStackTrace();
        }
        return null;
    }


}
