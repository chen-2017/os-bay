package com.ws.ztyt.msal.core.graph.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * @Project MsgFrom
 * @Title MsgFrom.java
 * @Description 消息来源对象
 * @Package com.ws.ztyt.msal.core.graph.model
 * @author chentl
 * @date 2020/1/8 22:01
 * @Copyright: 上海顺益信息科技有限公司 All rights reserved.
 * @version V1.0
 */
@Data
public class MsgFrom {
    @Expose
    @JsonProperty(value = "application")
    private String application;

    @Expose
    @JsonProperty(value = "device")
    private String device;

    @Expose
    @JsonProperty(value = "user")
    private MsgUser user;

    @Expose
    @JsonProperty(value = "conversation")
    private String conversation;


}
