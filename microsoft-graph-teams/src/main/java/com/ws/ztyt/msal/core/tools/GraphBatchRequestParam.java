/**
 * @Project：
 * @Title：GraphBatchRequestParam.java
 * @Description：Microsoft批量请求参数类
 * @Package com.ws.ztyt.msal.core.tools
 * @author：yangyucheng
 * @date：2019/12/30
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ws.ztyt.msal.core.tools;

public class GraphBatchRequestParam {

    /**
     * 请求的ID("1","2","3")
     */
    private String id;

    /**
     * 批量请求method(POST,GET)
     */
    private HttpMethod method;

    /**
     * 批量请求URL
     */
    private String url;
    /**
     * 请求数据体
     */
    private Object body;

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public enum HttpMethod {

        GET("GET"),
        POST("POST"),
        PATCH("PATCH");

        private String method;

        HttpMethod(String method) {
            this.method = method;
        }
    }

}
