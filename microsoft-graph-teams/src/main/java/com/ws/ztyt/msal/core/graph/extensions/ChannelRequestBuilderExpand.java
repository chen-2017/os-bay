package com.ws.ztyt.msal.core.graph.extensions;// ------------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All Rights Reserved.  Licensed under the MIT License.  See License in the project root for license information.
// ------------------------------------------------------------------------------


import com.microsoft.graph.core.IBaseClient;
import com.microsoft.graph.http.BaseRequestBuilder;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.extensions.*;


/**
 * The class for the Channel Request Builder.
 */
public class ChannelRequestBuilderExpand extends BaseRequestBuilder implements IChannelRequestBuilder {

    /**
     * The request builder for the Channel
     *
     * @param requestUrl     the request URL
     * @param client         the service client
     * @param requestOptions the options for this request
     */
    public ChannelRequestBuilderExpand(final String requestUrl, final IBaseClient client, final java.util.List<? extends Option> requestOptions) {
        super(requestUrl, client, requestOptions);
    }

    /**
     * Creates the request
     *
     * @return the IChannelRequest instance
     */
    @Override
    public IChannelRequestExpand buildRequest() {
        return buildRequestExpand(getOptions());
    }

    /**
     * Creates the request with specific requestOptions instead of the existing requestOptions
     *
     * @param requestOptions the options for this request
     * @return the IChannelRequest instance
     */
    @Override
    public IChannelRequest buildRequest(final java.util.List<? extends Option> requestOptions) {
        return new ChannelRequest(getRequestUrl(), getClient(), requestOptions);
    }

    /**
     * Creates the request with specific requestOptions instead of the existing requestOptions
     *
     * @param requestOptions the options for this request
     * @return the IChannelRequest instance
     */
    public IChannelRequestExpand buildRequestExpand(final java.util.List<? extends Option> requestOptions) {
        return new ChannelRequestExpand(getRequestUrl(), getClient(), requestOptions);
    }


    @Override
    public ITeamsTabCollectionRequestBuilder tabs() {
        return new TeamsTabCollectionRequestBuilder(getRequestUrlWithAdditionalSegment("tabs"), getClient(), null);
    }

    @Override
    public ITeamsTabRequestBuilder tabs(final String id) {
        return new TeamsTabRequestBuilder(getRequestUrlWithAdditionalSegment("tabs") + "/" + id, getClient(), null);
    }
}

