package com.ws.ztyt.msal.modular.oauth.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.graph.http.GraphServiceException;
import com.microsoft.graph.models.extensions.*;
import com.microsoft.graph.models.generated.GiphyRatingType;
import com.microsoft.graph.requests.extensions.IGroupCollectionPage;
import com.ws.ztyt.core.reqres.response.ResponseData;
import com.ws.ztyt.msal.config.RedisService;
import com.ws.ztyt.msal.core.constant.GraphConst;
import com.ws.ztyt.msal.core.constant.SystemConstants;
import com.ws.ztyt.msal.core.constant.redis.RedisConstants;
import com.ws.ztyt.msal.core.graph.MsGraphClient;
import com.ws.ztyt.msal.core.graph.extensions.ChannelRequestBuilderExpand;
import com.ws.ztyt.msal.core.graph.extensions.TeamRequestBuilderExpand;
import com.ws.ztyt.msal.core.graph.model.SubscriptionExpand;
import com.ws.ztyt.msal.core.rabbitmq.RabbitSender;
import com.ws.ztyt.msal.core.support.JsonHandler;
import com.ws.ztyt.msal.core.tools.RestClient;
import com.ws.ztyt.msal.core.tools.StringUtils;
import com.ws.ztyt.msal.core.tools.ValueUtils;
import com.ws.ztyt.msal.modular.graph.model.Channels;
import com.ws.ztyt.msal.modular.graph.model.Subscriptions;
import com.ws.ztyt.msal.modular.graph.service.IChannelsService;
import com.ws.ztyt.msal.modular.oauth.consumer.GraphConsumer;
import com.ws.ztyt.msal.modular.oauth.enums.ChannelsTypeEnum;
import com.ws.ztyt.msal.modular.oauth.model.ChannelMessage;
import com.ws.ztyt.msal.modular.oauth.model.MessageBody;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import com.ws.ztyt.msal.modular.oauth.service.MsGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import java.util.List;

@Slf4j
@Service
public class MsGraphServiceImpl implements MsGraphService {

    /**
     * 订阅超时时间
     */
    public static final String EXPIRATION_DATE_TIME = "expirationDateTime";
    /**
     * 批量请求等待时间
     */
    public static final int WAIT_MILLIS = 1000;
    @Autowired
    private RabbitSender rabbitSender;

    @Value("${aad.username}")
    private String username;

    @Value("${aad.password}")
    private String password;

    @Value("${aad.subscription.enable}")
    private boolean subscriptionEnable;

    @Autowired
    private RestClient restClient;

    @Value("${aad.encryptionCertificate}")
    private String encryptionCertificate;

    @Value("${aad.encryptionCertificateId}")
    private String encryptionCertificateId;

    @Value("${aad.subscription.expireMinute}")
    private String expireMinute;

    @Value("${aad.subscription.changeType}")
    private String changeType;

    @Value("${aad.subscription.notificationUrl}")
    private String notificationUrl;

    @Value("${aad.subscription.lifecycleNotificationUrl}")
    private String lifecycleNotificationUrl;

    @Autowired
    private MsGraphClient msGraphClient;

    @Autowired
    private RedisService redisService;

    @Autowired
    private GraphConsumer graphConsumer;

    @Autowired
    private IChannelsService channelsService;


    @Override
    public ResponseData generateTeam(TeamsParam teamsParam, boolean generateDefaultChannel) {
        try {
            //1 构造组员
            String memberIds = teamsParam.getMemberIds();
            List<String> mIds = Arrays.asList(memberIds.split(","));
            List<String> membersUrls = new ArrayList<>();
            for (String mid : mIds) {
                membersUrls.add(GraphConst.GET_USER + mid);
            }
            //2 构造组
            Group newGroup = new Group();
            newGroup.displayName = teamsParam.getDisplayName();
            newGroup.description = teamsParam.getDescription();
            newGroup.mailEnabled = teamsParam.isMailEnabled();
            newGroup.mailNickname = teamsParam.getMailNickname();
            newGroup.securityEnabled = teamsParam.isSecurityEnabled();
            newGroup.groupTypes = teamsParam.getGroupTypes();
            //绑定组成员
            newGroup.additionalDataManager()
                    .put(GraphConst.MEMBERS_BIND, JsonHandler.parseJsonElement(membersUrls));

            //3 创建 post 请求
            Group group = MsGraphClient.graphClient
                    .groups()
                    .buildRequest()
                    .post(newGroup);
            String teamsId = group.id;
            //4 创建 teams 使用自定义的TeamRequest的封装类发送put请求。（sdk中没有提供put接口）
            Team team = new TeamRequestBuilderExpand("https://graph.microsoft.com/v1.0/groups/" + teamsId + "/team", MsGraphClient.graphClient, null)
                    .buildPutRequest()
                    .put(buildTeam());
            //       teams信息入库
            teamsParam.setMsTeamId(teamsId);
            ResponseData res = graphConsumer.addTeam(teamsParam);
            Integer teamId = ValueUtils.parseInteger(res.getData());
            teamsParam.setTeamId(teamId);
            //5 创建 默认频道
            if (generateDefaultChannel) {
                List<Channel> channels = generateDefaultChannel(teamsParam);
            }
            return ResponseData.success(200, "团队创建成功", teamsParam);
        } catch (Exception e) {
            log.error("生成 Microsoft Teams 时发生异常", e);
            return ResponseData.error("生成 Microsoft Teams 时发生异常,创建失败");
        }
    }

    @Override
    public List<Channel> generateDefaultChannel(TeamsParam teamsParam) {
        List<Channel> channels = new ArrayList<>();
        LinkedList<String> names = new LinkedList<String>() {{
            add("Booking" + new Random().nextInt(999999));
        }};
        try {
            for (String name : names) {
                Channel channel = new Channel();
                channel.displayName = name;
                channel.description = GraphConst.CHANNEL_DEFAULT_DESCRIPTION;
                Channel result = MsGraphClient.graphClient.teams(teamsParam.getMsTeamId()).channels()
                        .buildRequest()
                        .post(channel);
                channels.add(result);
                String msChannelId = result.id;
                //构造待入库的  频道本地信息
                teamsParam.setMsChannelId(msChannelId);
                teamsParam.setDisplayName(name);
                teamsParam.setDescription(GraphConst.CHANNEL_DEFAULT_DESCRIPTION);
                teamsParam.setType(ChannelsTypeEnum.BOOKING.getCode());
                ResponseData res = graphConsumer.addChannel(teamsParam);
                Integer channelId = ValueUtils.parseInteger(res.getData());
                teamsParam.setChannelId(channelId);
                //创建此频道的订阅  根据开关决定是否创建订阅
                if (subscriptionEnable) {
                    rabbitSender.sendSubscriptionMsg(JSONObject.toJSONString(teamsParam));
                }
                log.info("发送【订阅频道请求】消息, 参数内容： {}", JSONObject.toJSONString(teamsParam));

            }
            log.info("默认频道 Booking 创建成功，已同步到业务系统");
        } catch (Exception e) {
            log.error("创建默认频道时发生异常", e);
        }
        return channels;
    }

    @Override
    public ResponseData createChannel(TeamsParam teamsParam) {
        try {
            String displayName = teamsParam.getDisplayName();
            String description = teamsParam.getDescription();
            Channel channel = new Channel();
            channel.displayName = displayName;
            channel.description = description;
            Channel result = MsGraphClient.graphClient.teams(teamsParam.getMsTeamId()).channels()
                    .buildRequest()
                    .post(channel);
            String msChannelId = result.id;
            //构造待入库的  频道本地信息
            teamsParam.setMsChannelId(msChannelId);
            ResponseData res = graphConsumer.addChannel(teamsParam);
            if (res.getCode() != 200) {
                return res;
            }
            Integer channelId = ValueUtils.parseInteger(res.getData());
            teamsParam.setChannelId(channelId);
            log.info("频道 {} 创建成功，已同步到业务系统", displayName);

            //创建此频道的订阅 根据开关决定是否创建订阅
            if (subscriptionEnable) {
                rabbitSender.sendSubscriptionMsg(JSONObject.toJSONString(teamsParam));
                log.info("已发送【订阅频道】消息, 参数内容： {}", JSONObject.toJSONString(teamsParam));
            }

            return ResponseData.success(msChannelId);
        } catch (Exception e) {
            log.error("创建频道时发生异常", e);
            return ResponseData.error("创建频道时发生异常");
        }

    }

    @Override
    public ResponseData sendMessage(TeamsParam teamsParam) {
        try {
            // 构造消息
            ChannelMessage channelMessage = new ChannelMessage();
            MessageBody messageBody = new MessageBody();
            messageBody.setContent(teamsParam.getContent());
            //contentType不为空则设置为发送对应类型的消息
            if (StringUtils.isNotBlank(teamsParam.getContentType())) {
                messageBody.setContentType(teamsParam.getContentType());
            }
            channelMessage.setBody(messageBody);

            //向频道中发送指定消息
            String channelUrl = "https://graph.microsoft.com/beta/teams/" + teamsParam.getMsTeamId() + "/channels/" + teamsParam.getMsChannelId() + "/messages";
            Object message = new ChannelRequestBuilderExpand(channelUrl, MsGraphClient.graphClientBeta, null)
                    .buildRequest()
                    .sendMessage(JsonHandler.parseJsonElement(channelMessage));
            //入库消息至业务系统
            teamsParam.setMsMessageId(((Channel) message).id);
            graphConsumer.addMessages(teamsParam);
            log.info("向teams发送消息成功，消息内容：{},发送人：{}", teamsParam.getContent(), teamsParam.getCreateName());
            return ResponseData.success(200, "消息发送成功", null);
        } catch (Exception e) {
            log.error("发送消息时异常", e);
            return ResponseData.error("发送消息时异常");
        }
    }

    @Override
    public Team buildTeam() {
        Team team = new Team();
        TeamMemberSettings memberSettings = new TeamMemberSettings();
        memberSettings.allowCreateUpdateChannels = true;
        team.memberSettings = memberSettings;
        TeamMessagingSettings messagingSettings = new TeamMessagingSettings();
        messagingSettings.allowUserEditMessages = true;
        messagingSettings.allowUserDeleteMessages = true;
        team.messagingSettings = messagingSettings;
        TeamFunSettings funSettings = new TeamFunSettings();
        funSettings.allowGiphy = true;
        funSettings.giphyContentRating = GiphyRatingType.STRICT;
        team.funSettings = funSettings;
        return team;
    }


    @Override
    public IGroupCollectionPage getGroups() {
        return MsGraphClient.graphClient.groups().buildRequest().get();
    }


    @Override
    public User getUserInfo() {
        User me = MsGraphClient.graphClient
                .me()
                .buildRequest()
                .get();

        return me;
    }

    @Override
    public User getUserInfo(String userId) {
        User me = MsGraphClient.graphClient
                .me()
                .buildRequest()
                .select(userId)
                .get();

        return me;
    }


    @Override
    public void registerGraphClient(String accessToken, String clientToken) {
        msGraphClient.ensureGraphClient(accessToken);
        msGraphClient.ensureGrapClientBeta(accessToken);
        msGraphClient.ensureGrapClientBase(clientToken);
    }

    @Override
    public IAuthenticationResult getAccessTokenByUserBase(String account, String password) {
        return msGraphClient.getAccessToken(account, password);
    }

    @Override
    public IAuthenticationResult getAccessTokenForClient() {
        return msGraphClient.getAccessToken();
    }

    @Override
    public IAuthenticationResult refreshAccessToken(String refreshAccessToken) {
        return msGraphClient.refreshAccessToken(refreshAccessToken);
    }


    @Override
    public void storeToken(String accessToken, String refreshToken, String clientToken) {
        redisService.cacheValue(RedisConstants.MS_ACCESS_TOKEN, accessToken, SystemConstants.DEFAULT_MS_TOKEN_TIME_OUT_SECS);
        redisService.cacheValue(RedisConstants.MS_CLIENT_TOKEN, clientToken, SystemConstants.DEFAULT_MS_TOKEN_TIME_OUT_SECS);
        redisService.cacheValue(RedisConstants.MS_REFRESH_TOKEN, refreshToken, SystemConstants.DEFAULT_REFRESH_TOKEN_TIME_OUT_SECS);
    }

    @Override
    public String keepToken() {
        log.info("==========开始检查 Microsft Graph 身份验证 Token==========");
        String accessToken = ValueUtils.parseString(redisService.getValue(RedisConstants.MS_ACCESS_TOKEN));
        String refreshToken = ValueUtils.parseString(redisService.getValue(RedisConstants.MS_REFRESH_TOKEN));
        String clientToken = ValueUtils.parseString(redisService.getValue(RedisConstants.MS_CLIENT_TOKEN));
        IAuthenticationResult result;
        IAuthenticationResult client;
        if (StringUtils.isBlank(accessToken) || StringUtils.isBlank(refreshToken) || StringUtils.isBlank(clientToken)) {
            log.info("==========缓存中未缺少验证令牌，准备重新获取==========");
            result = getAccessTokenByUserBase(username, password);
        } else {
            log.info("==========成功识别验证令牌,准备刷新令牌==========");
            result = refreshAccessToken(refreshToken);
        }
        client = getAccessTokenForClient();
        if (result == null) {
            String error = "身份信息为空，请检查相关参数";
            log.error(error);
            return error;
        }
        if (client == null) {
            String error = "客户端验证信息为空，请检查相关参数";
            log.error(error);
            return error;
        }

        log.info("获取成功,返回的用户身份信息" + JsonHandler.parseJson(result));
        log.info("获取成功,返回的客户端身份信息" + JsonHandler.parseJson(client));
        accessToken = result.accessToken();
        clientToken = client.accessToken();
        log.info("当前有效验证用户身份令牌:" + accessToken);
        log.info("当前有效验证客户端身份令牌:" + clientToken);
        refreshToken = ValueUtils.parseString(ReflectUtil.getFieldValue(result, "refreshToken"));
        storeToken(accessToken, refreshToken, clientToken);
        registerGraphClient(accessToken, clientToken);
        log.info("==========Microsoft Graph Client 已注册==========");
        return "";
    }

    @Override
    public ResponseData subscriptionChannel(TeamsParam teamsParam) {
        try {
            //构造默认订阅时间 60分钟  或多去配置文件中的时间
            Calendar expirationDateTime = Calendar.getInstance();
            expirationDateTime.add(Calendar.MINUTE, ValueUtils.parseInt(expireMinute));
            //构造 resource
            String msTeamsId = teamsParam.getMsTeamId();
            String msChannelId = teamsParam.getMsChannelId();
            String resource = "/teams/" + msTeamsId + "/channels/" + msChannelId + "/messages";

            //构造订阅对象
            SubscriptionExpand subscription = new SubscriptionExpand();
            subscription.resource = resource;
            subscription.expirationDateTime = expirationDateTime;
            subscription.includeResourceData = true;
            //以下内容读取配置
            subscription.changeType = changeType;
            subscription.notificationUrl = notificationUrl;
            subscription.lifecycleNotificationUrl = lifecycleNotificationUrl;
            subscription.encryptionCertificate = encryptionCertificate;
            subscription.encryptionCertificateId = encryptionCertificateId;

            Subscription sub = MsGraphClient.graphClientBase
                    .subscriptions()
                    .buildRequest()
                    .post(subscription);
            //对象转换
            ObjectMapper objectMapper = new ObjectMapper();
            SubscriptionExpand result = objectMapper.readValue(sub.getRawObject().toString(), new TypeReference<SubscriptionExpand>() {
            });
            log.info("频道已订阅，接收到返回值：" + result.toString());
            String msSubscriptionsId = result.id;
            teamsParam.setMsSubscriptionsId(msSubscriptionsId);
            teamsParam.setExpirationDateTime(result.expirationDateTime.getTime());
            //业务系统保存订阅信息
            ResponseData res = graphConsumer.addSubscriptions(teamsParam);
            if (res.getCode() == 200) {
                log.info("频道订阅完成");
                return ResponseData.success(200, "频道订阅完成", result);
            }
            return res;
        } catch (IOException e) {
            String error = "根据频道ID订阅消息时，出现异常";
            log.error(error, e);
            return ResponseData.error(error);
        }
    }

    @Override
    public void subscriptionExpirationCleanup(GraphServiceException graphException, Subscriptions currentSub) {
        int responseCode = graphException.getResponseCode();
        if (responseCode == 404 && graphException.getServiceError().code.equals(GraphConst.RESOURCE_NOT_FOUND)) {
            if (currentSub == null) {
                log.error("该订阅业务对象为空，系统将自动忽略无效信息");
            } else {
                Integer channelId = currentSub.getChannelsId();
                Integer subId = currentSub.getId();
                String msId = currentSub.getMsId();
                log.info("该频道 id:{},的订阅在预期外已过期，即将删除此失效订阅信息,待删除订阅业务id为：{}, msId为: {}", channelId, subId, msId);
                Channels one = channelsService.selectById(channelId);
                if (one == null) {
                    // 否 -> 输出频道已经不存在的日志
                    log.info("频道信息不存在，忽略本次续订");
                } else {
                    //是 -> 重新订阅频道信息
                    log.info("频道id：{}仍存在，对应订阅信息缺失或已过期，即将重新订阅。", channelId);
                    Integer teamId = one.getTeamsId();
                    String msChannelId = one.getMsId();
                    String msTeamsId = graphConsumer.getTeamsMsId(teamId);
                    TeamsParam teamsParam = new TeamsParam(msTeamsId, msChannelId, channelId);
                    //重新订阅
                    subscriptionChannel(teamsParam);
                }
                //删除已经失效订阅
                boolean result = graphConsumer.deleteSubById(subId);
                if (result) {
                    log.info("已删除已失效的订阅信息，订阅业务id：{}，msId：{}", channelId, msId);
                }

            }

        }
    }

    @Override
    public void renewAllSubscriptions() {
        Subscriptions currentSub = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            ResponseData res = graphConsumer.getAllSubscriptions();
            if (res.getCode() == 200) {
                //成功获取到所有订阅
                List<Subscriptions> subscriptions = JSONArray.parseArray(JSONObject.toJSONString(res.getData()), Subscriptions.class);
                if (CollectionUtil.isEmpty(subscriptions)) {
                    log.info("业务系统中暂时没有需要 续订的订阅信息");
                } else {
                    Calendar expireDateTime = Calendar.getInstance();
                    //上一波订阅的即将过期的创建时间 = 当前时间 - 订阅有效期
                    expireDateTime.add(Calendar.MINUTE, ValueUtils.parseInt(expireMinute));

                    for (Subscriptions sub : subscriptions) {
                        currentSub = sub;
                        //系统中订阅过期时间
                        String msSubscriptionsId = sub.getMsId();
                        Subscription subParam = new Subscription();
                        subParam.expirationDateTime = expireDateTime;
                        //更新订阅接口
                        Subscription result = MsGraphClient.graphClientBase
                                .subscriptions(msSubscriptionsId)
                                .buildRequest()
                                .patch(subParam);
                        if (result != null) {
                            String nextExpireTime = sdf.format(result.expirationDateTime.getTime());
                            log.info("频道消息订阅已成功续订，当前订阅id为: {} 过期时间为：{}", result.id, nextExpireTime);
                            //将新的订阅id更新至业务系统
                            sub.setMsId(result.id);
                            sub.setExpirationDateTime(result.expirationDateTime.getTime());
                            graphConsumer.updateSub(sub);
                            log.info("业务系统的订阅到期时间 {} 已更新", nextExpireTime);
                        } else {
                            log.info("频道消息订阅失败，无返回结果,失败的订阅id为: {}", msSubscriptionsId);
                        }
                        //防止请求过快
                        Thread.sleep(WAIT_MILLIS);
                    }
                }

            }
        } catch (GraphServiceException graphException) {
            log.error("续订业务系统中订阅信息列表时出现异常", graphException.getMessage());
            subscriptionExpirationCleanup(graphException, currentSub);
        } catch (InterruptedException e) {
            log.error("续订时系统出现异常", e);
        }
    }
}
