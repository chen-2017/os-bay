// ------------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All Rights Reserved.  Licensed under the MIT License.  See License in the project root for license information.
// ------------------------------------------------------------------------------

package com.ws.ztyt.msal.core.graph.extensions;

import com.microsoft.graph.concurrency.ICallback;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.core.IBaseClient;
import com.microsoft.graph.http.BaseRequest;
import com.microsoft.graph.http.HttpMethod;
import com.microsoft.graph.models.extensions.Team;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.ITeamRequest;


/**
 * 重写 TeamsRequest 类
 */
public class TeamRequestExpand extends BaseRequest implements ITeamRequestExpand {

    /**
     * The request for the Team
     *
     * @param requestUrl     the request URL
     * @param client         the service client
     * @param requestOptions the options for this request
     */
    public TeamRequestExpand(final String requestUrl, final IBaseClient client, final java.util.List<? extends Option> requestOptions) {
        super(requestUrl, client, requestOptions, Team.class);
    }

    /**
     * Gets the Team from the service
     *
     * @param callback the callback to be called after success or failure
     */
    @Override
    public void get(final ICallback<Team> callback) {
        send(HttpMethod.GET, callback, null);
    }

    /**
     * Gets the Team from the service
     *
     * @return the Team from the request
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    @Override
    public Team get() throws ClientException {
        return send(HttpMethod.GET, null);
    }

    /**
     * Delete this item from the service
     *
     * @param callback the callback when the deletion action has completed
     */
    @Override
    public void delete(final ICallback<Team> callback) {
        send(HttpMethod.DELETE, callback, null);
    }

    /**
     * Delete this item from the service
     *
     * @throws ClientException if there was an exception during the delete operation
     */
    @Override
    public void delete() throws ClientException {
        send(HttpMethod.DELETE, null);
    }

    /**
     * Put this Team with a source
     *
     * @param sourceTeam the source object with updates
     * @param callback   the callback to be called after success or failure
     */
    public void put(final Team sourceTeam, final ICallback<Team> callback) {
        send(HttpMethod.PUT, callback, sourceTeam);
    }

    /**
     * Patches this Team with a source
     *
     * @param sourceTeam the source object with updates
     * @param callback   the callback to be called after success or failure
     */
    @Override
    public void patch(final Team sourceTeam, final ICallback<Team> callback) {
        send(HttpMethod.PATCH, callback, sourceTeam);
    }

    /**
     * Patches this Team with a source
     *
     * @param sourceTeam the source object with updates
     * @return the updated Team
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    @Override
    public Team patch(final Team sourceTeam) throws ClientException {
        return send(HttpMethod.PATCH, sourceTeam);
    }

    /**
     * Creates a Team with a new object
     *
     * @param newTeam  the new object to create
     * @param callback the callback to be called after success or failure
     */
    @Override
    public void post(final Team newTeam, final ICallback<Team> callback) {
        send(HttpMethod.POST, callback, newTeam);
    }

    /**
     * Creates a Team with a new object
     *
     * @param newTeam the new object to create
     * @return the created Team
     * @throws ClientException this exception occurs if the request was unable to complete for any reason
     */
    @Override
    public Team post(final Team newTeam) throws ClientException {
        return send(HttpMethod.POST, newTeam);
    }

    /**
     * Sets the select clause for the request
     *
     * @param value the select clause
     * @return the updated request
     */
    @Override
    public ITeamRequest select(final String value) {
        getQueryOptions().add(new QueryOption("$select", value));
        return (TeamRequestExpand) this;
    }

    /**
     * Sets the expand clause for the request
     *
     * @param value the expand clause
     * @return the updated request
     */
    @Override
    public ITeamRequest expand(final String value) {
        getQueryOptions().add(new QueryOption("$expand", value));
        return (TeamRequestExpand) this;
    }

    @Override
    public Team put(Team sourceTeam) throws ClientException {
        return send(HttpMethod.PUT, sourceTeam);
    }
}

