package com.ws.ztyt.msal.modular.graph.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ws.ztyt.msal.core.graph.model.AcceptMessage;
import com.ws.ztyt.msal.core.tools.StringUtils;
import com.ws.ztyt.msal.modular.graph.dao.ChannelsMapper;
import com.ws.ztyt.msal.modular.graph.model.Channels;
import com.ws.ztyt.msal.modular.graph.service.IChannelsService;
import com.ws.ztyt.msal.modular.oauth.consumer.GraphConsumer;
import com.ws.ztyt.msal.modular.oauth.model.TeamsParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChannelsServiceImpl extends ServiceImpl<ChannelsMapper, Channels> implements IChannelsService {

    private static final Logger log = LoggerFactory.getLogger(ChannelsServiceImpl.class);

    @Autowired
    private ChannelsMapper mapper;

    @Autowired
    private GraphConsumer graphConsumer;

    @Override
    public List<Channels> getList(Integer maxId) {
        return mapper.getList(maxId);
    }


    //去除cert@metoor.onmicrosoft.com 用户
    @Value("${aad.excludeTenantId}")
    private String excludeTenantId;

    @Override
    public List<AcceptMessage> parseMessage(String message) {
        JSONObject responseJson = JSON.parseObject(message);
        String reponseArray = responseJson.getString("responses");
        Map<String, JSONObject> reponseMap = new HashMap<String, JSONObject>();

        JSONArray jsonArrays = JSONArray.parseArray(reponseArray);
        for (int i = 0; i < jsonArrays.size(); i++) {
            JSONObject jsonObject = jsonArrays.getJSONObject(i);
            String id = jsonObject.getString("id");
            reponseMap.put(id, jsonObject);
        }
        //log.info( "reponseMap:\n {}",reponseMap );

        List<AcceptMessage> list = new ArrayList<AcceptMessage>();

        String nextUrl = "";
        //迭代恢复消息
        for (String key : reponseMap.keySet()) {
            JSONObject reponseItem = reponseMap.get(key);
            String reponseCode = reponseItem.getString("status");
            if ("200".equals(reponseCode)) { //返回成功
                JSONObject reponseBody = reponseItem.getJSONObject("body");
                if (reponseBody != null) {
                    //解析每条消息
                    nextUrl = reponseBody.getString("@odata.nextLink");
                    if (StringUtils.isBlank(nextUrl)) {
                        nextUrl = reponseBody.getString("@odata.deltaLink");
                    }
                    JSONArray valueArray = reponseBody.getJSONArray("value");
                    for (int i = 0; i < valueArray.size(); i++) {
                        JSONObject valueObject = valueArray.getJSONObject(i);
                        JSONObject messageObject = valueObject.getJSONObject("body");
                        JSONObject fromObject = valueObject.getJSONObject("from");
                        String msMessageId = valueObject.getString("id");
                        String userName = "";
                        if (messageObject != null) {
                            String contentType = messageObject.getString("contentType");
                            String content = messageObject.getString("content");
                            if (fromObject != null) {
                                JSONObject userObject = fromObject.getJSONObject("user");//获取发送人用户信息
                                //7a86e7b5-ccde-4bed-8263-0f6a4551c308 用户Id,用于排除是否页面发起的请求
                                userName = userObject.getString("displayName");
                                String userId = userObject.getString("id");
                                if (!excludeTenantId.equals(userId)) {
                                    log.info("获取到的消息类型(contentType):{}, 消息内容(content):{}，发送人名称（userName）:{}", contentType, content, userName);
                                    AcceptMessage meesage = new AcceptMessage(Integer.parseInt(key),
                                            contentType, content, userName, "", "", msMessageId);
                                    list.add(meesage);
                                }
                            }
                        }
                    }
                }
            }
            Channels channel = mapper.selectById(Integer.parseInt(key));
            if (channel != null && StringUtils.isNotBlank(nextUrl)) {
                nextUrl = nextUrl.replaceAll("https://graph.microsoft.com/beta/", "");
                channel.setNextUrl(nextUrl);
                mapper.updateById(channel);
            }
        }
        return list;
    }

}
