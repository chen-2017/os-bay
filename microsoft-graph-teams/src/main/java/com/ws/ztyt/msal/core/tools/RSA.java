/**
 * @Project：
 * @Title：RSA.java
 * @Description：
 * @Package com.microsoft.azure.msalwebsample
 * @author：yangyucheng
 * @date：2020/1/3
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ws.ztyt.msal.core.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ws.ztyt.core.util.PropertiesUtils;
import com.ws.ztyt.msal.core.graph.model.MsgResult;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

@Slf4j
@Component
public class RSA {


    //test123
    private static final String pfxPassword = "123456";//私钥文件获取时设置的密钥

    private static String aliasName = "zhangsan";//alias名称

    /**
     * 签名
     *
     * @return 签名后经过base64处理的字符串
     * @throws Exception
     */
    public static String sign(String str) {
        String privateKeyFileName = PropertiesUtils.getPropertiesValue("aad.subscription.privateKeyFileName");
        String base64Sign = "";
        InputStream fis = null;
        try {
            fis = new FileInputStream(privateKeyFileName);
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            char[] pscs = pfxPassword.toCharArray();
            keyStore.load(fis, pscs);
            PrivateKey priKey = (PrivateKey) (keyStore.getKey(aliasName, pscs));
            // 签名
            Signature sign = Signature.getInstance("RSA");
            sign.initSign(priKey);
            byte[] bysData = str.getBytes("UTF-8");
            sign.update(bysData);
            byte[] signByte = sign.sign();
            BASE64Encoder encoder = new BASE64Encoder();
            base64Sign = encoder.encode(signByte);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return base64Sign;
    }

    /**
     * 数据验证
     *
     * @param signStr 加密后的数据
     * @param verStr  原始字符
     * @return
     */
    public static boolean verify(String signStr, String verStr)
            throws Exception {
        String publicKeyFileName = PropertiesUtils.getPropertiesValue("aad.subscription.publicKeyFileName");

        boolean verfy = false;
        InputStream fis = null;
        try {
            fis = new FileInputStream(publicKeyFileName);
            CertificateFactory cf = CertificateFactory.getInstance("x509");
            Certificate cerCert = cf.generateCertificate(fis);
            PublicKey pubKey = cerCert.getPublicKey();
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] signed = decoder.decodeBuffer(signStr);
            Signature sign = Signature.getInstance("SHA1withRSA");
            sign.initVerify(pubKey);
            sign.update(verStr.getBytes("UTF-8"));
            verfy = sign.verify(signed);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return verfy;
    }


    /**
     * 通过公钥文件进行加密数据
     *
     * @return 加密后经过base64处理的字符串
     */
    public static String encrypt(String source) throws Exception {
        String publicKeyFileName = PropertiesUtils.getPropertiesValue("aad.subscription.publicKeyFileName");

        InputStream fis = null;
        try {
            fis = new FileInputStream(publicKeyFileName);
            CertificateFactory cf = CertificateFactory.getInstance("x509");
            Certificate cerCert = cf.generateCertificate(fis);
            PublicKey pubKey = cerCert.getPublicKey();
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] sbt = source.getBytes();
            byte[] epByte = cipher.doFinal(sbt);
            BASE64Encoder encoder = new BASE64Encoder();
            String epStr = encoder.encode(epByte);
            return epStr;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 通过私钥文件进行解密数据
     *
     * @return 解密后的明文字符串
     */
    public static String decode(String source) throws Exception {
        String privateKeyFileName = PropertiesUtils.getPropertiesValue("aad.subscription.privateKeyFileName");

        //BASE64Decoder b64d = new BASE64Decoder();
        //byte[] keyByte = b64d.decodeBuffer(source);
        //byte[] keyByte = source.getBytes("UTF-8");
        //byte[] keyByte = stringToBytes(source);
        byte[] keyByte = Base64.decode(source.getBytes("UTF-8"));
        InputStream fis = null;
        try {
            fis = new FileInputStream(privateKeyFileName);
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            char[] pscs = pfxPassword.toCharArray();
            keyStore.load(fis, pscs);
            PrivateKey priKey = (PrivateKey) (keyStore.getKey(aliasName, pscs));
            //RSA/ECB/OAEPWithSHA-1AndMGF1Padding
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            byte[] epByte = cipher.doFinal(keyByte);
            return new String(epByte, "UTF-8");
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static byte[] hmacSHA256(String content, byte[] key) throws Exception {
        Mac hmacSha256 = Mac.getInstance("HmacSHA256");
        hmacSha256.init(new SecretKeySpec(key, "HmacSHA256"));
        byte[] hmacSha256Bytes = Base64.encode(hmacSha256.doFinal(Base64.decode(content.getBytes("utf-8"))));
        return hmacSha256Bytes;
    }

    public static byte[] decrypt(byte[] dataKey) throws Exception {
        String privateKeyFileName = PropertiesUtils.getPropertiesValue("aad.subscription.privateKeyFileName");

        byte[] keyByte = Base64.decode(dataKey);
        InputStream fis = null;
        try {
            fis = new FileInputStream(privateKeyFileName);
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            char[] pscs = pfxPassword.toCharArray();
            keyStore.load(fis, pscs);
            PrivateKey priKey = (PrivateKey) (keyStore.getKey(aliasName, pscs));
            //RSA/ECB/OAEPWithSHA-1AndMGF1Padding
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            byte[] epByte = cipher.doFinal(keyByte);
            return epByte;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static {
        Security.addProvider(new BouncyCastleProvider());
    }


    private static String transformation = "AES/CBC/PKCS7Padding";
    private static String algorithm = "AES";

    public static String decrypt(byte[] content, byte[] key) {
        try {
            SecretKeySpec skey = new SecretKeySpec(key, algorithm);
            //AlgorithmParameterSpec paramSpec = new IvParameterSpec(new byte[16]);
            IvParameterSpec iv = new IvParameterSpec(key, 0, 16);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, skey, iv);// 初始化
            byte[] result = cipher.doFinal(Base64.decode(content));
            return new String(result); // 解密
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Pair<String, MsgResult> decryptMsg(String data, String dataKey) {
        try {
            byte[] result = data.getBytes("UTF-8");
            byte[] key = decrypt(dataKey.getBytes());
            String dataSignature = new String(hmacSHA256(data, key), "utf-8");
            String realContent = decrypt(result, key);
            ObjectMapper objectMapper = new ObjectMapper();
            MsgResult msgResult = objectMapper.readValue(realContent, MsgResult.class);
            //对象转换
            return Pair.makePair(dataSignature, msgResult);
        } catch (UnsupportedEncodingException e) {
            log.error("订阅消息解密时编码转换出现异常", e);
            return null;
        } catch (Exception e) {
            log.error("订阅消息解密时出现异常", e);
            return null;
        }
    }

    public static void main(String[] args) throws Exception {

        String dataValue = "i9fG1Y0AT8vlVL1Cwjwj7zlq/SN7QmCnlKqoO9WU9B/puS7g3aLuRnXnN/DCEGdDhzJBqKucq7jnNj00wkqnO5Bvl/tuitJVhcdkyj0Vi6H83YzU7k441iZPm5ojRCDHUPDMe+KiHqZ9BWwQ+KIBjkNvDs6kX7AY/jx6t4IU9p9qDj3D81kkktadAAB87clZZmusqXo+eFfKx67mpbKn1szVkJJQ9NF6K4d356RXMZbvxnigSfiMRkYpyYUB99Wa3QOldIdPHadGvSkD/Ti+ITQ/Ehaz2oIBzbykrRhbbZXF2Cr73Y5IU90uUrTYw+pFRkWudkYaZQZmyNzLKO2gqJFM8Bjo+MdtTj8MP59xK2bkfZ1r+zUWHkj6js2iyvg0FN5Vviyn5O7df91rAQ7HSGUdtx8eljkClcnTg6EPzKdahS3oseZ0Uaow1RRsnJ9WM11pk4OCTOSJ5G3oB7MXNHzpUFZh9qhVhUx/QsEWdEcmQFHggSQqhkTYS+Gf6PPx+bk+a560Wg3weIWKBCLbWGVcUq4KW4bRyNIaFruqojY+Kf2sfUFy/7Jdb34AKC/W5zCwpxQFd6HJvwJwyO6o+6p1u15wdI/vBzeIyFvFYe/FaVhLrjY1XICbJbBOttm/3DJE04JmH8hRQrE8rOLiWOzLvluSywLVFMviQyyMRmacdixewv45o76/er/D6x3OF6FLmK3zHtqVCPgtybG72clxOba5tTn3DpzQFxQ/ZZFcXGWpPEHfbHqiW2LTLO96YG1HCoX/35H6MH+H8YoClQ26UjzWq2p8dPZ/El9eq5cYvMhyMjjw8uj998Ke2LFzyNC8eSo2micknoSlEdca3m7JdptgPxe0T6Bi0p3g1U4gLa8XSOgK87IW4JdVw21fg4HyAGSEDs2RWBUjjdEoF3TIJoeQpo4lE7aWkCoe9qHM5CjOYqC5yO/pHY2um+OFw9SCmGLs3V7z6ybnKlnLD3YwkQX/DyV7tpsFusNhttvJpXGJkamg8Gd2uqCRFKnEnJMHYNtvm2a3lLJdrPG+PBy5H5KfR7F+OXzgAAMRTVzJ5aFDoh9KU4OyNOmYR95DCUi4YsI1dvv1qSGzI5jhyQ==";
        byte[] result = dataValue.getBytes("UTF-8");

        String dataKey = "m/+TiLDBGJt6CmD1ulGEVzdlZOI6lUOl5o15TUcpt8KCMIHbnqIf8v5hvtgKP08GHy//IlamMGaNblhGJUNWg6gXjQKNVbnwYeWIs6I7Au3yIYo0No8ucGAhonuhfDm6QtHT4bxBn6MsJYL09W66NXB+N/W1FMPDhM4EfRgpiCQQSy37oIDVnazY0U47l/idYaXwVnFwPYNYolczMjBTpAdm/TY44RBLc0F1GtUWNvtrKnCel7av38Sa0t95ACIv5roHrsfpcAMyoWPljUjGyyeywK5FOAi9z2pxXen/cx0Yyb6XfWM+VzAKsPW3chjR/DIrQKZt668Sxc/pdBhZng==";
        byte[] key = decrypt(dataKey.getBytes());

        System.out.println("dataSignature:" + new String(hmacSHA256(dataValue, key), "utf-8"));

        System.out.println(decrypt(result, key));

    }


}
