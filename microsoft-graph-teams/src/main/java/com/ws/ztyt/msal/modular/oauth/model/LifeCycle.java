package com.ws.ztyt.msal.modular.oauth.model;

import lombok.Data;

@Data
public class LifeCycle {
    private String lifecycleEvent;
    private String subscriptionId;
    private String subscriptionExpirationDateTime;
    private String clientState;
    private String tenantId;

}
